﻿using System;
using System.IO;
using Verse;

namespace JTExport
{
    static class SaveLocation
    {

        public static readonly string split = "="; //Key and value splitter
        public static readonly string split1 = Environment.NewLine; //Different keys splitter
        public static readonly string split2 = "|"; //List values splitter
        public static readonly string split3 = ","; //List in a List splitter

        //Bill
        public static readonly string type = "type";
        public static readonly string recipeDef = "recipeDef";
        public static readonly string allowedSkillRange = "allowedSkillRange";
        public static readonly string suspended = "suspended";
        public static readonly string ingredientSearchRadius = "ingredientSearchRadius";
        public static readonly string ThingFilter = "ThingFilter";

        //ThingFilter
        public static readonly string allowedHitPointsConfigurable = "allowedHitPointsConfigurable";
        public static readonly string allowedQualitiesConfigurable = "allowedQualitiesConfigurable";
        public static readonly string AllowedHitPointsPercents = "AllowedHitPointsPercents";
        public static readonly string AllowedQualityLevels = "AllowedQualityLevels";
        public static readonly string AllowedThingDefs = "AllowedThingDefs";
        public static readonly string disallowedSpecialFilters = "disallowedSpecialFilters";

        //Bill_Production
        public static readonly string repeatCount = "repeatCount";
        public static readonly string repeatMode = "repeatMode";
        public static readonly string storeMode = "storeMode";
        public static readonly string targetCount = "targetCount";
        //A17
        public static readonly string pauseWhenSatisfied = "pauseWhenSatisfied";
        public static readonly string unpauseWhenYouHave = "unpauseWhenYouHave";
        public static readonly string paused = "paused";

        //Hysteresis
        //public static readonly string paused = "paused";
        public static readonly string pauseOnCompletion = "pauseOnCompletion";
        public static readonly string unpauseOnExhaustion = "unpauseOnExhaustion";
        public static readonly string unpauseThreshold = "unpauseThreshold";

        //EC
        public static readonly string AssignedPawn = "AssignedPawn";
        public static readonly string Name = "Name";
        public static readonly string MinCount = "MinCount";
        public static readonly string IsPaused = "IsPaused";

        //Stockpiles
        public static readonly string Priority = "Priority";

        public readonly static DirectoryInfo saveBase = new DirectoryInfo(Path.Combine(GenFilePaths.SaveDataFolderPath, "JTExport"));
        public readonly static DirectoryInfo saveBills = new DirectoryInfo(Path.Combine(saveBase.FullName, "Bills"));
        public readonly static DirectoryInfo saveStockpiles = new DirectoryInfo(Path.Combine(saveBase.FullName, "Stockpiles"));
        public readonly static DirectoryInfo saveStorage = new DirectoryInfo(Path.Combine(saveBase.FullName, "Storage"));

        public static void prime()
        {
            if (!saveBase.Exists)
            {
                saveBase.Create();
            }
            if (!saveBills.Exists)
            {
                saveBills.Create();
            }
            if (!saveStockpiles.Exists)
            {
                saveStockpiles.Create();
            }
            if (!saveStorage.Exists)
            {
                saveStorage.Create();
            }
        }

        public static bool saveData(string location, string data, string filename)
        {
            try
            {

                File.WriteAllText(location, data);
                Log.Message("JTExport: exported " + filename + ".txt");
                return true;

            }
            catch (Exception e)
            {
                Log.Error("JTExport: " + e.GetType().ToString() + " error while trying to save to = " + location);
                return false;
            }
        }

        public static string[] loadData(string location)
        {
            try
            {

                return File.ReadAllLines(location);

            }
            catch (Exception e)
            {
                Log.Error("JTExport: " + e.GetType().ToString() + " error while trying to load from = " + location);
                return null;
            }
        }

        public static string[] findFiles(DirectoryInfo loc)
        {
            FileInfo[] files = loc.GetFiles("*.txt", SearchOption.TopDirectoryOnly);
            string[] names = new string[files.Length];
            for (int i = 0; i < files.Length; i++)
            {
                string[] split = files[i].Name.Split(new string[] { "." }, StringSplitOptions.None);
                names[i] = split[0];
            }
            return names;
        }

    }
}
