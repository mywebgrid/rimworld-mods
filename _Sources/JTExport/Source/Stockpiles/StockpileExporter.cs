﻿using RimWorld;
using System.IO;

namespace JTExport
{
    class StockpileExporter
    {

        ///StorageSettings
        ///priority (enum StoragePriority)
        ///ThingFilter from BillExporter

        public static bool exportStockpile(string filename, StorageSettings settings)
        {
            string result = "";

            result += SaveLocation.Priority + SaveLocation.split + (int)settings.Priority + SaveLocation.split1;

            result += SaveLocation.ThingFilter + SaveLocation.split + BillExporter.ThingFilterString(settings.filter) + SaveLocation.split1;

            return SaveLocation.saveData(Path.Combine(SaveLocation.saveStockpiles.FullName, filename + ".txt"), result, filename);
        }
        
    }
}
