﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.IO;

namespace JTExport
{
    class StockpileImporter
    {

        public static void importStockpile(string filename, StorageSettings settings)
        {
            string[] data = SaveLocation.loadData(Path.Combine(SaveLocation.saveStockpiles.FullName, filename + ".txt"));

            getStockpile(data, settings);

        }

        public static void getStockpile(string[] data, StorageSettings settings)
        {
            Dictionary<string, string> holder = new Dictionary<string, string>();

            BillImporter.fillDictionary(holder, data, 0, data.Length);

            if (holder.ContainsKey(SaveLocation.Priority))
            {
                int Priority;
                if (int.TryParse(holder[SaveLocation.Priority], out Priority))
                {
                    settings.Priority = (StoragePriority)Priority;
                }
            }
            if (holder.ContainsKey(SaveLocation.ThingFilter))
            {
                string[] split = holder[SaveLocation.ThingFilter].Split(new string[] { SaveLocation.split2 }, StringSplitOptions.RemoveEmptyEntries);
                BillImporter.fillThingFilter(settings.filter, split);
            }
        }

    }
}
