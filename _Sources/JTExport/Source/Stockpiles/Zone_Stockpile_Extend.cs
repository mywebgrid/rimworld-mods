﻿using RimWorld;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using UnityEngine;
using Verse;
using Verse.Sound;

namespace JTExport
{
    public class Zone_Stockpile_Extend : Zone
    {
#pragma warning disable 0649
        StorageSettings settings;
#pragma warning restore 0649

        [DebuggerHidden]
        public override IEnumerable<Gizmo> GetGizmos()
        {
            foreach (Gizmo g in base.GetGizmos())
            {
                yield return g;
            }

            foreach (Gizmo g2 in StorageSettingsClipboard.CopyPasteGizmosFor(this.settings))
            {
                yield return g2;
            }

            //Compatibility
            foreach (MethodInfo method in Vaccine.compatZone_StockpileMethods)
            {
                foreach (Gizmo g in (IEnumerable<Gizmo>)method.Invoke(null, new object[] { this }))
                {
                    yield return g;
                }
            }

            foreach (Gizmo g in getGizmos_Extend(this))
            {
                yield return g;
            }
        
            yield break;

        }

        //Call this through reflection for compatibility
        public static IEnumerable<Gizmo> getGizmos_Extend(object vanillaClass)
        {
            if (!(vanillaClass is Zone_Stockpile))
            {
                Log.Error("JTExport: could not get Zone_Stockpile from vanilla class");
                yield break;
            }

            Zone_Stockpile vanilla = (Zone_Stockpile)vanillaClass;

            //Export
            yield return new Command_Action
            {
                icon = ContentFinder<Texture2D>.Get("Export", true),
                defaultLabel = "Export stockpile",
                defaultDesc = "Export stockpile settings",
                action = delegate
                {
                    SoundDefOf.TickHigh.PlayOneShotOnCamera();
                    Find.WindowStack.Add(new Dialog_Name(vanilla.label, SaveLocation.saveStockpiles, delegate (string name)
                    {
                        //BillExporter.exportBills(name, wt.billStack);
                        StockpileExporter.exportStockpile(name, vanilla.settings);
                    }));
                },
                //hotKey = KeyBindingDefOf.Misc4
            };

            //Import
            Command_Action command_Action = new Command_Action();
            command_Action.icon = ContentFinder<Texture2D>.Get("Import", true);
            command_Action.defaultLabel = "Import stockpile";
            command_Action.defaultDesc = "Import stockpile settings";

            string[] files = SaveLocation.findFiles(SaveLocation.saveStockpiles);

            command_Action.action = delegate
            {
                SoundDefOf.TickHigh.PlayOneShotOnCamera();

                List<FloatMenuOption> list = new List<FloatMenuOption>();
                for (int i = 0; i < files.Length; i++)
                {
                    string name = files[i]; //because some trouble with index
                    list.Add(new FloatMenuOption(name, delegate
                    {
                        //BillImporter.loadBills(name, wt.billStack, wt.def.AllRecipes);
                        StockpileImporter.importStockpile(name, vanilla.settings);
                    }, MenuOptionPriority.Default, null, null, 0f, null));
                }
                if (list.Count == 0) //should never be true
                {
                    Log.Error("JTExport: you managed to magic your way past command_Action.Disable");
                }
                else
                {
                    FloatMenu floatMenu = new FloatMenu(list);
                    floatMenu.vanishIfMouseDistant = true;
                    Find.WindowStack.Add(floatMenu);
                }
            };
            if (files.Length == 0)
            {
                command_Action.Disable("No stockpile to import, export first");
            }
            //command_Action.hotKey = KeyBindingDefOf.Misc5;
            yield return command_Action;

            yield break;
        }

        protected override Color NextZoneColor
        {
            get
            {
                return ZoneColorUtility.NextStorageZoneColor();
            }
        }
    }
}