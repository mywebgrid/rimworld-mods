﻿using RimWorld;
using System;
using System.IO;
using UnityEngine;
using Verse;
using Verse.Sound;

namespace JTExport
{
    class Dialog_Name : Window
    {
        public string curName;
        DirectoryInfo folder;
        public Action<string> action;

        public readonly int MaxNameLength = 28;

        public override Vector2 InitialSize
        {
            get
            {
                return new Vector2(280, 200); //There is 18f margin, actual size -= 36f
            }
        }

        public Dialog_Name(string curName, DirectoryInfo folder, Action<string> action)
        {
            //forcePause = true;
            doCloseX = true;
            closeOnEscapeKey = true;
            absorbInputAroundWindow = true;
            closeOnClickedOutside = true;

            this.curName = curName;
            this.folder = folder;
            this.action = action;
            
        }

        public override void DoWindowContents(Rect inRect)
        {
            //Listing_Standard causes GUI problems, don't use
            Widgets.Label(new Rect(0f, 0f, 140f, 35f), "Name your present");
            if (Widgets.ButtonText(new Rect(25f, 30f, inRect.width - 50f, 35f), "Open presents folder", true, false))
            {
                SoundDefOf.TickHigh.PlayOneShotOnCamera();
                Application.OpenURL(folder.FullName);
            }

            Text.Font = GameFont.Small;
            bool flag = false;
            if (Event.current.type == EventType.KeyDown && Event.current.keyCode == KeyCode.Return)
            {
                flag = true;
                Event.current.Use();
            }
            string text = Widgets.TextField(new Rect(0f, inRect.height - 85f, inRect.width, 35f), curName);
            if (text.Length < MaxNameLength)
            {
                curName = text;
            }
            if (Widgets.ButtonText(new Rect(15f, inRect.height - 35f, inRect.width - 30f, 35f), "OK", true, false) || flag)
            {
                if (curName.Length == 0)
                {
                    Messages.Message("NameIsInvalid".Translate(), MessageSound.RejectInput);
                } else if (curName.Contains("."))
                {
                    Messages.Message("You can't have a . in the name", MessageSound.RejectInput);
                }
                else
                {
                    bool overwrite = false;
                    string[] files = SaveLocation.findFiles(folder);
                    for (int i = 0; i < files.Length; i++)
                    {
                        if (files[i].EqualsIgnoreCase(curName))
                        {
                            overwrite = true;
                            break;
                        }
                    }
                    if (overwrite)
                    {
                        Find.WindowStack.Add(new Dialog_Overwrite(curName, action, this));
                    } else
                    {
                        action(curName);
                        Find.WindowStack.TryRemove(this, true);
                    }
                }
            }
        }
        
    }
}
