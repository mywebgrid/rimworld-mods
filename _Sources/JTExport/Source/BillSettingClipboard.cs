﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using Verse;

namespace JTExportBillsZones
{
    class BillSettingClipboard
    {
        public static Dictionary<String, BillStack> clipboard = new Dictionary<string, BillStack>();

        public static Boolean CHLoaded = false;
        public static String[] sameShit = new String[] { "Smithy", "Tailoring", "Stove" };
        public const int maxBillsPerTable = 15;

        public static bool HasCopiedSettings(String s)
        {
            return clipboard.ContainsKey(s);
        }

        public static int canPasteAmount(String s, BillStack table)
        {
            if (clipboard.ContainsKey(s))
            {
                return (maxBillsPerTable - table.Bills.Count >= clipboard[s].Bills.Count) ? clipboard[s].Bills.Count : 15 - table.Bills.Count;
            }
            return 0;
        }

        public static bool canPasteAll(String s, BillStack table)
        {
            if (canPasteAmount(s, table) == clipboard[s].Bills.Count)
            {
                return true;
            }
            return false;
        }


        public static void Copy(String s, BillStack b) //stored BillStacks have copied billGiver and Bills have no unique ID
        {

            checkCH();

            BillStack addBill = new BillStack(b.billGiver);
            foreach (Bill bill in b.Bills)
            {
                addBill.AddBill(copyBill(bill, false, b));
            }
            if (clipboard.ContainsKey(s))
            {
                clipboard[s] = addBill;
            }
            else
            {
                clipboard.Add(s, addBill);
            }
        }


        public static BillStack PasteInto(String s, BillStack bills, List<RecipeDef> recipes)
        {
            if (clipboard.ContainsKey(s))
            {
                BillStack nbs = new BillStack(bills.billGiver); //Assign billGiver to be bench to be pasted
                foreach (Bill bill in bills)
                {
                    nbs.AddBill(bill);
                }
                //Should only ever have 15 bills in a stack
                int loops = canPasteAmount(s, bills);
                for (int i = 0; i < loops; i++)
                {
                    if (recipes.Contains(clipboard[s].Bills[i].recipe)) //Only copies over recipes that it can have; incase later electric vs non have different recipes
                    {
                        nbs.AddBill(copyBill(clipboard[s].Bills[i], true, nbs)); //Copy again to assign unique ID
                    }
                }
                return nbs;
            }
            return null; //Should never run, prevented in Building_JT
        }


        public static Bill copyBill(Bill b, Boolean getNewID, BillStack nbs) //Stop changing ref parameters, new recipes need unique ID
        {
            //CraftingHysteresis
            if (CHLoaded)
            {
                //Make a copy of a CH bill
                if (b.GetType().ToString().Contains("Hysteresis"))
                {
                    Bill temp = CHMethods.CraftingHysteresisWorkAround(b, getNewID, nbs);
                    if (temp != null)
                    {
                        return temp;
                    }
                    Log.Error("JTCopyBills: Unable to paste Hysteresis bills."); //This should never run
                } else
                //Convert and copy a vanilla bill to a CH bill
                {
                    Bill temp = CHMethods.convertToCraftingHysteresis(b, getNewID, nbs);
                    if (temp != null)
                    {
                        return temp;
                    }
                    Log.Error("JTCopyBills: Unable to convert to and paste Hysteresis bills."); //This should never run
                }
            }

            //Vanilla bills
            Bill_Production newB;
            Bill_Production oldB = (Bill_Production)b;
            if (b is Bill_ProductionWithUft) //Bills that have Uft are copied over with Uft, but with no assigned pawn
            {
                newB = (getNewID) ? new Bill_ProductionWithUft(b.recipe) : new Bill_ProductionWithUft();

            } else
            {
                newB = (getNewID) ? new Bill_Production(b.recipe) : new Bill_Production();
            }

            copyBaseBillProduction(newB, oldB, nbs);

            return newB;
        }

        public static String same(String s) //Allows copying and pasting same stuff (electric and non tailoring)
        {
            foreach (String same in sameShit)
            {
                if (s.Contains(same))
                {
                    return same;
                }
            }
            return s;
        }

        public static ThingFilter copyThingFilter(ThingFilter tf)
        {
            ThingFilter nft = new ThingFilter();
            nft.CopyAllowancesFrom(tf);
            return nft;
        }

        public static void copyBaseBillProduction(Bill_Production newB, Bill_Production oldB, BillStack nbs)
        {
            //Bill
            newB.allowedSkillRange = oldB.allowedSkillRange;
            newB.billStack = nbs;
            newB.ingredientFilter = copyThingFilter(oldB.ingredientFilter);
            newB.ingredientSearchRadius = oldB.ingredientSearchRadius;
            newB.recipe = oldB.recipe;
            newB.suspended = oldB.suspended;
            //Bill_Production
            newB.repeatCount = oldB.repeatCount;
            newB.repeatMode = oldB.repeatMode;
            newB.storeMode = oldB.storeMode;
            newB.targetCount = oldB.targetCount;
        }

        public static void checkCH()
        {

            //https://stackoverflow.com/questions/8499593/c-sharp-how-to-check-if-namespace-class-or-method-exists-in-c

            if (!CHLoaded)
            {
                var CH = (from assembly in AppDomain.CurrentDomain.GetAssemblies()
                          where assembly.GetName().Name == "CraftingHysteresis"
                          select assembly.GetName().Name);
                foreach (string t in CH)
                {
                    CHLoaded = true;
                    Log.Message("JTCopyBills: detected CraftingHysteresis");
                    break;
                }
            }
        }

    }
}
