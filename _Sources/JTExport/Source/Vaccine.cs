﻿using System;
using Verse;
using System.Reflection;
using RimWorld;
using System.Collections.Generic;

namespace JTExport
{
    public class Vaccine : Mod
    {

        public string nameSpace = MethodBase.GetCurrentMethod().DeclaringType.Namespace;

        public static List<MethodInfo> compatBuildingMethods = new List<MethodInfo>();
        public static List<MethodInfo> compatZone_StockpileMethods = new List<MethodInfo>();

        public Vaccine(ModContentPack modContentPack) : base(modContentPack)
        {
            ECMethods.checkEC();

            replaceGizmo(typeof(Building), typeof(Building_Extend), compatBuildingMethods, BindingFlags.Instance | BindingFlags.Public);

            replaceGizmo(typeof(Zone_Stockpile), typeof(Zone_Stockpile_Extend), compatZone_StockpileMethods, BindingFlags.Instance | BindingFlags.Public);

            //Creates folders if required
            SaveLocation.prime();
        }

        //Class name should be [vanillaClass]_Extend
        //Gizmos should be getGizmos_Extend
        public void replaceGizmo(Type typeVanilla, Type typeReplace, List<MethodInfo> methodList, BindingFlags vanillaFlags)
        {
            Holder holder = new Holder();
            holder = compat(typeReplace.Name, "getGizmos_Extend", methodList);
            if (holder.compatMod == "")
            {
                MethodInfo methodVanilla = typeVanilla.GetMethod("GetGizmos", vanillaFlags);
                MethodInfo methodReplace = typeReplace.GetMethod("GetGizmos", vanillaFlags);
                if (giveAutism(methodVanilla, methodReplace))
                {
                    Log.Message(nameSpace + " probably sucessfully replaced " + typeVanilla.Name + ".GetGizmos(). Code stolen from CCL/RawCode.");
                    foreach (string mod in holder.replace)
                    {
                        Log.Message(nameSpace + ": doing compatibility for " + typeVanilla.Name + ".GetGizmos() for " + mod);
                    }
                }
                else
                {
                    Log.Error(nameSpace + ": failed to replace " + typeVanilla.Name + ".GetGizmos()");
                }
            }
            else
            {
                methodList.Clear();
                Log.Message(nameSpace + ": " + holder.compatMod + " should be doing compatbility for " + typeVanilla.Name + ".GetGizmos()");
            }
        }

        //Check who will do compatibility (last mod in load order) and which mods needs
        public Holder compat(string typeName, string methodName, List<MethodInfo> methodList)
        {
            Holder holder = new Holder();
            int loadOrder = Content.loadOrder;
            foreach (ModContentPack mod in LoadedModManager.RunningMods)
            {
                if (mod.LoadedAnyAssembly)
                {

                    foreach (Assembly ass in mod.assemblies.loadedAssemblies)
                    {

                        foreach (Type type in ass.GetTypes())
                        {
                            if (type.Name == typeName) //If method has correct method compat nomenclature
                            {
                                if (mod.loadOrder > loadOrder) //If mod is loaded after
                                {
                                    holder.compatMod = mod.Name; //That mod will do compatibility
                                    holder.replace.Clear();
                                    methodList.Clear();
                                    loadOrder = mod.loadOrder;
                                }
                                else if (holder.compatMod == "" && mod.Name != Content.Name)//Else this mod will do compatibility
                                {
                                    MethodInfo temp = type.GetMethod(methodName, BindingFlags.Static | BindingFlags.Public);
                                    if (temp != null)
                                    {
                                        methodList.Add(temp);
                                        holder.replace.Add(mod.Name);
                                    }
                                }
                            }

                        } //Type Loop

                    } //Ass Loop

                }
            }
            return holder;
        }

        public class Holder
        {
            public string compatMod = ""; //Will be "" if this mod is doing compatibility
            public List<string> replace = new List<string>();
        }


        //This shit stolen from CCL because I'm a pleb programmer. CCL got code from user RawCode.
        unsafe bool giveAutism(MethodInfo source, MethodInfo destination)
        {
            if (source == null || destination == null)
            {
                return false;
            }
            if (IntPtr.Size == sizeof(Int64))
            {
                // 64-bit systems use 64-bit absolute address and jumps
                // 12 byte destructive

                // Get function pointers
                long Source_Base = source.MethodHandle.GetFunctionPointer().ToInt64();
                long Destination_Base = destination.MethodHandle.GetFunctionPointer().ToInt64();

                // Native source address
                byte* Pointer_Raw_Source = (byte*)Source_Base;

                // Pointer to insert jump address into native code
                long* Pointer_Raw_Address = (long*)(Pointer_Raw_Source + 0x02);

                // Insert 64-bit absolute jump into native code (address in rax)
                // mov rax, immediate64
                // jmp [rax]
                *(Pointer_Raw_Source + 0x00) = 0x48;
                *(Pointer_Raw_Source + 0x01) = 0xB8;
                *Pointer_Raw_Address = Destination_Base; // ( Pointer_Raw_Source + 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09 )
                *(Pointer_Raw_Source + 0x0A) = 0xFF;
                *(Pointer_Raw_Source + 0x0B) = 0xE0;

            }
            else
            {
                // 32-bit systems use 32-bit relative offset and jump
                // 5 byte destructive
                // Get function pointers
                int Source_Base = source.MethodHandle.GetFunctionPointer().ToInt32();
                int Destination_Base = destination.MethodHandle.GetFunctionPointer().ToInt32();

                // Native source address
                byte* Pointer_Raw_Source = (byte*)Source_Base;

                // Pointer to insert jump address into native code
                int* Pointer_Raw_Address = (int*)(Pointer_Raw_Source + 1);

                // Jump offset (less instruction size)
                int offset = (Destination_Base - Source_Base) - 5;

                // Insert 32-bit relative jump into native code
                *Pointer_Raw_Source = 0xE9;
                *Pointer_Raw_Address = offset;

            }
            // done!
            return true;
        }
    }
}
