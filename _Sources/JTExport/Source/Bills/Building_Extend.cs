using RimWorld;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using UnityEngine;
using Verse;
using Verse.Sound;

namespace JTExport
{
	public class Building_Extend : ThingWithComps
    {

		[DebuggerHidden]
		public override IEnumerable<Gizmo> GetGizmos()
		{
            foreach (Gizmo c in base.GetGizmos())
            {
                yield return c;
            }
            if (def.Minifiable && Faction == Faction.OfPlayer)
            {
                yield return InstallationDesignatorDatabase.DesignatorFor(def);
            }
            Command buildCopy = BuildCopyCommandUtility.BuildCopyCommand(def, Stuff);
			if (buildCopy != null)
			{
				yield return buildCopy;
			}

            //Compatibility
            foreach (MethodInfo method in Vaccine.compatBuildingMethods)
            {
                foreach (Gizmo g in (IEnumerable<Gizmo>)method.Invoke(null, new object[] { this }))
                {
                    yield return g;
                }
            }

            foreach (Gizmo g in getGizmos_Extend(this))
            {
                yield return g;
            }

			yield break;
		}

        //Call this through reflection for compatibility
        public static IEnumerable<Gizmo> getGizmos_Extend(object vanillaClass)
        {
            //START of my code
            if (vanillaClass is Building_WorkTable)
            {
                Building_WorkTable wt = (Building_WorkTable)vanillaClass;
                if (wt.def.AllRecipes.Count > 0) //If table has no recipes, no need for button.
                {

                    string defName = wt.def.defName;

                    //Export
                    yield return new Command_Action
                    {
                        icon = ContentFinder<Texture2D>.Get("Export", true),
                        defaultLabel = "Export bills",
                        defaultDesc = "Export " + defName + " bill settings",
                        action = delegate
                        {
                            SoundDefOf.TickHigh.PlayOneShotOnCamera();
                            //BillSettingClipboard.Copy(defName, wt.billStack);
                            Find.WindowStack.Add(new Dialog_Name(defName, SaveLocation.saveBills, delegate(string name)
                            {
                                BillExporter.exportBills(name, wt.billStack);
                            }));
                        },
                        //hotKey = KeyBindingDefOf.Misc4
                    };

                    //Import
                    Command_Action command_Action = new Command_Action();
                    command_Action.icon = ContentFinder<Texture2D>.Get("Import", true);
                    command_Action.defaultLabel = "Import bills";
                    command_Action.defaultDesc = "Import " + defName + " bill settings";

                    string[] files = SaveLocation.findFiles(SaveLocation.saveBills);

                    command_Action.action = delegate
                    {
                        SoundDefOf.TickHigh.PlayOneShotOnCamera();
                        //wt.billStack = BillSettingClipboard.PasteInto(defName, wt.billStack, wt.def.AllRecipes);

                        List<FloatMenuOption> list = new List<FloatMenuOption>();
                        for (int i = 0; i < files.Length; i++)
                        {
                            string name = files[i]; //because some trouble with index
                            list.Add(new FloatMenuOption(name, delegate
                            {
                                if (File.Exists(Path.Combine(SaveLocation.saveBills.FullName, name + ".txt")))
                                {
                                    BillImporter.importBills(name, wt.billStack, wt.def.AllRecipes);
                                } else
                                {
                                    Messages.Message(name + " no longer exists.", MessageSound.Standard);
                                }
                            }, MenuOptionPriority.Default, null, null, 0f, null));
                        }
                        if (list.Count == 0) //should never be true
                        {
                            Log.Error("JTExport: you managed to magic your way past command_Action.Disable");
                        } else
                        {
                            FloatMenu floatMenu = new FloatMenu(list);
                            floatMenu.vanishIfMouseDistant = true;
                            Find.WindowStack.Add(floatMenu);
                        }
                    };
                    if (files.Length == 0)
                    {
                        command_Action.Disable("No " + defName + " bills to import, export first");
                    }
                    //command_Action.hotKey = KeyBindingDefOf.Misc5;

                    yield return command_Action;

                }

            }
            yield break;
            //END of my code
        }

    }
}
