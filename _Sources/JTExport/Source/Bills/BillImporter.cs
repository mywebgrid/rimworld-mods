﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.IO;
using Verse;

namespace JTExport
{
    class BillImporter
    {

        public const int maxBillsPerTable = 15;

        public static void importBills(string filename, BillStack bills, List<RecipeDef> recipes)
        {
            List<Bill> newBills = getBills(bills.billGiver.Map, filename);

            //Should only ever have 15 bills in a stack
            if (!canPasteAll(newBills.Count, bills))
            {
                Messages.Message(canPasteAmount(newBills.Count, bills) + " bills were pasted, max 15 per table", MessageSound.Standard);
            }
            int loops = canPasteAmount(newBills.Count, bills);
            int invalid = 0;
            for (int i = 0; i < loops; i++)
            {
                if (recipes.Contains(newBills[i].recipe)) //Only copies over recipes that it can have; incase later electric vs non have different recipes
                {
                    bills.AddBill(newBills[i]); //Copy again to assign unique ID
                } else
                {
                    invalid++;
                }
            }
            if (invalid > 0)
            {
                Messages.Message(invalid + " bills do not belong to the table", MessageSound.Standard);
            }
        }

        public static List<Bill> getBills(Map map, string filename)
        {
            List<Bill> bills = new List<Bill>();
            int start = 0;
            string[] data = SaveLocation.loadData(Path.Combine(SaveLocation.saveBills.FullName, filename + ".txt"));

            //data.Length - 1 to compensate for double new line in exporter
            //Not needed because File.ReadAllLines(location) doesn't read the last line in a double empty/newline

            for (int i = start; i < data.Length; i++)
            {
                if (data[i] == "")
                {
                    Bill temp = getBill(map, data, start, i, filename);
                    if (temp != null)
                    {
                        bills.Add(temp);
                    }
                    start = i + 1;
                }
            }
            return bills;
        }

        public static Bill getBill(Map map, string[] data, int start, int end, string filename)
        {
            Dictionary<string, string> holder = new Dictionary<string, string>();
            fillDictionary(holder, data, start, end);

            if (holder.ContainsKey(SaveLocation.recipeDef))
            {
                RecipeDef recipeDef = DefDatabase<RecipeDef>.GetNamed(holder[SaveLocation.recipeDef]);
                if (recipeDef != null)
                {
                    Bill_Production bill = null;
                    
                    if (holder.ContainsKey(SaveLocation.type))
                    {
                        if (ECMethods.loaded)
                        {
                            bill = ECMethods.importEC(map, holder, recipeDef);
                        }
                        else if (holder[SaveLocation.type].Contains("Bill_ProductionWithUft"))
                        {
                            bill = new Bill_ProductionWithUft(recipeDef);
                        }
                        else
                        {
                            bill = new Bill_Production(recipeDef);
                        }

                        fillBill(holder, bill);

                        return bill;
                    }
                    Log.Error("JTExport: no type starting at " + start + " in bills " + filename);
                    return null;
                }
                Log.Error("JTExport: can't get recipeDef from database, starting at " + start + " in bills " + filename);
            }
            Log.Error("JTExport: no recipeDef starting at " + start + " in bills " + filename);
            return null;
        }

        public static void fillBill(Dictionary<string, string> holder, Bill_Production bill)
        {
            if (bill == null) //should never run
            {
                Log.Message("JTExport: bill was null");
                return;
            }

            //Bill
            fillBillBase(holder, bill);

            //Bill_Production
            fillProduction(holder, bill);

            //Import CH data
            fillCraftingHysteresis(holder, bill);

            //Import EC data
            fillEnhancedCrafting(holder, bill);

            //A17
            fillAlphaSeventeen(holder, bill);

        }

        public static void fillBillBase(Dictionary<string, string> holder, Bill_Production bill)
        {
            if (holder.ContainsKey(SaveLocation.allowedSkillRange))
            {
                bill.allowedSkillRange = IntRange.FromString(holder[SaveLocation.allowedSkillRange]);
            }
            if (holder.ContainsKey(SaveLocation.suspended))
            {
                bool suspended;
                if (bool.TryParse(holder[SaveLocation.suspended], out suspended))
                {
                    bill.suspended = suspended;
                }
            }
            if (holder.ContainsKey(SaveLocation.ingredientSearchRadius))
            {
                float ingredientSearchRadius;
                if (float.TryParse(holder[SaveLocation.ingredientSearchRadius], out ingredientSearchRadius))
                {
                    bill.ingredientSearchRadius = ingredientSearchRadius;
                }
            }
            if (holder.ContainsKey(SaveLocation.ThingFilter))
            {
                string[] split = holder[SaveLocation.ThingFilter].Split(new string[] { SaveLocation.split2 }, StringSplitOptions.RemoveEmptyEntries);
                fillThingFilter(bill.ingredientFilter, split);
            }
        }
        public static void fillProduction(Dictionary<string, string> holder, Bill_Production bill)
        {
            if (holder.ContainsKey(SaveLocation.repeatCount))
            {
                int repeatCount;
                if (int.TryParse(holder[SaveLocation.repeatCount], out repeatCount))
                {
                    bill.repeatCount = repeatCount;
                }
            }
            if (holder.ContainsKey(SaveLocation.repeatMode))
            {
                BillRepeatModeDef repeatMode = DefDatabase<BillRepeatModeDef>.GetNamedSilentFail(holder[SaveLocation.repeatMode]);
                if (repeatMode != null)
                {
                    bill.repeatMode = repeatMode;
                } else
                {
                    switch (holder[SaveLocation.repeatMode])
                    {
                        case "0":
                            repeatMode = DefDatabase<BillRepeatModeDef>.GetNamedSilentFail("RepeatCount");
                            break;
                        case "1":
                            repeatMode = DefDatabase<BillRepeatModeDef>.GetNamedSilentFail("TargetCount");
                            break;
                        case "2":
                            repeatMode = DefDatabase<BillRepeatModeDef>.GetNamedSilentFail("Forever");
                            break;
                    }
                    if (repeatMode == null)
                    {
                        Log.Error("JTExport: could not match repeatMode for bill " + bill.recipe.defName);
                    }
                    else
                    {
                        Log.Message("JTExport: matched A16 repeatMode enum with A17 def for " + bill.recipe.defName);
                        bill.repeatMode = repeatMode;
                    }
                }
            }
            if (holder.ContainsKey(SaveLocation.storeMode))
            {
                BillStoreModeDef storeMode = DefDatabase<BillStoreModeDef>.GetNamedSilentFail(holder[SaveLocation.storeMode]);
                if (storeMode != null)
                {
                    bill.storeMode = storeMode;
                } else
                {
                    switch (holder[SaveLocation.storeMode])
                    {
                        case "0":
                            storeMode = DefDatabase<BillStoreModeDef>.GetNamedSilentFail("DropOnFloor");
                            break;
                        case "1":
                            storeMode = DefDatabase<BillStoreModeDef>.GetNamedSilentFail("BestStockpile");
                            break;
                    }
                    if (storeMode == null)
                    {
                        Log.Error("JTExport: could not match storeMode for bill " + bill.recipe.defName);
                    }
                    else
                    {
                        Log.Message("JTExport: matched A16 storeMode enum with A17 def for " + bill.recipe.defName);
                    }
                }
            }
            if (holder.ContainsKey(SaveLocation.targetCount))
            {
                int targetCount;
                if (int.TryParse(holder[SaveLocation.targetCount], out targetCount))
                {
                    bill.targetCount = targetCount;
                }
            }
        }
        public static void fillCraftingHysteresis(Dictionary<string, string> holder, Bill_Production bill)
        {
            if (holder.ContainsKey(SaveLocation.paused))
            {
                bool paused;
                if (bool.TryParse(holder[SaveLocation.paused], out paused))
                {
                    bill.paused = paused;
                }
            }
            if (holder.ContainsKey(SaveLocation.pauseOnCompletion))
            {
                bool pauseOnCompletion;
                if (bool.TryParse(holder[SaveLocation.pauseOnCompletion], out pauseOnCompletion))
                {
                    bill.pauseWhenSatisfied = pauseOnCompletion;
                }
            }
            if (holder.ContainsKey(SaveLocation.unpauseThreshold))
            {
                int unpauseThreshold;
                if (int.TryParse(holder[SaveLocation.unpauseThreshold], out unpauseThreshold))
                {
                    bill.unpauseWhenYouHave = unpauseThreshold;
                }
            }
        }
        public static void fillAlphaSeventeen(Dictionary<string, string> holder, Bill_Production bill)
        {
            if (holder.ContainsKey(SaveLocation.pauseWhenSatisfied))
            {
                bool pauseWhenSatisfied;
                if (bool.TryParse(holder[SaveLocation.pauseWhenSatisfied], out pauseWhenSatisfied))
                {
                    bill.pauseWhenSatisfied = pauseWhenSatisfied;
                }
            }
            if (holder.ContainsKey(SaveLocation.unpauseWhenYouHave))
            {
                int unpauseWhenYouHave;
                if (int.TryParse(holder[SaveLocation.unpauseWhenYouHave], out unpauseWhenYouHave))
                {
                    bill.unpauseWhenYouHave = unpauseWhenYouHave;
                }
            }
            if (holder.ContainsKey(SaveLocation.paused))
            {
                bool paused;
                if (bool.TryParse(holder[SaveLocation.paused], out paused))
                {
                    bill.paused = paused;
                }
            }
        }
        public static void fillEnhancedCrafting(Dictionary<string, string> holder, Bill_Production bill)
        {
            if (holder.ContainsKey(SaveLocation.MinCount))
            {
                int MinCount;
                if (int.TryParse(holder[SaveLocation.MinCount], out MinCount))
                {
                    bill.unpauseWhenYouHave = MinCount;
                    if (MinCount < bill.repeatCount)
                    {
                        bill.pauseWhenSatisfied = true;
                    }
                }
            }
            if (holder.ContainsKey(SaveLocation.IsPaused))
            {
                bool IsPaused;
                if (bool.TryParse(holder[SaveLocation.IsPaused], out IsPaused))
                {
                    bill.paused = IsPaused;
                }
            }
        }

        public static void fillThingFilter(ThingFilter thingFilter, string[] data)
        {
            Dictionary<string, string> holder = new Dictionary<string, string>();
            fillDictionary(holder, data, 0, data.Length);

            if (holder.ContainsKey(SaveLocation.allowedHitPointsConfigurable))
            {
                bool allowedHitPointsConfigurable;
                if (bool.TryParse(holder[SaveLocation.allowedHitPointsConfigurable], out allowedHitPointsConfigurable))
                {
                    thingFilter.allowedHitPointsConfigurable = allowedHitPointsConfigurable;
                }
            }
            if (holder.ContainsKey(SaveLocation.allowedQualitiesConfigurable))
            {
                bool allowedQualitiesConfigurable;
                if (bool.TryParse(holder[SaveLocation.allowedQualitiesConfigurable], out allowedQualitiesConfigurable))
                {
                    thingFilter.allowedQualitiesConfigurable = allowedQualitiesConfigurable;
                }
            }
            if (holder.ContainsKey(SaveLocation.AllowedHitPointsPercents))
            {
                thingFilter.AllowedHitPointsPercents = FloatRange.FromString(holder[SaveLocation.AllowedHitPointsPercents]);
            }
            if (holder.ContainsKey(SaveLocation.AllowedQualityLevels))
            {
                thingFilter.AllowedQualityLevels = QualityRange.FromString(holder[SaveLocation.AllowedQualityLevels]);
            }
            if (holder.ContainsKey(SaveLocation.AllowedThingDefs))
            {
                thingFilter.SetDisallowAll();
                string[] thingDefs = holder[SaveLocation.AllowedThingDefs].Split(new string[] { SaveLocation.split3 }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < thingDefs.Length; i++)
                {
                    if (DefDatabase<ThingDef>.GetNamedSilentFail(thingDefs[i]) != null)
                    {
                        thingFilter.SetAllow(DefDatabase<ThingDef>.GetNamedSilentFail(thingDefs[i]), true);
                    }
                }
            }
            if (holder.ContainsKey(SaveLocation.disallowedSpecialFilters))
            {
                foreach (SpecialThingFilterDef def in DefDatabase<SpecialThingFilterDef>.AllDefs)
                {
                    thingFilter.SetAllow(def, true); //This clears disallowedSpecialFilters
                }
                string[] thingDefs = holder[SaveLocation.disallowedSpecialFilters].Split(new string[] { SaveLocation.split3 }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < thingDefs.Length; i++)
                {
                    if (DefDatabase<SpecialThingFilterDef>.GetNamedSilentFail(thingDefs[i]) != null)
                    {
                        thingFilter.SetAllow(DefDatabase<SpecialThingFilterDef>.GetNamedSilentFail(thingDefs[i]), false);
                    }
                }
            }
        }

        //Puts key=value into Dictionary<key,value> holder
        public static void fillDictionary(Dictionary<string, string> holder, string[] data, int start, int end)
        {
            for (int i = start; i < end; i++)
            {
                string[] split = data[i].Split(new string[] { SaveLocation.split }, 2, StringSplitOptions.RemoveEmptyEntries);
                if (split.Length > 1)
                {
                    holder[split[0]] = split[1];
                } else if (split.Length == 1)
                {
                    holder[split[0]] = "";
                }
            }
        }

        public static int canPasteAmount(int amountToPaste, BillStack table)
        {
            return (maxBillsPerTable - table.Bills.Count >= amountToPaste) ? amountToPaste : maxBillsPerTable - table.Bills.Count;
        }

        public static bool canPasteAll(int amountToPaste, BillStack table)
        {
            if (canPasteAmount(amountToPaste, table) == amountToPaste)
            {
                return true;
            }
            return false;
        }

    }
}
