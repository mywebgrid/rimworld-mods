﻿using RimWorld;
using System.Collections.Generic;
using System.IO;
using Verse;

namespace JTExport
{
    static class BillExporter
    {

        ///Bill (split name and value with =)
        ///type
        ///recipeDef.defName
        ///allowedSkillRange.ToString .FromString       min max seperated by ~
        ///suspended.ToString .TryParse(string, out bool)
        ///ingredientSearchRadius (float)
        ///ThingFilter.allowedHitPointsConfigurable.ToString.TryParse|allowedQualitiesConfigurable.ToString.TryParse|AllowedHitPointsPercents.ToString.FromString|AllowedQualityLevels.ToString.FromString|AllowedThingDefs.defName,loop|disallowedSpecialFilters.defName,loop
        ///
        ///Bill_Production
        ///repeatCount (int)
        ///repeatMode (enum BillRepeatMode) as int
        ///storeMode (enum BillStoreMode) as int
        ///targetCount (int)

        public static bool exportBills(string filename, BillStack bills)
        {

            string result = "";

            foreach (Bill bill in bills)
            {
                if (bill is Bill_Production)
                {
                    result += billString((Bill_Production)bill);
                    if (bill.GetType().Namespace == "AlcoholV.Overriding")
                    {
                        result += ECMethods.ECString(bill);
                    }
                } else
                {
                    Log.Error("JTExport: tried to export " + bill.GetType() + " which is not supported.");
                }

                result += SaveLocation.split1;
            }

            return SaveLocation.saveData(Path.Combine(SaveLocation.saveBills.FullName, filename + ".txt"), result, filename);
        }

        public static string billString(Bill_Production b)
        {
            string result = SaveLocation.type + SaveLocation.split + b.GetType() + SaveLocation.split1;
            //Bill
            result += SaveLocation.recipeDef + SaveLocation.split + b.recipe.defName + SaveLocation.split1;
            result += SaveLocation.allowedSkillRange + SaveLocation.split + b.allowedSkillRange.ToString() + SaveLocation.split1;
            result += SaveLocation.suspended + SaveLocation.split + b.suspended.ToString() + SaveLocation.split1;
            result += SaveLocation.ingredientSearchRadius + SaveLocation.split + b.ingredientSearchRadius + SaveLocation.split1;
            result += SaveLocation.ThingFilter + SaveLocation.split + ThingFilterString(b.ingredientFilter) + SaveLocation.split1;

            //Bill_Production
            result += SaveLocation.repeatCount + SaveLocation.split + b.repeatCount + SaveLocation.split1;
            result += SaveLocation.repeatMode + SaveLocation.split + b.repeatMode.defName + SaveLocation.split1;
            result += SaveLocation.storeMode + SaveLocation.split + b.storeMode.defName + SaveLocation.split1;
            result += SaveLocation.targetCount + SaveLocation.split + b.targetCount + SaveLocation.split1;

            //A17
            result += SaveLocation.pauseWhenSatisfied + SaveLocation.split + b.pauseWhenSatisfied + SaveLocation.split1;
            result += SaveLocation.unpauseWhenYouHave + SaveLocation.split + b.unpauseWhenYouHave + SaveLocation.split1;
            result += SaveLocation.paused + SaveLocation.split + b.paused + SaveLocation.split1;

            return result;
        }

        public static string ThingFilterString(ThingFilter tf)
        {
            string result = SaveLocation.allowedHitPointsConfigurable + SaveLocation.split + tf.allowedHitPointsConfigurable.ToString() + SaveLocation.split2;
            result += SaveLocation.allowedQualitiesConfigurable + SaveLocation.split + tf.allowedQualitiesConfigurable.ToString() + SaveLocation.split2;
            result += SaveLocation.AllowedHitPointsPercents + SaveLocation.split + tf.AllowedHitPointsPercents.ToString() + SaveLocation.split2;
            result += SaveLocation.AllowedQualityLevels + SaveLocation.split + tf.AllowedQualityLevels.ToString() + SaveLocation.split2;
            result += SaveLocation.AllowedThingDefs + SaveLocation.split;
            foreach (ThingDef def in tf.AllowedThingDefs)
            {
                result += def.defName + SaveLocation.split3; //There will be extra , at the end, but StringSplitOptions.RemoveEmptyEntries
            }
            result += SaveLocation.split2;
            result += SaveLocation.disallowedSpecialFilters + SaveLocation.split;
            foreach (SpecialThingFilterDef def in getSpecialThings(tf))
            {
                result += def.defName + SaveLocation.split3; //There will be extra , at the end, but StringSplitOptions.RemoveEmptyEntries
            }
            return result;
        }

        public static IEnumerable<SpecialThingFilterDef> getSpecialThings(ThingFilter tf)
        {
            foreach (SpecialThingFilterDef def in DefDatabase<SpecialThingFilterDef>.AllDefs)
            {
                if (!tf.Allows(def))
                {
                    yield return def;
                }
            }
            yield break;
        }

    }
}
