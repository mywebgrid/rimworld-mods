﻿using RimWorld;
using System.Collections.Generic;
using Verse;

namespace JTExport
{
    class ECMethods
    {
        public static bool loaded = false;
        public static int loadOrder = 0;

        public static void checkEC()
        {
            if (!loaded)
            {
                int counter = 0;
                foreach (ModMetaData mod in ModsConfig.ActiveModsInLoadOrder)
                {
                    if (mod.Name.Contains("Enhanced Crafting"))
                    {
                        loaded = true;
                        loadOrder = counter;
                        Log.Message("JTExport: detected AC-Enhanced Crafting");
                        break;
                    }
                    counter++;
                }
            }
        }

        ///Bill_Production_Hysteresis --> WithUft same stuff but doesn't extend each other
        ///_assignedPawn (Pawn)
        ///_name (string)
        ///_minCount (int)
        ///_isPaused (bool)

        public static string ECString(Bill b)
        {
            if (b is AlcoholV.Overriding.Bill_ProductionWithUft)
            {
                AlcoholV.Overriding.Bill_ProductionWithUft temp = (AlcoholV.Overriding.Bill_ProductionWithUft)b;
                string result = "";
                if (temp.AssignedPawn != null)
                {
                    result = SaveLocation.AssignedPawn + SaveLocation.split + temp.AssignedPawn.ThingID + SaveLocation.split1;
                }
                result += SaveLocation.Name + SaveLocation.split + temp.Name + SaveLocation.split1;
                result += SaveLocation.MinCount + SaveLocation.split + temp.MinStock + SaveLocation.split1;
                result += SaveLocation.IsPaused + SaveLocation.split + temp.IsPaused + SaveLocation.split1;
                return result;
            } else if (b is AlcoholV.Overriding.Bill_Production)
            {
                AlcoholV.Overriding.Bill_Production temp = (AlcoholV.Overriding.Bill_Production)b;
                string result = "";
                if (temp.AssignedPawn != null)
                {
                    result = SaveLocation.AssignedPawn + SaveLocation.split + temp.AssignedPawn.ThingID + SaveLocation.split1;
                }
                result += SaveLocation.Name + SaveLocation.split + temp.Name + SaveLocation.split1;
                result += SaveLocation.MinCount + SaveLocation.split + temp.MinStock + SaveLocation.split1;
                result += SaveLocation.IsPaused + SaveLocation.split + temp.IsPaused + SaveLocation.split1;
                return result;
            }
            return "";
        }

        public static Bill_Production importEC(Map map, Dictionary<string, string> holder, RecipeDef recipeDef)
        {
            if (holder[SaveLocation.type].Contains("Bill_ProductionWithUft")) //AlcoholV.Overriding.Bill_ProductionWithUft doesn't extend AlcoholV.Overriding.Bill_Production
            {
                AlcoholV.Overriding.Bill_ProductionWithUft temp = new AlcoholV.Overriding.Bill_ProductionWithUft(recipeDef);
                
                //CH stuff
                if (holder.ContainsKey(SaveLocation.unpauseThreshold))
                {
                    int unpauseThreshold;
                    if (int.TryParse(holder[SaveLocation.unpauseThreshold], out unpauseThreshold))
                    {
                        temp.MinStock = unpauseThreshold;
                    }
                }
                if (holder.ContainsKey(SaveLocation.paused))
                {
                    bool paused;
                    if (bool.TryParse(holder[SaveLocation.paused], out paused))
                    {
                        temp.IsPaused = paused;
                    }
                }

                //A17 stuff
                if (holder.ContainsKey(SaveLocation.unpauseWhenYouHave))
                {
                    int unpauseWhenYouHave;
                    if (int.TryParse(holder[SaveLocation.unpauseWhenYouHave], out unpauseWhenYouHave))
                    {
                        temp.MinStock = unpauseWhenYouHave;
                    }
                }
                if (holder.ContainsKey(SaveLocation.paused))
                {
                    bool paused;
                    if (bool.TryParse(holder[SaveLocation.paused], out paused))
                    {
                        temp.IsPaused = paused;
                    }
                }

                //EC stuff
                if (holder.ContainsKey(SaveLocation.AssignedPawn))
                {
                    foreach (Pawn p in map.mapPawns.FreeColonists)
                    {
                        if (p.ThingID == holder[SaveLocation.AssignedPawn])
                        {
                            temp.AssignedPawn = p;
                            break;
                        }
                    }
                }
                if (holder.ContainsKey(SaveLocation.Name))
                {
                    temp.Name = holder[SaveLocation.Name];
                }
                if (holder.ContainsKey(SaveLocation.MinCount))
                {
                    int MinCount;
                    if (int.TryParse(holder[SaveLocation.MinCount], out MinCount))
                    {
                        temp.MinStock = MinCount;
                    }
                }
                if (holder.ContainsKey(SaveLocation.IsPaused))
                {
                    bool IsPaused;
                    if (bool.TryParse(holder[SaveLocation.IsPaused], out IsPaused))
                    {
                        temp.IsPaused = IsPaused;
                    }
                }

                return temp;
            }
            else
            {
                AlcoholV.Overriding.Bill_Production temp = new AlcoholV.Overriding.Bill_Production(recipeDef);

                //CH stuff
                if (holder.ContainsKey(SaveLocation.unpauseThreshold))
                {
                    int unpauseThreshold;
                    if (int.TryParse(holder[SaveLocation.unpauseThreshold], out unpauseThreshold))
                    {
                        temp.MinStock = unpauseThreshold;
                    }
                }
                if (holder.ContainsKey(SaveLocation.paused))
                {
                    bool paused;
                    if (bool.TryParse(holder[SaveLocation.paused], out paused))
                    {
                        temp.IsPaused = paused;
                    }
                }

                //A17 stuff
                if (holder.ContainsKey(SaveLocation.unpauseWhenYouHave))
                {
                    int unpauseWhenYouHave;
                    if (int.TryParse(holder[SaveLocation.unpauseWhenYouHave], out unpauseWhenYouHave))
                    {
                        temp.MinStock = unpauseWhenYouHave;
                    }
                }
                if (holder.ContainsKey(SaveLocation.paused))
                {
                    bool paused;
                    if (bool.TryParse(holder[SaveLocation.paused], out paused))
                    {
                        temp.IsPaused = paused;
                    }
                }

                //EC stuff
                if (holder.ContainsKey(SaveLocation.AssignedPawn))
                {
                    foreach (Pawn p in map.mapPawns.FreeColonists)
                    {
                        if (p.ThingID == holder[SaveLocation.AssignedPawn])
                        {
                            temp.AssignedPawn = p;
                            break;
                        }
                    }
                }
                if (holder.ContainsKey(SaveLocation.Name))
                {
                    temp.Name = holder[SaveLocation.Name];
                }
                if (holder.ContainsKey(SaveLocation.MinCount))
                {
                    int MinCount;
                    if (int.TryParse(holder[SaveLocation.MinCount], out MinCount))
                    {
                        temp.MinStock = MinCount;
                    }
                }
                if (holder.ContainsKey(SaveLocation.IsPaused))
                {
                    bool IsPaused;
                    if (bool.TryParse(holder[SaveLocation.IsPaused], out IsPaused))
                    {
                        temp.IsPaused = IsPaused;
                    }
                }

                return temp;
            }
        }

    }
}
