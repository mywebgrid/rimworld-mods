﻿using RimWorld;
using System;
using UnityEngine;
using Verse;

namespace JTExport
{
    class Dialog_Overwrite : Window
    {
        public string curName;
        public Action<string> action;
        public Dialog_Name previous;

        public override Vector2 InitialSize
        {
            get
            {
                return new Vector2(280f, 175f);
            }
        }

        public Dialog_Overwrite(string curName, Action<string> action, Dialog_Name previous)
        {
            //forcePause = true;
            doCloseX = true;
            closeOnEscapeKey = true;
            absorbInputAroundWindow = true;
            closeOnClickedOutside = true;

            this.curName = curName;
            this.action = action;
            this.previous = previous;
        }

        public override void DoWindowContents(Rect inRect)
        {
            Widgets.Label(new Rect(0f, 0f, inRect.width, 40f), curName + ".txt already exists.");
            Widgets.Label(new Rect(0f, 40f, inRect.width, 40f), "Are you sure you want to overwrite this present?");

            Text.Font = GameFont.Small;
            bool flag = false;
            if (Event.current.type == EventType.KeyDown && Event.current.keyCode == KeyCode.Return)
            {
                flag = true;
                Event.current.Use();
            }

            float buttonWidth = 120;
            if (Widgets.ButtonText(new Rect(0f, inRect.height - 35f - 15f, buttonWidth, 35f), "YES", true, false, true) || flag)
            {
                action(curName);
                Find.WindowStack.TryRemove(previous, true);
                Find.WindowStack.TryRemove(this, true);
            }

            if (Widgets.ButtonText(new Rect(buttonWidth + 10f, inRect.height - 35f - 15f, buttonWidth, 35f), "NO", true, false, true))
            {
                Find.WindowStack.TryRemove(this, true);
            }
        }

    }
}
