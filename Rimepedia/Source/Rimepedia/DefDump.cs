﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text.Json;
using Verse;

namespace Rimepedia
{
    public class DefDump
    {

        private void dumpGeneralDef(Def def)
        {
            var d = new Dictionary<string, string>
            {
                {"defName", def.defName},
                {"label", def.label},
                {"description", def.description},
                {"defName", def.defName},
            };
        }
        
        
        private void dumpThingCategoryDef(ThingCategoryDef def)
        {
                
        }

        private void dump()
        {
            var thingCategoryDefs = DefDatabase<ThingCategoryDef>.AllDefsListForReading;
            string jsonOutput = JsonSerializer.Serialize(thingCategoryDefs);
            File.WriteAllText("./Mods/thingCategoryDefs.json", jsonOutput);
        }
    }
}