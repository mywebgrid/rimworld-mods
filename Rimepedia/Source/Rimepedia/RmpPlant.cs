using Verse;

namespace Rimepedia
{
    public class RmpPlant
    {
        public string label { get; set; }
        public float growDays { get; set; }
        public float harvestYield { get; set; }
        public float lightLevel { get; set; }
        public ThingDef harvested { get; set; }
        public float harvestNutrition { get; set; }
        public float nph { get; set; }
        public float nphd { get; set; }
        
        public ThingDef thing { get; set; }

        public RmpPlant()
        {
            growDays = 0;
            harvestYield = 0;
            lightLevel = 0;
            harvested = null;
            harvestNutrition = 0;
            nph = 0;
            nphd = 0;
        }

        public float Nph()
        {
            return harvestNutrition * harvestYield;
        }
        
        public float Nphd()
        {
            return harvestNutrition * harvestYield / growDays;
        }
    }
}