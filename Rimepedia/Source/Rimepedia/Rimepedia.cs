﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;
using Verse;
using RimWorld;

namespace Rimepedia
{
    public class MainTabWindowRimepedia : MainTabWindow
    {
        private const int ROW_HEIGHT = 30;
        private const int STAT_WIDTH = 150;
        private const int ICON_WIDTH = 29;
        private const int LABEL_WIDTH = 200;

        private string sortProperty = "label";
        private string sortOrder = "ASC";

        public Vector2 scrollPosition = Vector2.zero;
        private float scrollViewHeight;
        private float tableHeight;

        private bool isDirty = true;
        private int listUpdateNext = 0;

        private List<RmpMaterial> baseMaterialList;
        private List<RmpMaterial> materialList;
        private int baseMaterialsCount = 0;
        private int materialsCount = 0;

        private List<StuffCategoryDef> stuffCategoryList;
        private List<StuffCategoryDef> chosenStuffCategoryList;
        private bool[] stuffCheckboxes;

        private List<RmpPlant> basePlantList;
        private List<RmpPlant> plantList;
        private int basePlantCount = 0;
        private int plantCount = 0;

        private List<RmpAnimal> baseAnimalList;
        private List<RmpAnimal> animalList;
        private int baseAnimalCount = 0;
        private int animalCount = 0;

        private List<RmpApparel> baseApparelList;
        private List<RmpApparel> apparelList;
        private int baseApparelCount = 0;
        private int apparelCount = 0;

        private enum RimepediaTab : byte
        {
            Materials,
            Plants,
            Animals,
            Dump
        }

        private struct colDef
        {
            public string label;
            public string property;

            public colDef(string l, string p)
            {
                label = l;
                property = p;
            }
        }

        private RimepediaTab curTab = RimepediaTab.Materials;

        public MainTabWindowRimepedia()
        {
            // Materials
            stuffCategoryList = DefDatabase<StuffCategoryDef>.AllDefs.ToList();
            chosenStuffCategoryList = DefDatabase<StuffCategoryDef>.AllDefs.ToList();
            stuffCheckboxes = Enumerable.Repeat(true, stuffCategoryList.Count()).ToArray();
            baseMaterialList = new List<RmpMaterial>();
            RmpMaterial mat;
            var resources = DefDatabase<ThingDef>.AllDefs.Where<ThingDef>((Func<ThingDef, bool>)(def => def.CountAsResource));
            foreach (var resource in resources)
            {
                if (resource.IsStuff)
                {
                    mat = new RmpMaterial();
                    mat.label = resource.label;
                    mat.thing = resource;
                    if (resource.stuffProps.statOffsets != null)
                    {
                        foreach (var so in resource.stuffProps.statOffsets)
                        {
                            if (so.stat == StatDefOf.Beauty)
                            {
                                mat.beautyOffset = so.value;
                            }
                            else if (Prefs.DevMode)
                            {
                                Log.Message("Unknown offset: " + so.stat.ToString());
                            }
                        }
                    }

                    if (resource.stuffProps.statFactors != null)
                    {
                        foreach (var sf in resource.stuffProps.statFactors)
                        {
                            if (sf.stat == StatDefOf.Beauty)
                            {
                                mat.beautyFactor = sf.value;
                            }
                            else if (sf.stat == StatDefOf.MaxHitPoints)
                            {
                                mat.hpFactor = sf.value;
                            }
                            else if (sf.stat == StatDefOf.Flammability)
                            {
                                mat.flammability = sf.value;
                            }
                            else if (Prefs.DevMode)
                            {
                                Log.Message("Unknown factor: " + sf.stat.ToString());
                            }
                        }
                    }

                    mat.categoryDefs = resource.stuffProps.categories;
                    baseMaterialList.Add(mat);
                    baseMaterialsCount++;
                }
            }

            // Plants
            basePlantList = new List<RmpPlant>();
            plantList = new List<RmpPlant>();
            basePlantCount = 0;
            RmpPlant tmpPlant;
            var plants = DefDatabase<ThingDef>.AllDefs.Where<ThingDef>((Func<ThingDef, bool>)(def =>
                def.plant != null && def.plant.sowTags.Any() && def.plant.harvestedThingDef != null));
            foreach (ThingDef p in plants)
            {
                tmpPlant = new RmpPlant();
                tmpPlant.label = p.label;
                tmpPlant.thing = p;
                tmpPlant.growDays = p.plant.growDays;
                tmpPlant.harvestYield = p.plant.harvestYield;
                tmpPlant.harvested = p.plant.harvestedThingDef;
                tmpPlant.lightLevel = p.plant.growMinGlow * 100;
                basePlantList.Add(tmpPlant);
                plantList.Add(tmpPlant);
                basePlantCount++;
            }

            plantCount = basePlantCount;

            // Animals
            baseAnimalList = new List<RmpAnimal>();
            animalList = new List<RmpAnimal>();
            baseAnimalCount = 0;
            RmpAnimal tmpAnimal;
            var animals = DefDatabase<ThingDef>.AllDefs.Where<ThingDef>((Func<ThingDef, bool>)(def =>
                def.race != null));
            ThingDef leatherdef;
            ThingDef meatdef;
            foreach (ThingDef p in animals)
            {
                if (p.race.leatherDef != null)
                {
                    leatherdef = p.race.leatherDef;
                }
                else if (p.race.useLeatherFrom?.race.leatherDef != null)
                {
                    leatherdef = p.race.useLeatherFrom.race.leatherDef;
                }
                else
                {
                    leatherdef = null;
                }

                if (p.race.meatDef != null)
                {
                    meatdef = p.race.meatDef;
                }
                else if (p.race.useMeatFrom?.race.meatDef != null)
                {
                    meatdef = p.race.useMeatFrom.race.meatDef;
                }
                else
                {
                    meatdef = null;
                }

                tmpAnimal = new RmpAnimal { label = p.label, thing = p, leather = leatherdef, meat = meatdef };
                baseAnimalList.Add(tmpAnimal);
                animalList.Add(tmpAnimal);
                baseAnimalCount++;
            }

            animalCount = baseAnimalCount;

            // Apparel
            baseApparelList = new List<RmpApparel>();
            apparelList = new List<RmpApparel>();
            baseApparelCount = 0;
            RmpApparel tmpApparel;
            var apparels = DefDatabase<ThingDef>.AllDefs.Where<ThingDef>((Func<ThingDef, bool>)(def => def.IsApparel));

            apparelCount = baseApparelCount;
        }

        private int GetStartingWidth()
        {
            return 2 + ICON_WIDTH;
        }

        private void DrawCommon(int num, float w)
        {
            int fnum = num;
            if (num == -1)
            {
                fnum = 0;
            }

            GUI.color = new Color(1f, 1f, 1f, 0.2f);
            Widgets.DrawLineHorizontal(0, ROW_HEIGHT * (fnum + 1), w);
            GUI.color = Color.white;
            Rect rowRect = new Rect(0, ROW_HEIGHT * num, w, ROW_HEIGHT);
            if (num > -1)
            {
                if (Mouse.IsOver(rowRect))
                {
                    GUI.DrawTexture(rowRect, TexUI.HighlightTex);
                }
            }
        }

        private void printCell(string content, int rowNum, int x, int width = STAT_WIDTH, string tooltip = "")
        {
            Rect tmpRec = new Rect(x, ROW_HEIGHT * rowNum + 3, width, ROW_HEIGHT - 3);
            Widgets.Label(tmpRec, content);
            if (!string.IsNullOrEmpty(tooltip))
            {
                TooltipHandler.TipRegion(tmpRec, tooltip);
            }
        }

        private void printThingCell(ThingDef content, string prefix, int rowNum, int x, int width = STAT_WIDTH, string tooltip = "")
        {
            Rect tmpRec = new Rect(x + ICON_WIDTH, ROW_HEIGHT * rowNum + 3, width, ROW_HEIGHT - 3);
            if (content != null)
            {
                Rect icoRect = new Rect(x, ROW_HEIGHT * rowNum, ICON_WIDTH, ICON_WIDTH);
                Widgets.ThingIcon(icoRect, content);
                Widgets.Label(tmpRec, prefix + " " + content.label);
            }
            else
            {
                Widgets.Label(tmpRec, "-");
            }

            if (!string.IsNullOrEmpty(tooltip))
            {
                TooltipHandler.TipRegion(tmpRec, tooltip);
            }
        }

        private void printCellSort(RimepediaTab page, string sortProperty, string content, int x, int width = STAT_WIDTH)
        {
            Rect tmpRec = new Rect(x, 2, width, ROW_HEIGHT - 2);
            Widgets.Label(tmpRec, content);
            if (Mouse.IsOver(tmpRec))
            {
                GUI.DrawTexture(tmpRec, TexUI.HighlightTex);
            }

            if (Widgets.ButtonInvisible(tmpRec))
            {
                if (this.sortProperty == sortProperty)
                {
                    this.sortOrder = this.sortOrder == "ASC" ? "DESC" : "ASC";
                }
                else
                {
                    this.sortProperty = sortProperty;
                }

                this.isDirty = true;
            }

            if (this.sortProperty == sortProperty)
            {
                Texture2D texture2D = (this.sortOrder == "ASC")
                    ? ContentFinder<Texture2D>.Get("UI/Icons/Sorting", true)
                    : ContentFinder<Texture2D>.Get("UI/Icons/SortingDescending", true);
                Rect p = new Rect(tmpRec.xMax - (float)texture2D.width - 30, tmpRec.yMax - (float)texture2D.height - 1, (float)texture2D.width,
                    (float)texture2D.height);
                GUI.DrawTexture(p, texture2D);
            }
        }

        private void printCell(float content, int rowNum, int x, int width = STAT_WIDTH)
        {
            printCell(content.ToString(), rowNum, x, width);
        }

        private void printCell(double content, int rowNum, int x, int width = STAT_WIDTH, string unit = "")
        {
            printCell(content.ToString() + unit, rowNum, x, width);
        }

        private void PrintAutoCheckbox(string text, ref bool value, ref float currentX, ref float currentY, bool defaultValue = false)
        {
            var textWidth = Text.CalcSize(text).x + 25f;
            if (currentX > this.windowRect.width - 25)
            {
                currentX = 0f;
                currentY = currentY + Text.CalcSize(text).y + 15f;
            }

            Widgets.CheckboxLabeled(new Rect(currentX, currentY, textWidth, 30), text, ref value, defaultValue);
            currentX += textWidth + 25f;
        }

        private int DrawCommonButtons(int x, int rowNum, float rowWidth, ThingDef t)
        {
            Widgets.InfoCardButton(rowWidth - 20, ROW_HEIGHT * rowNum, t);
            Rect icoRect = new Rect(0, ROW_HEIGHT * rowNum, ICON_WIDTH, ICON_WIDTH);
            Widgets.ThingIcon(icoRect, t);
            return ICON_WIDTH + 2;
        }

        private void DrawMaterialRow(RmpMaterial t, int num, float w)
        {
            DrawCommon(num, w);
            int ww = 0;
            ww = this.DrawCommonButtons(ww, num, w, t.thing);
            printCell(t.label, num, ww, LABEL_WIDTH);
            ww += LABEL_WIDTH;
            printCell(t.beautyFactor * 100 + "%", num, ww);
            ww += STAT_WIDTH;
            printCell("+" + t.beautyOffset, num, ww);
            ww += STAT_WIDTH;
            printCell(t.hpFactor * 100 + "%", num, ww);
            ww += STAT_WIDTH;
            printCell(t.flammability * 100 + "%", num, ww);
            ww += STAT_WIDTH;
        }

        private void DrawPlantRow(RmpPlant t, int num, float w)
        {
            DrawCommon(num, w);
            int ww = 0;
            ww = this.DrawCommonButtons(ww, num, w, t.thing);
            printCell(t.label, num, ww, LABEL_WIDTH);
            ww += LABEL_WIDTH;
            printCell(t.growDays, num, ww);
            ww += STAT_WIDTH;
            printThingCell(t.harvested, t.harvestYield + "x", num, ww);
            ww += (STAT_WIDTH * 2);
            printCell(t.lightLevel, num, ww);
            ww += (STAT_WIDTH);
        }

        private void DrawAnimalRow(RmpAnimal t, int num, float w)
        {
            DrawCommon(num, w);
            int ww = 0;
            ww = this.DrawCommonButtons(ww, num, w, t.thing);
            printCell(t.label, num, ww, LABEL_WIDTH);
            ww += LABEL_WIDTH;
            printThingCell(t.leather, "", num, ww);
            ww += STAT_WIDTH;
            printThingCell(t.meat, "", num, ww);
            ww += STAT_WIDTH;
        }

        private void UpdateList()
        {
            if (curTab == RimepediaTab.Materials)
            {
                materialList = new List<RmpMaterial>();
                materialsCount = 0;

                foreach (var rmp in baseMaterialList)
                {
                    if (rmp.categoryDefs.Intersect(chosenStuffCategoryList).Any())
                    {
                        materialList.Add(rmp);
                        materialsCount++;
                    }
                }

                System.Reflection.PropertyInfo pir;
                pir = typeof(RmpMaterial).GetProperty(this.sortProperty);
                if (pir == null)
                {
                    this.sortProperty = "label";
                    pir = typeof(RmpMaterial).GetProperty(this.sortProperty);
                }

                materialList = this.sortOrder == "DESC"
                    ? materialList.OrderByDescending(o => pir.GetValue(o, null)).ToList()
                    : materialList.OrderBy(o => pir.GetValue(o, null)).ToList();
            }
            else if (curTab == RimepediaTab.Plants)
            {
                System.Reflection.PropertyInfo pir;
                pir = typeof(RmpPlant).GetProperty(this.sortProperty);
                if (pir == null)
                {
                    this.sortProperty = "label";
                    pir = typeof(RmpPlant).GetProperty(this.sortProperty);
                }

                plantList = this.sortOrder == "DESC"
                    ? plantList.OrderByDescending(o => pir.GetValue(o, null)).ToList()
                    : plantList.OrderBy(o => pir.GetValue(o, null)).ToList();
            }
            else if (curTab == RimepediaTab.Animals)
            {
                System.Reflection.PropertyInfo pir;
                pir = typeof(RmpAnimal).GetProperty(this.sortProperty);
                if (pir == null)
                {
                    this.sortProperty = "label";
                    pir = typeof(RmpAnimal).GetProperty(this.sortProperty);
                }

                animalList = this.sortOrder == "DESC"
                    ? animalList.OrderByDescending(o => pir.GetValue(o, null)).ToList()
                    : animalList.OrderBy(o => pir.GetValue(o, null)).ToList();
            }

            isDirty = false;
        }

        private void DoMaterialsPage(Rect rect)
        {
            int oldCount = stuffCheckboxes.Count(c => c);

            float currentX = rect.x;
            float currentY = rect.y + 10;
            int i = 0;
            chosenStuffCategoryList.Clear();
            foreach (var stuffChb in stuffCategoryList.ToList())
            {
                if (currentX > rect.width - 150)
                {
                    currentX = 0;
                    currentY += 30;
                }

                PrintAutoCheckbox(stuffChb.label, ref stuffCheckboxes[i], ref currentX, ref currentY);
                if (stuffCheckboxes[i])
                {
                    chosenStuffCategoryList.Add(stuffChb);
                }

                i++;
            }

            if (Widgets.ButtonText(new Rect(currentX + 10, currentY, 30, 30), "X"))
            {
                chosenStuffCategoryList.Clear();
                isDirty = true;
                stuffCheckboxes = Enumerable.Repeat(false, stuffCategoryList.Count()).ToArray();
            }

            int newCount = stuffCheckboxes.Count(c => c);

            if (oldCount != newCount)
            {
                isDirty = true;
            }

            colDef[] headers =
            {
                new colDef("Beauty factor", "beautyFactor"),
                new colDef("Beauty offset", "beautyOffset"),
                new colDef("HP", "hpFactor"),
                new colDef("Flammability", "flammability")
            };
            rect.y = currentY + 30f;
            GUI.BeginGroup(rect);
            tableHeight = materialsCount * ROW_HEIGHT;
            Rect inRect = new Rect(0, 0, rect.width - 16, tableHeight + 100);
            int num = 0;
            int ww = GetStartingWidth();
            DrawCommon(-1, inRect.width);
            printCellSort(RimepediaTab.Materials, "label", "Name", ww, LABEL_WIDTH);
            ww += LABEL_WIDTH;
            foreach (colDef h in headers)
            {
                printCellSort(RimepediaTab.Materials, h.property, h.label, ww);
                ww += STAT_WIDTH;
            }

            Rect scrollRect = new Rect(0, ROW_HEIGHT + 5, rect.width, rect.height);
            Widgets.BeginScrollView(scrollRect, ref scrollPosition, inRect);
            foreach (RmpMaterial m in materialList)
            {
                DrawMaterialRow(m, num, inRect.width);
                num++;
            }

            Widgets.EndScrollView();
            GUI.EndGroup();
        }

        private void DoPlantsPage(Rect rect)
        {
            colDef[] headers =
            {
                new colDef("Days", "growDays"),
                new colDef("Yield", "harvestYield"),
                new colDef("Min. light", "lightLevel")
            };
            rect.y += 30;
            GUI.BeginGroup(rect);
            tableHeight = plantCount * ROW_HEIGHT;
            Rect inRect = new Rect(0, 0, rect.width - 16, tableHeight + 100);
            int num = 0;
            int ww = GetStartingWidth();
            DrawCommon(-1, inRect.width);
            printCellSort(RimepediaTab.Plants, "label", "Name", ww, LABEL_WIDTH);
            ww += LABEL_WIDTH;
            printCellSort(RimepediaTab.Plants, "growDays", "Days", ww);
            ww += STAT_WIDTH;
            printCellSort(RimepediaTab.Plants, "harvestYield", "Yield", ww);
            ww += STAT_WIDTH * 2;
            printCellSort(RimepediaTab.Plants, "lightLevel", "Min. light", ww);
            ww += STAT_WIDTH;

            Rect scrollRect = new Rect(rect.x, rect.y - 35, rect.width, rect.height);
            Widgets.BeginScrollView(scrollRect, ref scrollPosition, inRect);
            foreach (RmpPlant m in plantList)
            {
                DrawPlantRow(m, num, inRect.width);
                num++;
            }

            Widgets.EndScrollView();
            GUI.EndGroup();
        }

        private void DoAnimalsPage(Rect rect)
        {
            colDef[] headers =
            {
                new colDef("Leather", "leatherAmount"),
                new colDef("Meat", "meatAmount"),
            };
            rect.y += 30;
            GUI.BeginGroup(rect);
            tableHeight = animalCount * ROW_HEIGHT;
            Rect inRect = new Rect(0, 0, rect.width - 16, tableHeight + 100);
            int num = 0;
            int ww = GetStartingWidth();
            DrawCommon(-1, inRect.width);
            printCellSort(RimepediaTab.Animals, "label", "Name", ww, LABEL_WIDTH);
            ww += LABEL_WIDTH;
            foreach (colDef h in headers)
            {
                printCellSort(RimepediaTab.Animals, h.property, h.label, ww);
                ww += STAT_WIDTH;
            }

            Rect scrollRect = new Rect(rect.x, rect.y - 35, rect.width, rect.height);
            Widgets.BeginScrollView(scrollRect, ref scrollPosition, inRect);
            foreach (RmpAnimal m in animalList)
            {
                DrawAnimalRow(m, num, inRect.width);
                num++;
            }

            Widgets.EndScrollView();
            GUI.EndGroup();
        }

        private string SerializeDictionary(Dictionary<string, string> d, bool valueIsString = true)
        {
            IEnumerable<string> entries = null;
            if (valueIsString)
            {
                entries = d.Select(e => $"\"{e.Key}\":\"{e.Value}\"");
            }
            else
            {
                entries = d.Select(e => $"\"{e.Key}\":{e.Value}");
            }

            return "{" + string.Join(",", entries) + "}";
        }

        private string SerializeList(List<Dictionary<string, string>> l, bool valueIsString = true)
        {
            var entries = from e in l select SerializeDictionary(e, valueIsString);
            return "[" + string.Join(",", entries) + "]";
        }

        private bool IsBasicCategory(StatCategoryDef sc, bool isPawn = false)
        {
            if (isPawn)
            {
                return sc == StatCategoryDefOf.Basics
                       || sc == StatCategoryDefOf.BasicsImportant
                       || sc == StatCategoryDefOf.BasicsPawn
                       || sc == StatCategoryDefOf.BasicsPawnImportant
                       || sc == StatCategoryDefOf.PawnCombat
                       || sc == StatCategoryDefOf.PawnMisc
                       || sc == StatCategoryDefOf.PawnSocial
                       || sc == StatCategoryDefOf.PawnWork;
            }

            return sc == StatCategoryDefOf.Basics
                   || sc == StatCategoryDefOf.BasicsImportant
                   || sc == StatCategoryDefOf.BasicsNonPawn
                   || sc == StatCategoryDefOf.BasicsNonPawnImportant;
        }

        private Dictionary<string, string> InitDefDictionary(Def d)
        {
            return new Dictionary<string, string>
            {
                ["defName"] = d.defName,
                ["label"] = d.label != null ? d.label.Replace("\"", "") : "",
                ["modId"] = d.modContentPack?.PackageId ?? "unknown",
                ["modName"] = d.modContentPack?.Name != null ? d.modContentPack.Name.Replace("\"", "") : "unknown",
            };
        }

        private string EscapeString(string str)
        {
            return str;
        }

        private void DoDumpPage(Rect rect)
        {
            rect.y += 30;
            GUI.BeginGroup(rect);
            var techLevelDict = new Dictionary<TechLevel, string>()
            {
                { TechLevel.Neolithic, "Neolithic" },
                { TechLevel.Medieval, "Medieval" },
                { TechLevel.Industrial, "Industrial" },
                { TechLevel.Spacer, "Spacer" },
                { TechLevel.Ultra, "Ultra" },
                { TechLevel.Archotech, "Archotech" }
            };
            IEnumerable<ResearchProjectDef> tlAll = null;
            IEnumerable<ResearchProjectDef> tlResearched = null;
            var tlPerc = 0.0;
            var rectY = 50;
            var tlAllCount = 0;
            var tlResearchedCount = 0;
            foreach (var tl in techLevelDict)
            {
                tlAll = DefDatabase<ResearchProjectDef>.AllDefsListForReading.Where(rpd => rpd.techLevel == tl.Key);
                tlResearched = tlAll.Where(rpd => rpd.IsFinished);
                tlAllCount = tlAll.Count();
                if (tlAllCount == 0)
                {
                    tlPerc = 0;
                    tlResearchedCount = 0;
                }
                else
                {
                    tlResearchedCount = tlResearched.Count();
                    tlPerc = Math.Round((float)tlResearchedCount * 100 / tlAllCount, 1);
                }
                Widgets.Label(new Rect(50, rectY, 500, 30), tl.Value + ": " + tlPerc + "% ( " + tlResearchedCount + " / " + tlAllCount + " )");
                rectY += 50;
            }
            
            
            
            
            if (Widgets.ButtonText(new Rect(165, 0, 50, 30), "Dump"))
            {
                var result = new Dictionary<string, string>();
                Dictionary<string, string> q = null;
                var list = new List<Dictionary<string, string>>();
            
                // STAT CATEGORY DEF
                foreach (var s in DefDatabase<StatCategoryDef>.AllDefsListForReading)
                {
                    q = InitDefDictionary(s);
                    list.Add(q);
                }
            
                result["stat_category"] = SerializeList(list);
                list.Clear();
                // END OF STAT CATEGORY DEF
            
                // STAT DEF
                foreach (var s in DefDatabase<StatDef>.AllDefsListForReading)
                {
                    q = InitDefDictionary(s);
                    q["category"] = s.category.defName;
                    list.Add(q);
                }
            
                result["stat"] = SerializeList(list);
                list.Clear();
                // END OF STAT DEF
            
                // PAWN KIND DEF
                foreach (var p in DefDatabase<PawnKindDef>.AllDefsListForReading)
                {
                    q = InitDefDictionary(p);
                    q["race"] = p.race.defName;
                    q["weapon_tags"] = p.weaponTags != null ? string.Join("|", p.weaponTags) : "";
                    q["apparel_tags"] = p.apparelTags != null ? string.Join("|", p.apparelTags) : "";
                    q["apparel_money"] = p.apparelMoney.ToString();
                    q["weapon_money"] = p.weaponMoney.ToString();
                    list.Add(q);
                }
            
                result["pawn_kind"] = SerializeList(list);
                list.Clear();
                // END OF PAWN KIND DEF
            
                // FACTION DEF
                foreach (var f in DefDatabase<FactionDef>.AllDefsListForReading)
                {
                    q = InitDefDictionary(f);
                    list.Add(q);
                }
            
                result["faction"] = SerializeList(list);
                list.Clear();
                // END OF FACTION DEF
            
                // THING DEF
                StatCategoryDef statCategory = null;
                var isPawn = false;
                foreach (var t in DefDatabase<ThingDef>.AllDefsListForReading)
                {
                    q = InitDefDictionary(t);
                    switch (t.category)
                    {
                        case ThingCategory.None:
                            continue;
                        case ThingCategory.Pawn:
                            isPawn = true;
                            q["type"] = "pawn";
                            break;
                        case ThingCategory.Item:
                            if (t.IsApparel)
                            {
                                q["type"] = "apparel";
                                q["apparel_tags"] = t.apparel?.tags != null ? string.Join("|", t.apparel.tags) : "";
                                statCategory = StatCategoryDefOf.Apparel;
                            }
                            else if (t.IsRangedWeapon)
                            {
                                q["type"] = "ranged";
                                q["weapon_tags"] = t.weaponTags != null ? string.Join("|", t.weaponTags) : "";
                                statCategory = StatCategoryDefOf.Weapon;
                            }
                            else if (t.IsMeleeWeapon && !t.IsStuff && !t.IsIngestible && !t.IsDrug)
                            {
                                q["type"] = "melee";
                                q["weapon_tags"] = t.weaponTags != null ? string.Join("|", t.weaponTags) : "";
                                statCategory = StatCategoryDefOf.Weapon;
                            }
                            else
                            {
                                continue;
                            }
            
                            break;
                        case ThingCategory.Building:
                            q["type"] = "building";
                            break;
                        case ThingCategory.Plant:
                            q["type"] = "plant";
                            break;
                        case ThingCategory.Projectile:
                            continue;
                        case ThingCategory.Filth:
                            continue;
                        case ThingCategory.Gas:
                            continue;
                        case ThingCategory.Attachment:
                            continue;
                        case ThingCategory.Mote:
                            continue;
                        case ThingCategory.Ethereal:
                            continue;
                        case ThingCategory.PsychicEmitter:
                            continue;
                        default:
                            continue;
                    }
            
                    if (t.uiIcon != null && false)
                    {
                        var tmp = RenderTexture.GetTemporary(
                            t.uiIcon.width,
                            t.uiIcon.height,
                            0,
                            RenderTextureFormat.Default,
                            RenderTextureReadWrite.Linear);
                        Graphics.Blit(t.uiIcon, tmp);
                        var previous = RenderTexture.active;
                        RenderTexture.active = tmp;
                        var myTexture2D = new Texture2D(t.uiIcon.width, t.uiIcon.height);
                        myTexture2D.ReadPixels(new Rect(0, 0, tmp.width, tmp.height), 0, 0);
                        myTexture2D.Apply();
                        RenderTexture.active = previous;
                        RenderTexture.ReleaseTemporary(tmp);
                        q["icon"] = Convert.ToBase64String(myTexture2D.EncodeToPNG());
                    }
                    else
                    {
                        q["icon"] = "";
                    }
            
                    if (q["type"] == "apparel" || q["type"] == "ranged" || q["type"] == "melee")
                    {
                        var madeThing = t.MadeFromStuff ? ThingMaker.MakeThing(t, GenStuff.DefaultStuffFor(t)) : ThingMaker.MakeThing(t);
                        foreach (var s in DefDatabase<StatDef>.AllDefsListForReading.Where(df => (df.category == statCategory || IsBasicCategory(df.category, isPawn))))
                        {
                            if (!s.Worker.IsDisabledFor(madeThing))
                            {
                                try
                                {
                                    q["stat|" + s.defName] = madeThing.GetStatValue(s).ToString(CultureInfo.InvariantCulture);
                                }
                                catch (NullReferenceException e)
                                {
                                    Log.Message(t.defName + "|" + madeThing.ToString() + "|" + s.defName + " NULL");
                                }
                                catch (System.ArgumentOutOfRangeException e)
                                {
                                    Log.Message(t.defName + "|" + madeThing.ToString() + "|" + s.defName + " OOB");
                                }
                                catch (System.InvalidCastException e)
                                {
                                    Log.Message(t.defName + "|" + madeThing.ToString() + "|" + s.defName + " CAST");
                                }
                            }
                        }
            
                        q["recipeUsers"] = "";
                        if (t.recipeMaker?.recipeUsers != null)
                        {
                            var recipeUsers = "";
                            foreach (var recipeUser in t.recipeMaker.recipeUsers)
                            {
                                recipeUsers += recipeUser.defName + "|";
                            }
            
                            q["recipeUsers"] = recipeUsers;
                        }
                    }
            
                    q["techLevel"] = t.techLevel.ToString();
            
                    list.Add(q);
                }
            
                result["thing"] = SerializeList(list);
                list.Clear();
                // END OF THING DEF
            
                // SAVE TO FILE
                var folderInfo = typeof(GenFilePaths).GetMethod("FolderUnderSaveData", BindingFlags.NonPublic | BindingFlags.Static);
                if (!(folderInfo is null))
                {
                    var folder = (string)folderInfo.Invoke(null, new object[] { "dumps" });
                    var fullPath = Path.Combine(folder, "datadump.json");
                    File.WriteAllText(fullPath, SerializeDictionary(result, false));
                }
            }
            else if (Widgets.ButtonText(new Rect(165, 45, 50, 30), "check"))
            {
                foreach (var t in DefDatabase<ThingDef>.AllDefsListForReading)
                {
                    if ((t.IsApparel || t.IsWeapon))
                    {
                        try
                        {
                            var ds = GenStuff.DefaultStuffFor(t);
                            Log.Message(ds.defName);
                        }
                        catch (Exception e)
                        {
                            Log.Message("Checking " + t.defName);
                            Log.Error(e.Message);
                        }
                    }
                }
            }

            GUI.EndGroup();
        }

        public override void DoWindowContents(Rect rect)
        {
            this.windowRect.width = 1200;
            rect.yMin += 35;

            if (this.listUpdateNext < Find.TickManager.TicksGame)
            {
                this.isDirty = true;
            }

            if (this.isDirty)
            {
                this.UpdateList();
            }

            Text.Font = GameFont.Small;
            Text.Anchor = TextAnchor.UpperLeft;
            GUI.color = Color.white;

            List<TabRecord> list = new List<TabRecord>();
            list.Add(new TabRecord("Tab.Materials".Translate(),
                delegate { this.curTab = RimepediaTab.Materials; }, this.curTab == RimepediaTab.Materials));
            list.Add(new TabRecord("Tab.Plants".Translate(),
                delegate { this.curTab = RimepediaTab.Plants; }, this.curTab == RimepediaTab.Plants));
            list.Add(new TabRecord("Tab.Animals".Translate(),
                delegate { this.curTab = RimepediaTab.Animals; }, this.curTab == RimepediaTab.Animals));
            list.Add(new TabRecord("Tab.Info".Translate(),
                delegate { this.curTab = RimepediaTab.Dump; }, this.curTab == RimepediaTab.Dump));

            TabDrawer.DrawTabs(rect, list);
            RimepediaTab tab = this.curTab;

            switch (tab)
            {
                case RimepediaTab.Materials:
                    DoMaterialsPage(rect);
                    break;
                case RimepediaTab.Plants:
                    DoPlantsPage(rect);
                    break;
                case RimepediaTab.Animals:
                    DoAnimalsPage(rect);
                    break;
                case RimepediaTab.Dump:
                    DoDumpPage(rect);
                    break;
                default:
                    DoMaterialsPage(rect);
                    break;
            }
        }
    }
}