using System.Collections.Generic;
using RimWorld;
using Verse;

namespace Rimepedia
{
    public class RmpMaterial
    {
        public string label { get; set; }
        public float beautyFactor { get; set; }
        public float beautyOffset { get; set; }
        public float hpFactor { get; set; }
        public float flammability { get; set; }
        public ThingDef thing { get; set; }

        public List<StuffCategoryDef> categoryDefs = new List<StuffCategoryDef>();

        public RmpMaterial()
        {
            beautyFactor = 1;
            beautyOffset = 0;
            hpFactor = 1;
            flammability = 0;
        }
    }
}