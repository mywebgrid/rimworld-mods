using Verse;
using RimWorld;

namespace Rimepedia
{
    public class RmpApparel
    {
        public float armorBlunt { get; set; }

        public float armorSharp { get; set; }

        public float armorHeat { get; set; }

        public float insulation { get; set; }

        public float insulationh { get; set; }

        public RmpApparel()
        {
        }
    }
}