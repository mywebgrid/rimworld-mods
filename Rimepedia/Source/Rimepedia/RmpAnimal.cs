using Verse;

namespace Rimepedia
{
    public class RmpAnimal
    {
        public string label { get; set; }
        public ThingDef thing { get; set; }

        public ThingDef leather { get; set; }
        
        public ThingDef meat { get; set; }

        public int leatherAmount;
        public int meatAmount;

        public RmpAnimal()
        {
            this.leatherAmount = 0;
            this.meatAmount = 0;
        }
    }
}