﻿using System;
using HarmonyLib;
using RimWorld;
using Verse;

namespace JAQoL
{
    [HarmonyPatch(typeof (Dialog_FormCaravan), MethodType.Constructor, new System.Type[] {typeof (Map), typeof (bool), typeof (Action), typeof (bool), typeof (IntVec3?)})]
    public class Dialog_FormCaravan_Patch
    {
        [HarmonyPostfix]
        public static void Postfix(ref bool ___autoSelectTravelSupplies) => ___autoSelectTravelSupplies = false;
    }
}