using System.Reflection;
using Verse;

namespace JAQoL
{
    public static class Utility
    {
        public static void CleanRecipeCache(ThingDef workTable)
        {
            typeof(ThingDef).GetField("allRecipesCached", BindingFlags.Instance | BindingFlags.NonPublic)?.SetValue(workTable, null);
        }
    }
}