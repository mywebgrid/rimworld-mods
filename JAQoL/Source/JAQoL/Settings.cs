﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Verse;
using RimWorld;

namespace JAQoL
{
    public class Settings : ModSettings
    {
        public Vector2 scrollPosition;
        public float scrollViewHeight;

        public bool prisonerNonInteractAlert = true;
        public bool copyHomeAreaButton = true;
        public bool allOperationsButton = true;
        public bool smoothFloorPowder = true;
        public bool smoothWallPowder = true;
        public bool pauseOnBeginAssault = true;
        public bool restockIndicator = true;
        public bool disablePrisonBreaks = true;
        public bool disableSlaveRebellions = true;
        public bool autoHomeDefault = false;
        public bool plantsNeverRest = false;
        public bool noForceSlowdown = false;
        public IntRange slaveCount  = new IntRange(3, 5);
        // public bool qualityBuilderForbidden = true;

        public static Settings Get()
        {
            return LoadedModManager.GetMod<Mod>().GetSettings<Settings>();
        }
        
        public override void ExposeData()
        {
            Scribe_Values.Look(ref prisonerNonInteractAlert, "prisonerNonInteractAlert", true);
            Scribe_Values.Look(ref copyHomeAreaButton, "copyHomeAreaButton", true);
            Scribe_Values.Look(ref allOperationsButton, "allOperationsButton", true);
            Scribe_Values.Look(ref pauseOnBeginAssault, "pauseOnBeginAssault", true);
            Scribe_Values.Look(ref plantsNeverRest, "plantsNeverRest", false);
            base.ExposeData();
        }

        public void DoWindowContents(Rect wrect)
        {
            var options = new Listing_Standard();

            var viewRect = new Rect(0f, 0f, wrect.width - 16f, scrollViewHeight);
            options.Begin(wrect);

            options.CheckboxLabeled("Prisoner non interact alert", ref prisonerNonInteractAlert);
            options.CheckboxLabeled("Copy home area button", ref copyHomeAreaButton);
            options.CheckboxLabeled("All operations button", ref allOperationsButton);
            options.CheckboxLabeled("Pause on beginning of assault", ref pauseOnBeginAssault);
            options.CheckboxLabeled("Plants never rest", ref plantsNeverRest);
            
            options.End();
            scrollViewHeight = viewRect.height;
        }
        
        public IEnumerable<string> getEnabledSettings
        {
            get
            {
                return GetType().GetFields().Where(p => p.FieldType == typeof(bool) && (bool)p.GetValue(this)).Select(p => p.Name);
            }
        }
    }
}