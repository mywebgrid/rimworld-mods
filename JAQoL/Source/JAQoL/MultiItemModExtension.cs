﻿using System.Collections.Generic;
using Verse;

namespace JAQoL
{
    public class MiThing
    {
        public ThingDef thing;
        public FloatRange weightRange;
    }

    public class MultiItemModExtension : DefModExtension
    {
        public FloatRange totalCount;
        public List<MiThing> things;
    }
}