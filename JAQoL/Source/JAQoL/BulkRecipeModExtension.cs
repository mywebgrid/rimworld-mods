using Verse;

namespace JAQoL
{
    public class BulkRecipeModExtension : DefModExtension
    {
        public string variants = "5,10";
        public float workAmountDiscount = 0.1f;
        public float ingredientsDiscount = 0.0f;
        public float skillReqIncrease = 0.05f;
    }
    // <modExtensions>
    //     <li Class="JAQoL.BulkRecipeModExtension">
    //         <variants>5,10,20,100</variants>
    //         <workAmountDiscount>0.3</workAmountDiscount>
    //     </li>
    // </modExtensions>
    
    // <Operation Class="PatchOperationAddModExtension">
    //     <xpath>Defs/RecipeDef[defName="Example"]</xpath>
    //     <value>
    //         <li Class="JAQoL.BulkRecipeModExtension">
    //             <variants>5,10</variants>
    //         </li>
    //     </value>
    // </Operation>   
}