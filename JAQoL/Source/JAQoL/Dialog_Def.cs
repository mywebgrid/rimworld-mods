using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using Verse;
using RimWorld;

namespace JAQoL
{
    public class Dialog_Def : Window
    {
        private Def def;
        public Dialog_Def(Def def)
        {
            this.doCloseX = true;
            this.doCloseButton = true;
            this.closeOnClickedOutside = true;
            this.def = def;
        }

        public override void DoWindowContents(Rect inRect)
        {
            Text.Font = GameFont.Small;
            Text.Anchor = TextAnchor.UpperLeft;
            GUI.color = Color.white;
            StringBuilder sb = new StringBuilder ();
            
            sb.AppendLine("defName: " + def.defName);
            sb.AppendLine("mod: " + def.modContentPack.Name);
            
            GUI.BeginGroup (inRect);
            Widgets.Label (inRect, sb.ToString ());
            GUI.EndGroup ();
        }
    }
}