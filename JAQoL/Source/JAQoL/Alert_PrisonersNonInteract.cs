﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using RimWorld;
using UnityEngine;
using Verse;

namespace JAQoL
{
    public class AlertPrisonersNonInteract : Alert_Critical
    {
        private List<Pawn> UnassignedPrisoners => PawnsFinder.AllMaps_PrisonersOfColonySpawned
            .Where(prisoner => prisoner.guest.ExclusiveInteractionMode == PrisonerInteractionModeDefOf.MaintainOnly
                               || (prisoner.guest.ExclusiveInteractionMode == PrisonerInteractionModeDefOf.ReduceResistance && prisoner.guest.resistance < 0.001f)
                               || (prisoner.guest.ExclusiveInteractionMode == PrisonerInteractionModeDefOf.ReduceWill && prisoner.guest.will < 0.001f)
                               || (prisoner.guest.ExclusiveInteractionMode == PrisonerInteractionModeDefOf.Convert && prisoner.guest.ideoForConversion == prisoner.Ideo)
            )
            .ToList();

        private const int RIGHT_CLICK = 1;
        private List<Pawn> _prevUnassignedPrisoners = new List<Pawn>();

        private bool _dismissed = false;

        public override string GetLabel()
        {
            if (UnassignedPrisoners.Count == 1)
            {
                return "Unassigned Prisoner";
            }

            return "Unassigned Prisoners";
        }

        public override TaggedString GetExplanation()
        {
            var stringBuilder = new StringBuilder();
            foreach (var item in UnassignedPrisoners)
            {
                stringBuilder.AppendLine("  - " + item.NameShortColored.Resolve());
            }

            return $"These prisoners are set to non-interact:\n\n{stringBuilder}";
        }

        public override AlertReport GetReport()
        {
            if (!JaQoLMod.settings.prisonerNonInteractAlert)
            {
                return AlertReport.CulpritsAre(new List<Pawn>());
            }

            if (IsDirty())
            {
                _prevUnassignedPrisoners = UnassignedPrisoners;
                _dismissed = false;
            }

            return AlertReport.CulpritsAre(_dismissed ? new List<Pawn>() : UnassignedPrisoners);
        }

        private bool IsDirty()
        {
            return _prevUnassignedPrisoners.Count != UnassignedPrisoners.Count || UnassignedPrisoners.Any(p => !_prevUnassignedPrisoners.Contains(p));
        }

        protected override void OnClick()
        {
            if (Event.current.button != RIGHT_CLICK)
            {
                base.OnClick();
                return;
            }

            _dismissed = true;
        }
    }
}