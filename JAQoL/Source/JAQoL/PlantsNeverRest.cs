﻿using HarmonyLib;
using RimWorld;

namespace JAQoL
{
    [HarmonyPatch(typeof(Plant), "Resting", MethodType.Getter)]
    public static class PlantsNeverRest
    {
        public static bool Prefix(ref bool __result)
        {
            if (!JaQoLMod.settings.plantsNeverRest) return true;
            __result = false;
            return false;
        }
    }
}