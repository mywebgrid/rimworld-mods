﻿using UnityEngine;
using Verse;

namespace JAQoL
{
    public class JaQoLMod : Verse.Mod
    {
        public static Settings settings;
        public JaQoLMod(ModContentPack content) : base(content)
        {
            settings = GetSettings<Settings>();
        }
        
        public override void DoSettingsWindowContents(Rect inRect)
        {
            base.DoSettingsWindowContents(inRect);
            GetSettings<Settings>().DoWindowContents(inRect);
        }

        public override string SettingsCategory()
        {
            return "JAQoL";
        }
    }
}