﻿using HarmonyLib;
using UnityEngine;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using RimWorld.Planet;
using Verse;

namespace JAQoL
{
    [StaticConstructorOnStartup]
    static class HarmonyPatches
    {
        static HarmonyPatches()
        {
            var harmony = new Harmony("rimworld.bodlosh.jaqol");
            var targetMethod = AccessTools.Method(typeof(RimWorld.HealthCardUtility), "DrawMedOperationsTab");
            HarmonyMethod prefixMethod;
            var postfixMethod = new HarmonyMethod(typeof(JAQoL.HarmonyPatches).GetMethod("DrawOperations_Postfix"));
            // harmony.Patch(targetMethod, null, postfixMethod);
            //
            targetMethod = AccessTools.Method(typeof(RimWorld.Dialog_ManageAreas), "DoWindowContents");
            postfixMethod = new HarmonyMethod(typeof(JAQoL.HarmonyPatches).GetMethod("ManageAreas_Postfix"));
            // harmony.Patch(targetMethod, null, postfixMethod);
            //
            // targetMethod = AccessTools.Method(typeof(RimWorld.JobDriver_SmoothFloor), "DoEffect");
            // postfixMethod = new HarmonyMethod(typeof(JAQoL.HarmonyPatches).GetMethod("SmoothFloor_Postfix"));
            // harmony.Patch(targetMethod, null, postfixMethod);
            //
            // targetMethod = AccessTools.Method(typeof(RimWorld.JobDriver_SmoothWall), "DoEffect");
            // postfixMethod = new HarmonyMethod(typeof(JAQoL.HarmonyPatches).GetMethod("SmoothWall_Postfix"));
            // harmony.Patch(targetMethod, null, postfixMethod);
            //
            targetMethod = AccessTools.Method(typeof(CaravanFormingUtility), "AllReachableColonyItems");
            prefixMethod = new HarmonyMethod(typeof(JAQoL.HarmonyPatches).GetMethod("AllReachableColonyItems_Prefix"));
            // harmony.Patch(targetMethod, prefixMethod, null);
            //
            targetMethod = AccessTools.Method(typeof(Dialog_InfoCard), "DoWindowContents");
            prefixMethod = new HarmonyMethod(typeof(JAQoL.HarmonyPatches).GetMethod("InfoCard_Postfix"));
            harmony.Patch(targetMethod, prefixMethod, null);
            //
            targetMethod = AccessTools.Method(typeof(Verse.GenText), "ToCommaList");
            prefixMethod = new HarmonyMethod(typeof(JAQoL.HarmonyPatches), "ToCommaList_Debug");
            // harmony.Patch(targetMethod, prefixMethod, null);
            targetMethod = AccessTools.Method(typeof(Verse.PollutionUtility), "CanPlantAt");
            prefixMethod = new HarmonyMethod(typeof(JAQoL.HarmonyPatches), "CanPlantAt_Prefix");
            // harmony.Patch(targetMethod, prefixMethod, null);
            //
            // targetMethod = AccessTools.Method(typeof(Verse.ResearchProjectDef), "CanBeResearchedAt");
            // prefixMethod = new HarmonyMethod(typeof(JAQoL.HarmonyPatches).GetMethod("CanBeResearchedAt_Prefix"));
            // harmony.Patch(targetMethod, prefixMethod, null);
            //
            // targetMethod = AccessTools.Method(typeof(RimWorld.StockGenerator_Armor), "HandlesThingDef");
            // prefixMethod = new HarmonyMethod(typeof(JAQoL.HarmonyPatches), "HandlesThingDef_Debug");
            // harmony.Patch(targetMethod, prefixMethod, null);
            //
            targetMethod = AccessTools.Method(typeof(Verse.CompEquippable), "get_Holder");
            prefixMethod = new HarmonyMethod(typeof(JAQoL.HarmonyPatches).GetMethod("CompEquippableGetHolder_Prefix"));
            // harmony.Patch(targetMethod, prefixMethod, null);
            
            // targetMethod = AccessTools.Method(typeof(RimWorld.StatWorker_MarketValue), "CalculatedBaseMarketValue");
            // prefixMethod = new HarmonyMethod(typeof(JAQoL.HarmonyPatches).GetMethod("CalculatedBaseMarketValue_Prefix"));
            // harmony.Patch(targetMethod, prefixMethod, null);
        }

        public static bool CompEquippableGetHolder_Prefix(CompEquippable __instance, ref Pawn __result)
        {
            if (__instance.PrimaryVerb?.CasterPawn == null)
            {
                __result = null;
                return false;
            }

            return true;
        }

        public static bool CalculatedBaseMarketValue_Prefix(BuildableDef def, ThingDef stuffDef, ref float __result)
        {
            Log.Message(def.defName + '|' + stuffDef.defName);
            foreach (var cl in def.costList)
            {
                Log.Message(cl.thingDef.ToString() + '#' + cl.count.ToString());
                if (cl.thingDef == null || cl.count == null || cl.count == 0)
                {
                    __result = 0;
                    return false;
                }
            }
            if (def.defName == "")
            {
                __result = 0;
                return false;
            }
            return true;
        }


        public static void ManageAreas_Postfix(Dialog_ManageAreas __instance, Rect inRect)
        {
            if (!JaQoLMod.settings.copyHomeAreaButton)
            {
                return;
            }

            var listing_Standard = new Listing_Standard {ColumnWidth = inRect.width / 3};
            listing_Standard.Begin(inRect);

            var map = Find.CurrentMap;
            // int areaCount = map.areaManager.AllAreas.Count;
            listing_Standard.Gap(30 * 9 + 75);
            if (map.areaManager.CanMakeNewAllowed() &&
                listing_Standard.ButtonText("New area from home area"))
            {
                Area_Allowed area;
                map.areaManager.TryMakeNewAllowed(out area);
                var length = area.Map.Size.x * area.Map.Size.z;
                for (var i = 0; i < length; i++)
                {
                    area[i] = map.areaManager.Home[i];
                }
            }

            listing_Standard.End();
        }

        public static void DrawOperations_Postfix(Rect leftRect, Pawn pawn, Thing thingForMedBills, float curY)
        {
            if (!JaQoLMod.settings.allOperationsButton)
            {
                return;
            }

            if (Widgets.ButtonText(new Rect(165, 0, 50, 30), "ALL"))
            {
                var lungRemoved = false;
                var kidneyRemoved = false;
                Bill_Medical bill;
                BodyPartRecord heartPart = null;
                Bill_Medical heartBill = null;
                // List<Hediff> hdf = pawn.health.hediffSet.hediffs;
                foreach (var recipe in thingForMedBills.def.AllRecipes)
                {
                    if (Prefs.DevMode)
                    {
                        Log.Message("Recipe: " + recipe.label);
                    }

                    if (recipe.label == "remove part")
                    {
                        foreach (var part in recipe.Worker.GetPartsToApplyOn(pawn, recipe))
                        {
                            var n = part.def.defName;
                            bill = new Bill_Medical(recipe, null);
                            if (Prefs.DevMode)
                            {
                                Log.Message("Part: " + part.def.defName + " alive: " + part.def.alive.ToString() + " suggest: " +
                                            part.def.canSuggestAmputation);
                            }

                            if (part.def.canSuggestAmputation)
                            {
                                if (n != "Kidney" && n != "Lung")
                                {
                                    pawn.BillStack.AddBill(bill);
                                    bill.Part = part;
                                }
                                else
                                {
                                    if (n == "Kidney" && !kidneyRemoved)
                                    {
                                        pawn.BillStack.AddBill(bill);
                                        bill.Part = part;
                                        kidneyRemoved = true;
                                    }
                                    else if (n == "Lung" && !lungRemoved)
                                    {
                                        pawn.BillStack.AddBill(bill);
                                        bill.Part = part;
                                        lungRemoved = true;
                                    }
                                }
                            }
                            else if (part.def == BodyPartDefOf.Heart)
                            {
                                heartBill = bill;
                                heartPart = part;
                            }
                        }
                    }
                }

                if (heartPart != null)
                {
                    pawn.BillStack.AddBill(heartBill);
                    heartBill.Part = heartPart;
                }
            }
        }

        public static void SmoothFloor_Postfix(JobDriver_SmoothFloor __instance, IntVec3 c)
        {
            if (!JaQoLMod.settings.smoothFloorPowder)
            {
                return;
            }

            ThingDef thingDef = DefDatabase<ThingDef>.GetNamed("RockPowder", false);
            if (thingDef != null)
            {
                var thing = (Thing) ThingMaker.MakeThing(thingDef, null);
                GenSpawn.Spawn(thing, c, __instance.pawn.Map);
            }
        }

        public static void SmoothWall_Postfix(JobDriver_SmoothWall __instance)
        {
            if (!JaQoLMod.settings.smoothWallPowder)
            {
                return;
            }

            var thingDef = DefDatabase<ThingDef>.GetNamed("RockPowder", false);
            if (thingDef != null)
            {
                var thing = (Thing) ThingMaker.MakeThing(thingDef, null);
                GenSpawn.Spawn(thing, __instance.pawn.Position, __instance.pawn.Map);
            }
        }

        public static void AllReachableColonyItems_Prefix(Map map, ref bool allowEvenIfOutsideHomeArea, ref bool allowEvenIfReserved,
            ref bool canMinify)
        {
            allowEvenIfOutsideHomeArea = true;
            allowEvenIfReserved = true;
            canMinify = true;
        }

        public static void InfoCard_Postfix(Dialog_InfoCard __instance, Thing ___thing, WorldObject ___worldObject, Def ___def, Rect inRect)
        {
            if (Widgets.ButtonText(new Rect(500, 15, 50, 30), "Def"))
            {
                Def useDef = null;
                if (___thing != null)
                {
                    useDef = ___thing.def;
                }
                else if (___worldObject != null)
                {
                    useDef = ___worldObject.def;
                }
                else
                {
                    useDef = ___def;
                }

                if (useDef != null)
                {
                    Dialog_Def dlg = new Dialog_Def(useDef);
                    Find.WindowStack.Add(dlg);
                }
            }
        }

        public static bool CanBeResearchedAt_Prefix(Verse.ResearchProjectDef __instance, Building_ResearchBench bench, bool ignoreResearchBenchPowerStatus, ref bool __result)
        {
            if (__instance.techLevel > Faction.OfPlayer.def.techLevel)
            {
                __result = false;
                return false;
            }

            return true;
        }

        public static bool ToCommaList_Debug(IEnumerable<string> items, bool useAnd, ref string __result)
        {
            try
            {
                Log.Message("+++++ Commalist +++++");
                if (items == null)
                {
                    __result = "";
                    return false;
                }

                foreach (var item in items)
                {
                    Log.Message(item ?? "!null");
                }
                return true;
            }
            catch (System.NullReferenceException e)
            {
                __result = "!exception";
                return false;
            }
        }

        public static void HandlesThingDef_Debug(ThingDef td)
        {
            Log.Message("HTDD: " + td.defName);
        }

        public static bool CanPlantAt_Prefix(ThingDef plantDef, IPlantToGrowSettable settable, ref bool __result)
        {
            Log.Message(settable.GetType().ToString());
            __result = true;
            return false;
            // return true;
        }
    }


    /*[HarmonyPatch (typeof(Building_Door), "GetGizmos")]
    static class Patch_Door_Gizmos
    {
        static void Postfix (Building_Door __instance, ref IEnumerable<Gizmo> __result)
        {
            Command_Action a = new Command_Action {
                action = new Action (delegate {
                    Log.Message ("Switch");
                }),
                defaultLabel = "TestLabel",
                defaultDesc = "TestDesc",
                icon = ContentFinder<Texture2D>.Get ("Things/Building/Art/Sculpturesmall/SculptureSmallAbstractA", true),
                activateSound = SoundDef.Named ("Click")
            };
            __result = __result.Concat (new[] { a });
        }
    }*/
}