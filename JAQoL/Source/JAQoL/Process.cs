using HarmonyLib;
using UnityEngine;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Verse;

namespace JAQoL
{
    [StaticConstructorOnStartup]
    public class Process
    {
        static Process()
        {
            try
            {
                // Type seedDefType = null;
                // foreach (var pack in LoadedModManager.RunningMods)
                // {
                //     foreach (var assembly in pack.assemblies.loadedAssemblies)
                //     {
                //         if (assembly.GetName().Name.Equals("Seeds") || assembly.GetName().Name.Equals("SeedsPlease"))
                //         {
                //             seedDefType = assembly.GetType("SeedsPlease.SeedDef");
                //         }
                //     }
                // }

                var defsToClean = new List<ThingDef>();
                // Recipes defined via RecipeDef
                var recipes = DefDatabase<RecipeDef>.AllDefsListForReading.ToList();
                foreach (var recipe in recipes)
                {
                    if (!recipe.HasModExtension<BulkRecipeModExtension>()) continue;
                    var bulks = recipe.GetModExtension<BulkRecipeModExtension>();
                    var skill = recipe.workSkill;
                    var bulkVariants = bulks.variants.Split(',');
                    foreach (var variant in bulkVariants)
                    {
                        try
                        {
                            var num = int.Parse(variant);
                            var r = new RecipeDef
                            {
                                defName = recipe.defName + "_genX" + variant,
                                label = recipe.label + " (" + variant + "x)",
                                description = recipe.description + " (" + variant + "x)",
                                jobString = recipe.jobString,
                                modContentPack = recipe.modContentPack,
                                workAmount = recipe.workAmount * GetWorkAmountFactor(num, bulks),
                                workSpeedStat = recipe.workSpeedStat,
                                efficiencyStat = recipe.efficiencyStat,
                                defaultIngredientFilter = recipe.defaultIngredientFilter,
                                targetCountAdjustment = recipe.targetCountAdjustment * num,
                                workSkill = recipe.workSkill,
                                workSkillLearnFactor = recipe.workSkillLearnFactor,
                                requiredGiverWorkType = recipe.requiredGiverWorkType,
                                unfinishedThingDef = recipe.unfinishedThingDef,
                                recipeUsers = recipe.AllRecipeUsers.ToList(),
                                effectWorking = recipe.effectWorking,
                                soundWorking = recipe.soundWorking,
                                researchPrerequisite = recipe.researchPrerequisite,
                                researchPrerequisites = recipe.researchPrerequisites.ListFullCopyOrNull<ResearchProjectDef>(),
                                factionPrerequisiteTags = recipe.factionPrerequisiteTags,
                                productHasIngredientStuff = recipe.productHasIngredientStuff,
                                fixedIngredientFilter = recipe.fixedIngredientFilter,
                                allowMixingIngredients = recipe.allowMixingIngredients,
                            };

                            foreach (var wt in recipe.AllRecipeUsers.ToList())
                            {
                                if (!defsToClean.Contains(wt))
                                {
                                    defsToClean.Add(wt);
                                }
                            }

                            var skillRequirements = recipe.skillRequirements.ListFullCopyOrNull<SkillRequirement>() ?? new List<SkillRequirement>();
                            if (r.skillRequirements == null)
                            {
                                r.skillRequirements = new List<SkillRequirement>();
                            }

                            if (skillRequirements.Count == 0)
                            {
                                if (r.workSkill != null)
                                {
                                    var alteredRequirement = new SkillRequirement { skill = r.workSkill, minLevel = GetLevelRequirement(1, num, bulks) };
                                    r.skillRequirements.Add(alteredRequirement);
                                }
                                else if (r.requiredGiverWorkType?.relevantSkills != null)
                                {
                                    foreach (var relevantSkill in r.requiredGiverWorkType.relevantSkills)
                                    {
                                        var alteredRequirement = new SkillRequirement { skill = relevantSkill, minLevel = GetLevelRequirement(1, num, bulks) };
                                        r.skillRequirements.Add(alteredRequirement);
                                    }
                                }
                            }
                            else
                            {
                                foreach (var requirement in skillRequirements)
                                {
                                    var alteredRequirement = new SkillRequirement { skill = requirement.skill, minLevel = GetLevelRequirement(requirement.minLevel, num, bulks) };
                                    r.skillRequirements.Add(alteredRequirement);
                                }
                            }

                            foreach (var ingredient in recipe.ingredients)
                            {
                                var alteredIngredient = new IngredientCount();
                                alteredIngredient.SetBaseCount(ingredient.GetBaseCount() * GetIngredientsFactor(num, bulks));
                                alteredIngredient.filter.CopyAllowancesFrom(ingredient.filter);
                                r.ingredients.Add(alteredIngredient);
                            }

                            foreach (var product in recipe.products)
                            {
                                var alteredProduct = new ThingDefCountClass(product.thingDef, product.count * num);
                                r.products.Add(alteredProduct);
                            }

                            if (recipe.IngredientValueGetter.GetType() != typeof(IngredientValueGetter_Volume))
                            {
                                var prop = r.GetType().GetField("ingredientValueGetterClass", BindingFlags.NonPublic | BindingFlags.Instance);
                                prop.SetValue(r, recipe.IngredientValueGetter.GetType());
                            }

                            DefDatabase<RecipeDef>.Add(r);
                        }
                        catch (System.NullReferenceException e)
                        {
                            Log.Error("JAQoL Process recipeDef: " + recipe.defName + ", variant: " + variant + ", msg: " + e.Message);
                        }
                    }
                }

                // Recipes defined via RecipeMaker
                foreach (var def in from d in DefDatabase<ThingDef>.AllDefs where d.recipeMaker != null && d.HasModExtension<BulkRecipeModExtension>() select d)
                {
                    var rm = def.recipeMaker;
                    var bulks = def.GetModExtension<BulkRecipeModExtension>();
                    var skill = rm.workSkill;
                    var bulkVariants = bulks.variants.Split(',');
                    foreach (var variant in bulkVariants)
                    {
                        try
                        {
                            var num = int.Parse(variant);
                            var r = new RecipeDef
                            {
                                defName = "Make_" + def.defName + "_genX" + variant,
                                label = "RecipeMake".Translate(def.label) + " (" + variant + "x)",
                                description = "make " + def.label + " (" + variant + "x)",
                                descriptionHyperlinks = new List<DefHyperlink>(),
                                jobString = "RecipeMakeJobString".Translate(def.label),
                                modContentPack = def.modContentPack,
                                workAmount = (float)rm.workAmount * GetWorkAmountFactor(num, bulks),
                                workSpeedStat = rm.workSpeedStat,
                                efficiencyStat = rm.efficiencyStat,
                                defaultIngredientFilter = rm.defaultIngredientFilter,
                                targetCountAdjustment = rm.targetCountAdjustment * num,
                                workSkill = rm.workSkill,
                                workSkillLearnFactor = rm.workSkillLearnPerTick,
                                requiredGiverWorkType = rm.requiredGiverWorkType,
                                unfinishedThingDef = rm.unfinishedThingDef,
                                recipeUsers = rm.recipeUsers.ListFullCopyOrNull<ThingDef>(),
                                effectWorking = rm.effectWorking,
                                soundWorking = rm.soundWorking,
                                researchPrerequisite = rm.researchPrerequisite,
                                researchPrerequisites = rm.researchPrerequisites.ListFullCopyOrNull<ResearchProjectDef>(),
                                factionPrerequisiteTags = rm.factionPrerequisiteTags,
                            };
                            if (rm.recipeUsers != null)
                            {
                                foreach (var wt in rm.recipeUsers)
                                {
                                    if (!defsToClean.Contains(wt))
                                    {
                                        defsToClean.Add(wt);
                                    }
                                }
                            }

                            var skillRequirements = rm.skillRequirements.ListFullCopyOrNull<SkillRequirement>() ?? new List<SkillRequirement>();
                            if (r.skillRequirements == null)
                            {
                                r.skillRequirements = new List<SkillRequirement>();
                            }

                            if (skillRequirements.Count == 0)
                            {
                                if (r.workSkill != null)
                                {
                                    var alteredRequirement = new SkillRequirement { skill = r.workSkill, minLevel = GetLevelRequirement(1, num, bulks) };
                                    r.skillRequirements.Add(alteredRequirement);
                                }
                                else if (r.requiredGiverWorkType?.relevantSkills != null)
                                {
                                    foreach (var relevantSkill in r.requiredGiverWorkType.relevantSkills)
                                    {
                                        var alteredRequirement = new SkillRequirement { skill = relevantSkill, minLevel = GetLevelRequirement(1, num, bulks) };
                                        r.skillRequirements.Add(alteredRequirement);
                                    }
                                }
                            }
                            else
                            {
                                foreach (var requirement in skillRequirements)
                                {
                                    var alteredRequirement = new SkillRequirement { skill = requirement.skill, minLevel = GetLevelRequirement(requirement.minLevel, num, bulks) };
                                    r.skillRequirements.Add(alteredRequirement);
                                }
                            }

                            if (def.MadeFromStuff)
                            {
                                var ingredientCount = new IngredientCount();
                                ingredientCount.SetBaseCount((float)def.costStuffCount * GetIngredientsFactor(num, bulks));
                                ingredientCount.filter.SetAllowAllWhoCanMake(def);
                                r.ingredients.Add(ingredientCount);
                                r.fixedIngredientFilter.SetAllowAllWhoCanMake(def);
                                r.productHasIngredientStuff = true;
                            }

                            if (def.costList != null)
                            {
                                foreach (var current in def.costList)
                                {
                                    var ingredientCount2 = new IngredientCount();
                                    ingredientCount2.SetBaseCount((float)current.count * GetIngredientsFactor(num, bulks));
                                    ingredientCount2.filter.SetAllow(current.thingDef, true);
                                    r.ingredients.Add(ingredientCount2);
                                }
                            }

                            r.products.Add(new ThingDefCountClass(def, rm.productCount * num));

                            r.generated = true;
                            r.modContentPack.AddDef((Def)r, "ImpliedDefs");
                            r.PostLoad();
                            r.ResolveReferences();

                            DefDatabase<RecipeDef>.Add(r);
                        }
                        catch (System.NullReferenceException e)
                        {
                            Log.Error("JAQoL Process recipeMaker: " + def.defName + ", variant: " + variant + ", msg: " + e.Message);
                        }
                    }
                }

                // if (seedDefType != null)
                // {
                //     // Recipes for seeds
                //     foreach (var def in from d in DefDatabase<ThingDef>.AllDefs where d.thingClass.Name == "Seed" && d.HasModExtension<SeedRecipeExtension>() select d)
                //     {
                //     }
                // }

                // clean recipes cache
                try
                {
                    foreach (var wt in defsToClean)
                    {
                        Utility.CleanRecipeCache(wt);
                    }
                }
                catch (System.NullReferenceException e)
                {
                    Log.Error("JAQoL Process clean recipe cache: msg: " + e.Message);
                }
            }
            catch (System.NullReferenceException e)
            {
                Log.Error("JAQoL Process exception: " + e.Message);
            }
        }

        private static int GetWorkAmountFactor(int num, BulkRecipeModExtension bulks)
        {
            return (int)Math.Floor(num - bulks.workAmountDiscount * num);
        }

        private static int GetIngredientsFactor(int num, BulkRecipeModExtension bulks)
        {
            return (int)Math.Floor(num - bulks.ingredientsDiscount * num);
        }

        private static int GetLevelRequirement(int defaultLevel, int num, BulkRecipeModExtension bulks)
        {
            var level = defaultLevel > 0 ? defaultLevel : 1;
            level += (int)Math.Floor(level * num * bulks.skillReqIncrease);
            return level > 20 ? 20 : level;
        }
    }
}