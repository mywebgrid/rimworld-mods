﻿using System;
using Verse;

namespace JAQoL
{
    public class MultiItem : Thing
    {
        public override void SpawnSetup(Map map, bool respawningAfterLoad)
        {
            base.SpawnSetup(map, respawningAfterLoad);
            if (this.def.HasModExtension<MultiItemModExtension>())
            {
                var ext = this.def.GetModExtension<MultiItemModExtension>();
                var totalWeight = 0f;
                var totalCount = (int) Math.Round(ext.totalCount.RandomInRange);
                foreach (var thing in ext.things)
                {
                    totalWeight += thing.weightRange.Average;
                }

                foreach (var thing in ext.things)
                {
                    var relativeCount = (int) Math.Ceiling((thing.weightRange.RandomInRange / totalWeight) * totalCount);
                    if (relativeCount > 0)
                    {
                        var newThing = ThingMaker.MakeThing(thing.thing);
                        newThing.stackCount = relativeCount;
                        GenPlace.TryPlaceThing(newThing, this.Position, map, ThingPlaceMode.Near);
                    }
                }

                this.Destroy();
            }
        }
    }
}