﻿using Harmony;
using UnityEngine;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Verse;

namespace CloudCore
{
	public class gasTestBuilding : Building
	{
		public gasTestBuilding ()
		{
		}

		//private int spawnGasTick = 0;

		private void Spawn_Gas () {
			ThingDef thingDef = DefDatabase<ThingDef>.GetNamed ("Helium_Gas", true);
			Thing thing = (Thing)ThingMaker.MakeThing (thingDef, null);
			IntVec3 loc = this.Position;
			Map map = this.Map;

			GenSpawn.Spawn (thing, loc, map);
		}

		public override string GetInspectString()
		{
			StringBuilder stringBuilder = new StringBuilder();
			MapComponent_WindSpeed cws = (MapComponent_WindSpeed)Find.CurrentMap.GetComponent (typeof(MapComponent_WindSpeed));
			if (cws != null) {
				stringBuilder.AppendLine("Speed: " + cws.windSpeed);
				stringBuilder.AppendLine("Direction: " + cws.windDirection);
				stringBuilder.AppendLine("Direction rad: " + cws.windDirectionRad);
			}
			stringBuilder.AppendLine(base.GetInspectString());
			return stringBuilder.ToString().TrimEndNewlines();
		}

		public override IEnumerable<Gizmo> GetGizmos ()
		{
			yield return new Command_Action {
				action = new Action (this.Spawn_Gas),
				defaultLabel = "Spawn Gas",
				defaultDesc = "",
				icon = ContentFinder<Texture2D>.Get ("Things/Gas/Puff", true)
			};
		}

		public override void Tick ()
		{
			/*if (this.spawnGasTick > 720) {				
				this.spawnGasTick = 0;
				this.Spawn_Gas ();
			}
			this.spawnGasTick++;*/
			base.Tick ();
		}
	}
}

