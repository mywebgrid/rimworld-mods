﻿using UnityEngine;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Verse;
using RimWorld.BaseGen;

namespace CloudCore
{
	public class Core_Gas : Thing
	{
		public int destroyTick;

		public float graphicRotation;

		public float graphicRotationSpeed;

		private const int MOVE_TICKS = 1;
		private const int CHECK_TICKS = 10;
		private const int WIND_SPEED_REDUCTION = 20;
		private const int HIT_TICKS = 120;

		private int moveGasTick = 0;
		private int checkGasTick = 0;
		private int hitTick = 0;
		private bool isStopped = false;

		private MapComponent_WindSpeed wsUtil;
		private System.Random rndUtil;

		private Vector3 drawPosition;
		public int intensity = 1;
		private List<Pawn> touchingPawns = new List<Pawn> ();

		public Core_Gas () : base ()
		{
			wsUtil = null;
			rndUtil = null;
		}

		private void Move_Gas (bool check)
		{
			if (wsUtil != null && this.Map != null && this.rndUtil != null) {
				if (!this.isStopped || check) {
					bool isOutdoors = this.Position.UsesOutdoorTemperature (this.Map);
					this.isStopped = false;
					Vector3 newPos = this.drawPosition;
					float rndWindSpeed = (float)this.rndUtil.NextDouble () / 5;
					float windDirection;
					float windSpeed;
					if (isOutdoors) {
						// direction
						int modifier = this.rndUtil.Next (19) - 10;
						windDirection = this.wsUtil.windDirectionRad + ((float)modifier * (float)Math.PI) / 180f;
						// speed
						int modifierSpeed = this.rndUtil.Next (19) - 10;
						windSpeed = this.wsUtil.windSpeed + rndWindSpeed;
						windSpeed /= (WIND_SPEED_REDUCTION + modifierSpeed);
					} else {						
						windDirection = (float)this.rndUtil.Next (628) / 100;
						windSpeed = rndWindSpeed / 2;
					}

					newPos.x += (float)Math.Cos (windDirection) * windSpeed;
					newPos.z += (float)Math.Sin (windDirection) * windSpeed;
					IntVec3 newPosInt = new IntVec3 ((int)Math.Round (newPos.x), (int)Math.Round (newPos.y), (int)Math.Round (newPos.z));				

					if (newPosInt != this.Position) {
						if (!newPosInt.InBounds (this.Map)) {
							this.isStopped = true;
							this.Destroy (DestroyMode.Vanish);
						} else {
							Thing th = newPosInt.GetRoofHolderOrImpassable (this.Map);
							if (th != null && !(th is Building_Vent)) {
								this.isStopped = true;
							}
						}
					}
					if (!this.isStopped) {
						this.drawPosition = newPos;
						this.Position = newPosInt;
					}
					if (!this.DestroyedOrNull() && this.Position.InBounds (this.Map)) {
						List<Thing> thingList = this.Position.GetThingList (this.Map);
						if (thingList != null) {
							int otherGases = 0;
							for (int i = 0; i < thingList.Count; i++) {
								Core_Gas otherGas = thingList [i] as Core_Gas;						
								if (otherGas != null && otherGas.intensity > 0) {
									otherGases++;
								}
							}
							if (otherGases > 1) {								
								windDirection = (float)this.rndUtil.Next (628) / 100;
								windSpeed = rndWindSpeed / 2;

								newPos.x += (float)Math.Cos (windDirection) * windSpeed;
								newPos.z += (float)Math.Sin (windDirection) * windSpeed;
								newPosInt = new IntVec3 ((int)Math.Round (newPos.x), (int)Math.Round (newPos.y), (int)Math.Round (newPos.z));

								if (newPosInt.InBounds (this.Map)) {
									Thing th = newPosInt.GetRoofHolderOrImpassable (this.Map);
									if (th == null || (th is Building_Vent)) {
										this.drawPosition = newPos;
										this.Position = newPosInt;
									}
								}
							}
						}
					}
				}
			}
		}

		public override void Draw ()
		{
			Mesh mesh = MeshPool.GridPlane (this.def.graphicData.drawSize);
			Graphics.DrawMesh (mesh, this.drawPosition, Quaternion.AngleAxis (this.graphicRotation, Vector3.up), this.def.DrawMatSingle, 0);
		}

		public override void SpawnSetup (Map map, bool respawningAfterLoad)
		{
			/*Gas gasOnPos = this.Position.GetGas (map);
			var chemistry_Gas = gasOnPos as Core_Gas;
			if (chemistry_Gas != null) {
				this.intensity += chemistry_Gas.intensity;
			}*/
			base.SpawnSetup (map, respawningAfterLoad);
			this.rndUtil = new System.Random (this.thingIDNumber);
			this.destroyTick = Find.TickManager.TicksGame + this.def.gas.expireSeconds.RandomInRange.SecondsToTicks ();
			this.graphicRotationSpeed = Rand.Range (-this.def.gas.rotationSpeed, this.def.gas.rotationSpeed) / 60;
			Vector3 exactPos = this.TrueCenter ();
			exactPos.x += (float)this.rndUtil.NextDouble () - 0.5f;
			exactPos.z += (float)this.rndUtil.NextDouble () - 0.5f;
			this.drawPosition = exactPos;
			this.wsUtil = (MapComponent_WindSpeed)this.Map.GetComponent (typeof(MapComponent_WindSpeed));
		}

		public override void ExposeData ()
		{
			base.ExposeData ();
			Scribe_Values.Look<int> (ref this.destroyTick, "destroyTick", 0, false);
			Scribe_Collections.Look<Pawn> (ref this.touchingPawns, "testees", LookMode.Reference, new object[0]);
		}

		private void applyGas (Pawn p)
		{
			// get damage def
			DamageDef dDef = DefDatabase<DamageDef>.GetNamed (this.def.projectile.damageDef.ToString (), true);

			Boolean preventGas = false;

			if (p.apparel != null) {
				List<Apparel> wornApparel = p.apparel.WornApparel;
				foreach (Apparel item in wornApparel) {
					GasProtectionExtension ext = item.def.GetModExtension<GasProtectionExtension>() ??
						GasProtectionExtension.defaultValues;

					if (ext.efficiency > 0) {					
						preventGas = true;
					}
				}
			}

			if (!preventGas) {
				Log.Message ("gas not prevented");
				if (this.def.projectile.GetDamageAmount (1f) > 0) {
					// gas does physical damage
					// generate injury and add it to pawn
					int num = this.def.projectile.GetDamageAmount (1f);
					BodyPartHeight height = (Rand.Value >= 0.666) ? BodyPartHeight.Middle : BodyPartHeight.Top;
					float armorPenetration = 0;

					DamageInfo dinfo = new DamageInfo (dDef, num, armorPenetration, -1, this, null, null, DamageInfo.SourceCategory.ThingOrUnknown, null);

					dinfo.SetBodyRegion (height, BodyPartDepth.Outside);

					p.TakeDamage (dinfo);
				} else {
					// gas does no physical damage
					// get hediff def
					HediffDef hDef = DefDatabase<HediffDef>.GetNamed (dDef.hediff.ToString (), true);

					// add hediff to pawn
					p.health.AddHediff (HediffMaker.MakeHediff (hDef, p, null), null, null, null);
				}
			} else {
				Log.Message ("gas prevented");
			}
		}

		public override void Tick ()
		{
			if (this.Spawned) {
				if (this.destroyTick <= Find.TickManager.TicksGame) {
					this.Destroy (DestroyMode.Vanish);
				} else {
					this.graphicRotation += this.graphicRotationSpeed + this.rndUtil.Next (5) - 3;

					if (this.hitTick > HIT_TICKS) {
						List<Thing> thingList = this.Position.GetThingList (this.Map);
						if (thingList != null) {
							for (int i = 0; i < thingList.Count; i++) {
								Pawn pawn = thingList [i] as Pawn;
								if (pawn != null && !this.touchingPawns.Contains (pawn) && !pawn.Dead) {
									this.touchingPawns.Add (pawn);
									this.applyGas (pawn);
									this.hitTick = 0;
								}
							}
						}
						for (int j = 0; j < this.touchingPawns.Count; j++) {
							Pawn pawn2 = this.touchingPawns [j];
							if (!pawn2.Spawned || pawn2.Position != this.Position) {
								this.touchingPawns.Remove (pawn2);
							}
						}
					} else {
						this.hitTick++;
					}
				
					if (this.moveGasTick > MOVE_TICKS) {				
						this.moveGasTick = 0;
						bool check = false;
						if (this.checkGasTick > CHECK_TICKS) {
							this.checkGasTick = 0;
							check = true;
						}
						this.Move_Gas (check);
					}
					this.moveGasTick++;
					this.checkGasTick++;
				}
			}
		}
	}
}

