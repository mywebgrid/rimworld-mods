﻿using Harmony;
using UnityEngine;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Verse;

namespace CloudCore
{
	public class MapComponent_WindSpeed : MapComponent
	{
		public float windSpeed;

		public float windDirection = 0f;

		public float windDirectionRad = 0f;

		private int tickCounterSpd = 0, tickCounterDir = 0;

		private const int WIND_SPEED_TICKS = 60;

		private const int WIND_DIRECTION_TICKS = 300;

		public MapComponent_WindSpeed (Map map) : base (map)
		{

		}

		private float getNewWindDirection ()
		{
			System.Random rnd = new System.Random ();
			float newDir = this.windDirection + (rnd.Next (2) * 2 - 1) * rnd.Next (5);
			if (newDir > 359) {
				newDir = 360 - newDir;
			} else if (newDir < 0) {
				newDir = 360 + newDir;
			}
			return newDir;
		}

		public override void MapComponentTick ()
		{			
			if (this.tickCounterSpd > WIND_SPEED_TICKS) {
				this.windSpeed = this.map.windManager.WindSpeed;
				this.tickCounterSpd = 0;
				if (this.tickCounterDir > WIND_DIRECTION_TICKS) {
					this.windDirection = this.getNewWindDirection ();
					this.windDirectionRad = (float)(Math.PI * this.windDirection / 180f);
					this.tickCounterDir = 0;
				}
			} else {
				this.tickCounterSpd++;
				this.tickCounterDir++;
			}
		}

	}
}

