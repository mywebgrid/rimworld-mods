﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection.Emit;
using RimWorld;
using Verse;
using UnityEngine;

namespace CloudCore
{
	public class GasDispenserExtension : DefModExtension
	{
		public static readonly GasDispenserExtension defaultValues = new GasDispenserExtension ();

		public string useGas = "Helium_Gas";
	}
}

