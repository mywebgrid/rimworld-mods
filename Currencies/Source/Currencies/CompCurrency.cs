using Verse;
using System;
using RimWorld;
using UnityEngine;

namespace Currencies
{
    public class CompProperties_Currency : CompProperties
    {
        public float value = 10f;

        public CompProperties_Currency()
        {
            this.compClass = typeof(CurrencyComp);
        }
    }

    public class CurrencyComp : ThingComp
    {
        public CompProperties_Currency Props => (CompProperties_Currency) this.props;
    }
}