﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using Verse;

namespace Currencies
{
    [StaticConstructorOnStartup]
    public class CurrenciesPatches
    {
        static CurrenciesPatches()
        {
            var harmony = new Harmony("rimworld.bodlosh.currencies");
            harmony.PatchAll();
        }
    }

    [HarmonyPatch(typeof(TradeUtility))]
    [HarmonyPatch(nameof(TradeUtility.GetPricePlayerSell))]
    [HarmonyPriority(Priority.Last)]
    public static class GetPricePlayerSell_Patch
    {
        [HarmonyPrefix]
        static bool Prefix(Thing thing,
            float priceFactorSell_TraderPriceType, float priceFactorSell_HumanPawn, float priceGain_PlayerNegotiator, float priceGain_FactionBase,
            float priceGain_DrugBonus, float priceGain_AnimalProduceBonus, TradeCurrency currency, ref float __result)
        {
            var comp = thing.TryGetComp<CurrencyComp>();
            if (comp == null) return true;
            if (currency == TradeCurrency.Favor)
            {
                var rfv = ThingDefOf.Silver.GetStatValueAbstract(StatDefOf.RoyalFavorValue);
                __result = rfv * comp.Props.value;
                return true;
            }

            __result = comp.Props.value;
            return false;
        }
    }

    [HarmonyPatch(typeof(TradeUtility))]
    [HarmonyPatch(nameof(TradeUtility.GetPricePlayerBuy))]
    [HarmonyPriority(Priority.Last)]
    public static class GetPricePlayerBuy_Patch
    {
        [HarmonyPrefix]
        static bool Prefix(Thing thing, float priceFactorBuy_TraderPriceType, float priceFactorBuy_JoinAs, float priceGain_PlayerNegotiator, float priceGain_FactionBase, ref float __result)
        {
            Log.Message("Prefix");
            var comp = thing.TryGetComp<CurrencyComp>();
            if (comp == null) return true;
            Log.Message("Currency");
            __result = comp.Props.value;
            return false;

        }
    }

    [HarmonyPatch(typeof(TraderKindDef))]
    [HarmonyPatch(nameof(TraderKindDef.WillTrade))]
    public static class WillTrade_Patch
    {
        [HarmonyPrefix]
        static bool Prefix(ThingDef td, ref bool __result)
        {
            if (!td.HasComp(typeof(Currencies.CurrencyComp))) return true;
            __result = true;
            return false;

        }
    }

    [HarmonyPatch(typeof(ThingSetMaker_TraderStock))]
    [HarmonyPatch(nameof(ThingSetMaker_TraderStock.Generate))]
    [HarmonyPatch(new Type[] { typeof(ThingSetMakerParams), typeof(List<Thing>) })]
    public static class TstockGenerate_Patch
    {
        [HarmonyPostfix]
        static void Postfix(ThingSetMakerParams parms, List<Thing> outThings)
        {
            var silver = outThings.Find(t => t.def == ThingDefOf.Silver);
            if (silver != null)
            {
                var cnt = GenMath.RoundRandom(silver.stackCount / 1000) + 1;
                Thing currency = ThingMaker.MakeThing(DefDatabase<ThingDef>.GetNamed("Currency_Credit"));
                currency.stackCount = cnt;
                outThings.Add(currency);
            }
        }
    }
}