using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using Verse;
using RimWorld;
using RimWorld.Planet;

namespace Chronology
{
    public class EpochWorldComponent : WorldComponent
    {
        public Chronology.Epoch startEpoch = Chronology.Epoch.StoneAge;
        public Chronology.Epoch currentEpoch = Chronology.Epoch.StoneAge;
        public Chronology.Epoch endEpoch = Chronology.Epoch.GreatWar;

        public EpochWorldComponent(World world) : base(world)
        {
        }

        private void ChangePawnKindDefs()
        {
            var ei = (int) currentEpoch;
            var meleeTag = ChronologyUtils.epochNames[ei] + "_Melee";
            var rangedTag = ChronologyUtils.epochNames[ei] + "_Ranged";
            var commonTag = ChronologyUtils.epochNames[ei] + "_Common";
            var soldierTag = ChronologyUtils.epochNames[ei] + "_Soldier";

            foreach (var pk in DefDatabase<PawnKindDef>.AllDefs)
            {
                if (pk.race.defName == "Human")
                {
                    if (pk.weaponTags == null)
                    {
                        pk.weaponTags = new List<string>();
                    }
                    else
                    {
                        pk.weaponTags.Clear();
                    }

                    pk.weaponTags.Add(meleeTag);
                    pk.weaponTags.Add(rangedTag);

                    if (pk.apparelTags == null)
                    {
                        pk.apparelTags = new List<string>();
                    }
                    else
                    {
                        pk.apparelTags.Clear();
                    }

                    pk.apparelTags.Add(commonTag);
                    pk.apparelTags.Add(soldierTag);
                }
            }
        }

        public void SetEpoch(Chronology.Epoch epoch)
        {
            currentEpoch = epoch;
            foreach (var faction in DefDatabase<FactionDef>.AllDefs)
            {
                faction.techLevel = (TechLevel) epoch;
            }

            var ep = DefDatabase<EpochDef>.GetNamed(currentEpoch.ToString());
//            Build weapons materials
            ChronologyUtils.weaponMaterials.Clear();
            ChronologyUtils.weaponMaterials = ep.weaponMaterials;
            foreach (var c in ep.weaponMaterialsCategory)
            {
                var tmp = DefDatabase<ThingDef>.AllDefsListForReading.FindAll(t => t.stuffProps != null && t.stuffProps.categories.Contains(c));
                ChronologyUtils.weaponMaterials = ChronologyUtils.weaponMaterials.Union(tmp).ToList();
            }

//            Build apparel materials            
            ChronologyUtils.apparelMaterials.Clear();
            ChronologyUtils.apparelMaterials = ep.apparelMaterials;
            foreach (var c in ep.apparelMaterialsCategory)
            {
                var tmp = DefDatabase<ThingDef>.AllDefsListForReading.FindAll(t => t.stuffProps != null && t.stuffProps.categories.Contains(c));
                ChronologyUtils.apparelMaterials = ChronologyUtils.apparelMaterials.Union(tmp).ToList();
            }

            ChangePawnKindDefs();

            Find.LetterStack.ReceiveLetter("Advanced to " + ("Epoch." + epoch).Translate(),
                "World has advanced to " + ("Epoch." + epoch).Translate(),
                LetterDefOf.PositiveEvent);
        }

        public void AdvanceEpochDebug()
        {
            AdvanceEpoch();
        }

        private void AdvanceEpoch()
        {
            if (currentEpoch < endEpoch)
            {
                currentEpoch++;
                SetEpoch(currentEpoch);
            }
        }

        public override void FinalizeInit()
        {
            ChronologyUtils.component = this;
            ChronologyUtils.worldIsGenerated = true;
            SetEpoch(currentEpoch);
            ChangePawnKindDefs();
        }

        public override void WorldComponentTick()
        {
        }

        public override void ExposeData()
        {
            Scribe_Values.Look(ref currentEpoch, "Chronology_CurrentEpoch");
        }
    }
}