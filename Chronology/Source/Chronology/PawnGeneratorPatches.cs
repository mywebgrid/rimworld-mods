using HarmonyLib;
using UnityEngine;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Verse;

namespace Chronology
{
    [StaticConstructorOnStartup]
    public class PawnGeneratorPatches
    {
        static PawnGeneratorPatches()
        {
            var harmony = new Harmony("rimworld.bodlosh.chronology");
            MethodInfo targetMethod;
            HarmonyMethod prefixMethod;
            HarmonyMethod postfixMethod;
//            targetMethod = AccessTools.Method(typeof(RimWorld.PawnWeaponGenerator), "TryGenerateWeaponFor");
//            postfixMethod= new HarmonyMethod(typeof(PawnGeneratorPatches).GetMethod("TryGenerateWeaponFor_Postfix"));
//            harmony.Patch(targetMethod, null, postfixMethod);
            //
//            targetMethod = AccessTools.Method(typeof(Verse.StuffProperties), "CanMake");
//            postfixMethod = new HarmonyMethod(typeof(PawnGeneratorPatches).GetMethod("CanMake_Postfix"));
//            harmony.Patch(targetMethod, null, postfixMethod);
            //
            targetMethod = AccessTools.Method(typeof(Verse.ThingMaker), nameof(ThingMaker.MakeThing));
            prefixMethod = new HarmonyMethod(typeof(PawnGeneratorPatches).GetMethod("MakeThing_Prefix"));
//            postfixMethod = new HarmonyMethod(typeof(PawnGeneratorPatches).GetMethod("MakeThing_Postfix"));
            harmony.Patch(targetMethod, prefixMethod, null);
        }

        public static void TryGenerateWeaponFor_Postfix(Pawn pawn)
        {
            if (pawn.equipment != null && pawn.equipment.Primary != null)
            {
                Log.Message(pawn.ToString() + ": ");
                Log.Message(pawn.equipment.Primary.ToString());
                Log.Message(pawn.equipment.Primary.Stuff.ToString());
                if (pawn.equipment.Primary.Stuff.defName == "Steel")
                {
                }

                Log.Message("------------------------");
            }
        }

        public static void CanMake_Postfix(StuffProperties __instance, BuildableDef t, bool __result)
        {
            Log.Message(__instance.stuffAdjective + " + " + t.ToString());
        }

        public static void MakeThing_Prefix(ref ThingDef def, ref ThingDef stuff, ref Thing __result)
        {
            if (ChronologyUtils.worldIsGenerated)
            {
                if (def != null)
                {
                    def = ChronologyUtils.GetAllowedThing(def);
                }

                if (stuff != null && def != null)
                {
                    stuff = ChronologyUtils.GetAllowedStuff(def, stuff);
                }
            }
        }

        public static void MakeThing_Postfix(ThingDef def, ThingDef stuff, ref Thing __result)
        {
            if (def != null && stuff != null)
            {
                Log.Message(def.ToString() + " + " + stuff.ToString() + " = " + __result.ToString());
            }
        }
    }
}