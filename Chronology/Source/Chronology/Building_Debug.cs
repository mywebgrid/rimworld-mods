using HarmonyLib;
using UnityEngine;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Verse;

namespace Chronology
{
    public class Building_Debug : Building
    {
        public override IEnumerable<Gizmo> GetGizmos()
        {
            EpochWorldComponent component = Find.World.GetComponent<EpochWorldComponent>();
            
            yield return new Command_Action
            {
                action = new Action(component.AdvanceEpochDebug),
                defaultLabel = "Advance epoch",
                defaultDesc = "",
                icon = ContentFinder<Texture2D>.Get("Things/Gas/Puff", true)
            };
            
            foreach (var epoch in Enum.GetValues(typeof(Chronology.Epoch)).Cast<Chronology.Epoch>())
            {
                yield return new Command_Action
                {
                    action = new Action(delegate { component.SetEpoch(epoch); }),
                    defaultLabel = "To " + epoch,
                    defaultDesc = "",
                    icon = ContentFinder<Texture2D>.Get("Things/Gas/Puff", true)
                };
            }
        }
    }
}