using HarmonyLib;
using UnityEngine;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
// using ResearchPal;
using Verse;
using Log = Verse.Log;

namespace Chronology
{
//    [HarmonyPatch(typeof(ResearchPal.Tree))]
//    [HarmonyPatch("RelevantTechLevels", MethodType.Getter), StaticConstructorOnStartup]
//    public class ResearchPalPatch
//    {
//        static bool Prefix(ref List<TechLevel> ____relevantTechLevels, ref List<TechLevel> __result)
//        {
//            Verse.Log.Message("Relevant tech levels");
//            if (____relevantTechLevels == null)
//            {
//                int[] techLevels = Enumerable.Range(2, ChronologyUtils.GetEpochCount()).ToArray();
//                ____relevantTechLevels = techLevels
//                    .Cast<TechLevel>()
//                    // filter down to relevant tech levels only.
//                    .Where(tl => DefDatabase<ResearchProjectDef>.AllDefsListForReading.Any(rp => rp.techLevel == tl))
//                    .ToList();
//            }
//
//            __result = ____relevantTechLevels;
//            return false;
//        }
//    }

    [StaticConstructorOnStartup]
    public static class ChronologyTechLevelPatches
    {
        static ChronologyTechLevelPatches()
        {
            //
            // HarmonyInstance.Create("ChronologyTechLevelPatches").PatchAll();
//            MethodInfo targetMethod;
//            HarmonyMethod prefixMethod;
//            HarmonyMethod postfixMethod;
//            //
//            targetMethod = AccessTools.Method(typeof(TechLevelUtility), nameof(TechLevelUtility.ToStringHuman));
//            prefixMethod = new HarmonyMethod(typeof(TechLevelPatches).GetMethod("TLU_ToStringHuman_Prefix"));
//            harmony.Patch(targetMethod, prefixMethod, null);
//            //
//            targetMethod = AccessTools.Method(typeof(ResearchPal.Tree), "get_RelevantTechLevels");
//            prefixMethod = new HarmonyMethod(typeof(TechLevelPatches).GetMethod("TLU_GetRelevantTechLevels_Prefix"));
//            harmony.Patch(targetMethod, prefixMethod, null);
//            //
//            targetMethod = AccessTools.Method(typeof(Verse.ResearchProjectDef), "get_PrerequisitesCompleted");
//            prefixMethod = new HarmonyMethod(typeof(TechLevelPatches).GetMethod("PrerequisitesCompleted_Prefix"));
//            harmony.Patch(targetMethod, prefixMethod, null);
        }
    }
    
//    [HarmonyPatch(typeof(Enum))]
//    [HarmonyPatch(nameof(Enum.Parse))]
//    [HarmonyPatch(new Type[] { typeof(Type), typeof(string), typeof(bool) })]
//    public static class BackCompatibleEnum_Patch
//    {
//        [HarmonyPrefix]
//        static bool Prefix(Type enumType, string value, bool ignoreCase, ref object __result)
//        {
//            Log.Message(enumType.ToString() + ": " + value);
//            return true;
//        }
//    }
    
    [HarmonyPatch(typeof(TechLevelUtility))]
    [HarmonyPatch(nameof(TechLevelUtility.ToStringHuman))]
    public static class ToStringHuman_Patch
    {
        [HarmonyPrefix]
        static bool Prefix(TechLevel tl, ref string __result)
        {
            __result = ("Epoch." + ChronologyUtils.GetEpochName(tl)).Translate();
            return false;
        }
    }

    // [HarmonyPatch(typeof(ResearchPal.Tree))]
    // [HarmonyPatch("RelevantTechLevels", MethodType.Getter)]
    // public static class RelevantTechLevels_Patch
    // {
    //     [HarmonyPrefix]
    //     static bool Prefix(ref List<TechLevel> __result, ref List<TechLevel> ____relevantTechLevels)
    //     {
    //         if (____relevantTechLevels == null)
    //         {
    //             int[] techLevels = Enumerable.Range(2, ChronologyUtils.GetEpochCount()).ToArray();
    //             ____relevantTechLevels = new List<TechLevel>();
    //             foreach (var tl in techLevels)
    //             {
    //                 ____relevantTechLevels.Add((TechLevel) tl);
    //             }
    //         }
    //
    //         __result = ____relevantTechLevels;
    //         return false;
    //     }
    // }
    
    [HarmonyPatch(typeof(ResearchProjectDef))]
    [HarmonyPatch("PrerequisitesCompleted", MethodType.Getter)]
    public static class PrerequisitesCompleted_Patch
    {
        [HarmonyPrefix]
        static bool Prefix(ref bool __result, ResearchProjectDef __instance)
        {
            if ((int) ChronologyUtils.component.currentEpoch < (int) __instance.techLevel)
            {
//                __result = false;
//                return false;
            }

            return true;
        }
    }
    
//     [HarmonyPatch(typeof(ResearchPal.ResearchNode))]
//     [HarmonyPatch(nameof(ResearchNode.Available), MethodType.Getter)]
//     public static class RNAvailable_Patch
//     {
//         [HarmonyPrefix]
//         static bool Prefix(ref bool __result, ResearchNode __instance)
//         {
//             if ((int) ChronologyUtils.component.currentEpoch < (int) __instance.Research.techLevel)
//             {
// //                __result = false;
// //                return false;
//             }
//
//             return true;
//         }
//     }
}