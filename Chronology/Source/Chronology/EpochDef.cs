using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using Verse;
using RimWorld;
using RimWorld.Planet;

namespace Chronology
{
    public class EpochDef : Def
    {
        public List<ThingDef> weaponMaterials;
        public List<ThingDef> apparelMaterials;
        public List<StuffCategoryDef> weaponMaterialsCategory;
        public List<StuffCategoryDef> apparelMaterialsCategory;
        
        
    }
}