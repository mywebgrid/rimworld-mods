using System;
using System.Linq;
using System.Collections.Generic;
using RimWorld;
using Verse;
using UnityEngine;

namespace Chronology
{
    public class Alert_Epoch : Alert
    {
        public Alert_Epoch()
        {
            this.defaultLabel = "";
            this.defaultPriority = AlertPriority.Medium;
        }

        public override TaggedString GetExplanation()
        {
            return ("Epoch." + Find.World.GetComponent<EpochWorldComponent>().currentEpoch).Translate();
        }

        public override string GetLabel()
        {
            return ("Epoch." + Find.World.GetComponent<EpochWorldComponent>().currentEpoch).Translate();
        }

        public override AlertReport GetReport()
        {
            return true;
        }
    }
}