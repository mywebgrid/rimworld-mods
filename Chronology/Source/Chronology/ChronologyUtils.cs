using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using Verse;
using RimWorld;
using RimWorld.Planet;
using Random = System.Random;

namespace Chronology
{
    public static class ChronologyUtils
    {
        public static EpochWorldComponent component;
        public static bool worldIsGenerated = false;

        public static List<ThingDef> weaponMaterials = new List<ThingDef>();
        public static List<ThingDef> apparelMaterials = new List<ThingDef>();

        public static string[] epochNames = new[]
        {
            "Undefined", // 0
            "Animal", // 1
            "StoneAge", // 2
            "CopperAge", // 3
            "BronzeAge", // 4
            "IronAge", // 5
            "Antiquity", // 6
            "EarlyMedieval", // 7
            "HighMedieval", // 8
            "LateMedieval", // 9
            "EarlyModern", // 10
            "Revolutions", // 11
            "Late19th", // 12
            "GreatWar", // 13
            "Ww2", // 14
            "LateModern1", // 15
            "LateModern2", // 16
            "Current", // 17
            "NearFuture", // 18
            "Spacer", // 19
            "Ultra", // 20
            "Archotech" // 21
        };

        public static int GetEpochCount()
        {
            return epochNames.Length - 2;
        }

        public static string GetEpochName(int tl)
        {
            return epochNames[tl];
        }

        public static string GetEpochName(TechLevel tl)
        {
            return GetEpochName((int) tl);
        }

        public static ThingDef GetAllowedThing(ThingDef thing)
        {
            return thing;
        }

        public static ThingDef GetAllowedStuff(ThingDef thing, ThingDef stuff)
        {
            if (thing.IsApparel)
            {
                if (apparelMaterials.Count == 0 || apparelMaterials.Contains(stuff))
                {
                    return stuff;
                }
                else
                {
                    var random = new Random();
                    int index = random.Next(apparelMaterials.Count);
                    return apparelMaterials[index];
                }
            }
            else if (thing.IsWeapon)
            {
                if (weaponMaterials.Count == 0 || weaponMaterials.Contains(stuff))
                {
                    return stuff;
                }
                else
                {
                    var random = new Random();
                    int index = random.Next(weaponMaterials.Count);
                    return weaponMaterials[index];
                }
            }
            else
            {
                return stuff;
            }
        }
    }
}