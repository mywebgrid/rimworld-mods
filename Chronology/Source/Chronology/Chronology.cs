﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using Verse;
using RimWorld;
using RimWorld.Planet;

namespace Chronology
{
    public class Chronology
    {
        public enum Epoch : byte
        {
            Undefined,
            Animal,
            StoneAge,
            CopperAge,
            BronzeAge,
            IronAge,
            Antiquity,
            EarlyMedieval,
            HighMedieval,
            LateMedieval,
            EarlyModern,
            Revolutions,
            Late19th,
            GreatWar,
            Ww2,
            LateModern1,
            LateModern2,
            Current,
            NearFuture,
            Spacer,
            Ultra,
            Archotech
        }

        public Chronology()
        {
            // Find.FactionManager.AllFactions;
        }
    }
}