﻿import xml.etree.ElementTree as ET

def list2xpath(defnames, filter_attr='defName', filter_attr_a='@Name', filter_attr_b='@ParentName'):
    xpaths = []
    for defname in defnames:
        if len(defname):
            if defname.startswith('@'):
                xpaths.append(f'{filter_attr_a}="{defname[1:]}"')
            elif defname.startswith('_@'):
                xpaths.append(f'{filter_attr_b}="{defname[2:]}"')
            else:
                xpaths.append(f'{filter_attr}="{defname}"')
    return ' or '.join(xpaths)


def list2li(defnames):
    lis = []
    for defname in defnames:
        if isinstance(defname, tuple):
            lis.append(f'<li MayRequire="{defname[1]}">{defname[0]}</li>')
        elif len(defname):
            lis.append(f'<li>{defname}</li>')
    return ''.join(lis)

def list2texts(defnames):
    texts = []
    for defname in defnames:
        if len(defname):
            texts.append(f'text()="{defname}"')
    return ' or '.join(texts)

def compress_xml(xml_string):
    root = ET.fromstring(xml_string)
    compressed_xml = ET.tostring(root, encoding='unicode', method='xml')
    compressed_xml = ' '.join(compressed_xml.split())
    return compressed_xml