﻿from ArchitectCategories.common import list2xpath, list2texts, compress_xml

merge_research = {
    'ComplexClothing': ['VAE_LeatherTanning', 'VAE_HazardProtection', 'VAE_MlitaryCamouflage', ]
}

xml_patches = ''

for out, ins in merge_research.items():
    xml_patches += f'''
<li Class="PatchOperationReplace">
    <xpath>//researchPrerequisite[{list2texts(ins)}]</xpath>
    <value><researchPrerequisite>{out}</researchPrerequisite></value>
</li>
<li Class="PatchOperationReplace">
    <success>Always</success>
    <xpath>//researchPrerequisites/li[{list2texts(ins)}]</xpath>
    <value><li>{out}</li></value>
</li>
<li Class="PatchOperationReplace">
    <success>Always</success>
    <xpath>//prerequisites/li[{list2texts(ins)}]</xpath>
    <value><li>{out}</li></value>
</li>
<li Class="PatchOperationRemove">
    <xpath>Defs/ResearchProjectDef[{list2xpath(ins)}]</xpath>
</li>'''


with open('D:/Games/Rimworld/rimworld-mods/AlienPatches/1.5/Patches/MergeResearch.xml', 'w+') as patches_file:
    out_xml = f'<?xml version="1.0" encoding="utf-8"?><Patch><Operation Class="PatchOperationSequence"><success>Always</success><operations>{xml_patches}</operations></Operation></Patch>'
    # patches_file.write(compress_xml(out_xml))