﻿data = {
    'Bdl_lights': [
        {'name': 'MAIN', 'things': [
            'TorchLamp', 'TorchWallLamp', 'Darktorch', 'DarktorchFungus',
            'MAG_ArchotechTorchLamp', '@LampBase', '@CCP_BaseLED', '@CCP_LEDRopeLightBase',
            'FloodLight', 'Brazier', 'Chemlamp', 'ChemlampPost',
            '@GlCeilingLampBase', '@GLWallLightBase', '@CeilingLampBase', '@GlassLightBase',
            '@MURWallLightBase', 'RP_CeilingLight', 'StandingLamp_Magenta', 'StandingLamp_Yellow',
            'StandingLamp_Orange', 'StandingLamp_Aqua', 'StandingLamp_Lime', 'StandingLamp_Purple',
            '@D9AJO_ChandelierBase', 'Light_SpacerLamp', 'Spacer_OutdoorLamp', 'VFEM_WallTorchLamp',
            'MorrowRim_KwamaEggStandingTorch', 'MorrowRim_KwamaEggStandingLamp', 'MorrowRim_KwamaEggStandingDarkTorch', 'MorrowRim_KwamaEggStandingDarkLamp',
            'MAG_ArchotechTorchLamp', 'LotRD_Chandelier', 'LotRD_DwarvenCandelabra', 'LotRE_ElvenCandelabra',
            'LotRD_Brazier', 'LotRD_DwarvenWallLight', 'LotRD_DwarvenStreetLamp', 'MedTimes_StreetLamp',
            'MedTimes_Brazier', 'MedTimes_Reflector', 'Owl_Fencepost_TorchLamp',
            'Toxilamp', 'VFEE_Candelabra', 'pphhyy_OsseousLamp', 'Light_Streetlamp',
            '@BotchJob_ProfanedLantern', '@BotchJob_ProfanedBrazier', 'OutdoorGroundLamp', 'OuterRim_FloorLamp',
            'OuterRim_WallLamp', 'Outland_StoneOilLantern', 'Outland_OilLantern', 'Outland_StoneOilStreetLamp',
            'Outland_OilStreetLamp', 'Outland_WallSconce', 'Outland_WallSconce_Darklight', 'Outland_StoneWallSconce',
            'Outland_StoneWallSconce_Darklight', 'pphhyy_OsseousBrazier', 'GLO_GlowstonePylon', 'GLO_GlowstoneLamp',
            '@GlowstoneChandelierBase', '@OutlandWallBoxLanternBase', 'BMT_DisplayLight', '@OutlandOilWallLanternBase',
            'VG_TorchLamp', '@DV_PyrinthLamp', '@DV_PyrinthBrazier', '',
            'DevDesigner_RusticBloodTorchLamp', 'DevDesigner_WallBloodTorch', 'DevDesigner_BloodBrazier', '',
            '', '', '', '',
        ], 'remove_dcat': [
            'TorchLamp', 'CCP_LEDWallLight', 'CCP_WRLEDWallLight', 'CCP_LEDLight',
            'CCP_WRLEDLight', 'CCP_HIDLEDLight', 'CCP_WRHIDLEDLight', 'CCP_LEDMedLight',
        ]},

    ]
}
