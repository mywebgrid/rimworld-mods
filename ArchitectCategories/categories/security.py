﻿data = {
    'Security': [
        {'name': 'machines', 'things': [
            'Outland_Ballista', 'Outland_BoltThrower', 'Outland_Mangonel', 'VFEP_Turret_Cannon',
            'VFEC_Turret_Scorpion', 'Outland_PactCatapult', 'VFES_Turret_Ballista', 'VFES_Turret_Catapult',
            '', '', '', '',
            '', '', '', '',
            '', '', '', '',
            '', '', '', '',
        ]},
        {'name': 'turrets', 'things': [
            '@AutocannonTurret', 'Turret_MiniTurret', 'Turret_RocketswarmLauncher', '',
            'MilitaryTurretGun', 'ShredderTurretGun', 'PrecisionTurretGun', 'BlastTurretGun',
            'VulcanCannon', 'ChargeCannon', 'PulsefireTurretGun', 'PulsefireTwinCannon',
            'VFES_Turret_AutocannonDouble', 'VFES_Turret_MilitaryTurret', 'VFES_Turret_Flame', 'MilitaryTurretGun',
            'VFES_Turret_Searchlight', 'VFES_Turret_ChargeTurret', '@LVE_TurretBase', 'BotchJob_BloodStoneTurret',
            'VFES_Turret_SentryGun', 'VFES_Turret_ChargeRailgunTurret', 'VFES_Turret_EMPTurret', '@GTBaseTurret',
            'DV_Turret_PulseAutogun', 'VFE_Turret_AutoChargeBlaster', 'VFE_Turret_AutoChargeLance', 'VFE_Turret_AutoInfernoCannon',
            'VFE_Turret_AutoMortar', '', '', '',
            '', '', '', '',
            '', '', '', '',
            # manned
            'GunComplex', 'RocketComplex', 'VFES_Turret_HMGComplex', 'MilitaryTurretGunManned',
            'ShredderTurretGunManned', 'PrecisionTurretGunManned', 'BlastTurretGunManned', 'VulcanCannonManned',
            'Turret_GatlingGun', 'VFES_Turret_ChargeComplex', '', '',
            '', '', '', '',
        ]},
        {'name': 'artillery', 'things': [
            '@BaseArtilleryBuilding', '', '', '',
            'Turret_DevastatorMortarBomb', '@GTBaseArtillery', '', '',
            '', '', '', '',
            '', '', '', '',
            '', '', '', '',
            '', '', '', '',
        ]},
        {'name': 'traps', 'things': [
            '@TrapIEDBase', '@VFED_RemoteChargeBase', 'AB_TrapIED_Tar', '',
            'TrapSpike', 'VFET_AnimalTrap', 'VFES_BearTrap', '@OutlandTrapMedievalBase',
            'RBSFE_LeafTrap', 'ImpactionCharge',
        ]},
        {'name': 'firefighting', 'things': [
            'Turret_FoamTurret', 'FirefoamPopper', 'FExt_CeilingFoamPopper', 'Turret_MiniTurret_FE ',
            '', '', '', '',
            '', '', '', '',
            '', '', '', '',
        ]},
        {'name': 'obstacles', 'things': [
            'Sandbags', 'HeavySandbags', 'Barricade', 'VFES_CavalrySpikes',
            'VVE_TankTrap', 'VFES_BarbedWire', 'VFES_Trench', 'VFES_Platform',
            'Watchtower_Watchtower', 'VFES_Turret_Decoy', 'VFES_DeployableBarrier', '',
            '', '', '', '',
            '', '', '', '',
            '', '', '', '',
        ]},
        {'name': 'biological', 'things': [
            '@USH_BiologicalBarrelBase', '@USH_BiologicalVentBase', '@USH_BiologicalMissileLauncherBase', '',
            '', '', '', '',
            '', '', '', '',
            '', '', '', '',
        ]},
    ]
}
