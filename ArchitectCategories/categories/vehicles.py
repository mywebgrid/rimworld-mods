﻿data = {
    'VF_Vehicles': [
        {'name': 'garage', 'things': [
            'VVE_GarageBench', 'VVE_RefuelingPump', '@VVE_GarageDoorAbstract', 'VVE_GarageCabinet',
            '', '', '', '',
            '', '', '', '',
            '', '', '', '',
            '', '', '', '',
            '', '', '', '',
        ], 'remove_dcat': []},
        {'name': 'SRTS', 'things': [
            '@NECShipBase',
        ], 'remove_dcat': []},
        {'name': 'ship', 'things': [
            'Ship_Beam', 'Ship_CryptosleepCasket', 'Ship_ComputerCore', 'Ship_Reactor',
            'Ship_Engine', 'Ship_SensorCluster', '', '',
            '', '', '', '',
            '', '', '', '',
            '', '', '', '',
        ], 'remove_dcat': []},
        # {'name': 'boats', 'things': [
        #     'VFEPD_Dinghy', '', '', '',
        #     '', '', '', '',
        #     '', '', '', '',
        #     '', '', '', '',
        #     '', '', '', '',
        # ], 'remove_dcat': []},
    ]
}
