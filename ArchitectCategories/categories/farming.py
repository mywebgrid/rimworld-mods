﻿data = {
    'RFF_Farm': [
        {'name': 'MAIN', 'things': [
            'PlantPot', 'PlantPot_Bonsai',
            'ZARS_Bonfire', 'Cworld_fungibasin', 'ZARS_HarvestPot',
            'ZARS_SlagHarvestBox', 'HydroponicsBasin', 'VFE_PlanterBox', 'VFE_PlanterBox_Tilable',
            'VFE_Ecosystem', 'VFE_Ecosystem_Tilable', 'VFE_Hydroponics_Tilable', 'SNS_Hydroponics_I',
            'SNS_Hydroponics_II', 'SNS_Hydroponics_III', '@CCP_LEDGrowLightBase', 'Anon2SquarePlantPot',
            'Anon2PlantSpot', 'Spacer_SunLamp', 'VG_SaltMine', 'VGb_DiggingSpot',
            'MedTimes_IceGatheringStation', 'BMT_FungiponicsBasin', 'Bdl_UndergroundHydroponics', 'MedTimes_GardeningBox',
            'ToxifuelSunLamp', 'GrowLamp', 'MSI_TubeGrowLights', 'MSI_StonePlanter',
            'MSI_WoodenPlanter', 'SpaceBucket', 'BMT_AdvancedFungiponicsBasin', 'VFE_Scarecrow',
            'PlantPot_BambooBonsai', '@OEArtEcosys', '', '',
            '', '', '', '',
            '', '', '', '',
        ],
         'terrains': [
             '@SS_TerrainBase'
         ],
         'remove_dcat': []
         },

    ]
}
