﻿from ArchitectCategories.common import list2li
from common import list2xpath

worktables = {
    'tailoring': {
        'workbenches': [
            'HandTailoringBench', 'ElectricTailoringBench',
        ],
        'things': [
            'Apparel_TribalA', 'Apparel_WarMask', 'Apparel_WarVeil', 'Apparel_TribalHeaddress',
            'Apparel_TwilekA', 'ChJAndroidThermalBandages', 'VAE_Apparel_TribalPoncho', 'VAE_Apparel_TribalKilt',
            'VAEA_Apparel_BattleBanner', 'VAEA_Apparel_Backpack', '@ApparelMakeableBaseZhthyhl', 'ZhthyhlTribal',
            'VBY_Woven_BasketWaste',
        ]
    },

    'loom': {
        'workbenches': [
            'TableLoom', 'TableElecLoom', ('VBY_PrimitiveLoom', 'PrimitiveProduction.velcroboy333'),
        ],
        'recipes': [
            'Make_Patchleather', 'MorrowRim_MakeClothFromKresh', 'MorrowRim_MakeClothFromKresh_Bulk', 'Make_Fungusleather',
            'BulkCraftHempFibers', 'BulkCraftDevilsleafFibers', 'BMT_Make_Mushroomleather', 'MakeHempClothHand',
            'VPE_WeaveClothFromMycothread', 'MakeXenweave', 'MakeXenweaveBulk', 'MakeClothHand', 'MakeLinenHand',
            'VBY_HayclothRecipe', 'VBY_HayclothRecipeBulk', 'Recycle_INSSlab_Fibre', 'DF_WeavePigTailCloth',
            'DF_WeavePigTailClothBulk', 'Reinforce_Toxicloth', 'VBY_BambooFiberRecipe', 'VBY_BambooFiberRecipeBulk',
            'Make_Patchleather', '', '', '',
            '', '', '', '',
            '', '', '', '',
            '', '', '', '',
        ]
    },

    'prosthetics': {
        'workbenches': [
            'ProstheticsWorkbench',
        ],
        'recipes': [

        ],
        'things': [
            '@AnimaImplant', '@GS_NeolithicBionicMakeableBase', '@GS_MedievalBionicMakeableBase', '@AnimaPart',
            '@BodyPartBasicBase',
        ]
    },

    'prosthetics2': {
        'workbenches': [
            'TableSyrProsthetics',
        ],
        'recipes': [

        ],
        'things': [
            '@GS_IndustrialBionicMakeableBase', '@GS_SpacerBionicMakeableBase', '@GS_HyperBionicMakeableBase', '@GS_UltraBionicMakeableBase',
            '_@LTS_BaseModuleItem', '@BodyPartProstheticMakeableBase', '@BodyPartBionicBase'
        ]
    },

    'churn': {
        'workbenches': [
            'FRMT_Churn'
        ],
        'recipes': [
            'MakeSmokeleafButter', 'MakeSmokeleafButterBulk', 'VCE_SmokeleafButter', 'DF_PressRockNutPasteBulk',
            'DF_PressRockNutPaste',
        ]
    },

    'oilpress': {
        'workbenches': [
            'FRMT_OilPress'
        ],
        'recipes': [
            'CraftSmokeleafSeedOil', 'BulkCraftSmokeleafSeedOil', '',
        ]
    },

    'rolling': {
        'workbenches': [
            'ZARS_RollingSpot', ('PoweredCultivationBench', 'Ogliss.bishop.SmokeleafIndustryReborn'),
        ],
        'recipes': [
            'VBE_MakeCigarettes', 'VBE_MakeCigars', '', '',
            '', '', '', '',
        ],
        'things': [
            'SmokeleafJoint', '', '', '',
            'AnimaJoint', 'Cigarillos', 'CrudeSmokeleafJoint', 'SD_FeywingJoint',
            'XenLeafJoint', 'SD_BladerootCigar', 'CatnipJoint', 'GR_FeatherdustJoint',
        ]
    }
}

xml_patches = ''

for wt in worktables.values():
    things = list2xpath(wt.get('things', []))
    recipes = list2xpath(wt.get('recipes', []))
    recipe_users = list2li(wt['workbenches'])

    if len(things):
        xml_patches += f'<li Class="XmlExtensions.PatchOperationAddOrReplace"><xpath>Defs/ThingDef[{things}]/recipeMaker</xpath><value><recipeUsers>{recipe_users}</recipeUsers></value></li>'

    if len(recipes):
        xml_patches += f'<li Class="XmlExtensions.PatchOperationAddOrReplace"><xpath>Defs/RecipeDef[{recipes}]</xpath><value><recipeUsers>{recipe_users}</recipeUsers></value></li>'

with open('D:/Games/Rimworld/rimworld-mods/AlienPatches/1.5/Patches/Worktables.xml', 'w') as patches_file:
    patches_file.write(f'<?xml version="1.0" encoding="utf-8"?><Patch><Operation Class="PatchOperationSequence"><success>Always</success><operations>{xml_patches}</operations></Operation></Patch>')
