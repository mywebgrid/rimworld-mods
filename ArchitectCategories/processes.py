﻿processes = [
    {
        # 'input': ['RawCompost', 50], 'output': ['Fertilizer', 50], 'days': '3',
        'input': ['Rotwort', 50], 'output': ['Rotmeth', 50], 'days': '3',
    }
]

xml_defs = ''
xml_patches = ''

for proc in processes:
    input = proc['input'][0]
    output = proc['output'][0]
    # xml_patches += f'<li Class="XmlExtensions.PatchOperationAdd"><xpath>Defs/</xpath><value><Fermenter.FermentDef><defName>Bdl_{input}_to_{output}</defName><Product>{output}</Product><Resource>{input}</Resource><SpoilProduct>SpoilMash</SpoilProduct><FermentHours>72.0</FermentHours><ResearchProject>Brewing</ResearchProject></Fermenter.FermentDef></value></li>'
    xml_defs += f'<Fermenter.FermentDef><defName>Bdl_{input}_to_{output}</defName><Product>{output}</Product><Resource>{input}</Resource><SpoilProduct>SpoilMash</SpoilProduct><FermentHours>72.0</FermentHours><ResearchProject>Brewing</ResearchProject></Fermenter.FermentDef>'


with open('D:/Games/Rimworld/rimworld-mods/AlienPatches/1.5/Defs/Processes.xml', 'w+') as defs_file:
    defs_file.write(f'<?xml version="1.0" encoding="utf-8"?><Defs>{xml_defs}</Defs>')


if xml_patches:
    with open('D:/Games/Rimworld/rimworld-mods/AlienPatches/1.5/Patches/Processes.xml', 'w+') as patches_file:
        patches_file.write(f'<?xml version="1.0" encoding="utf-8"?><Patch><Operation Class="PatchOperationSequence"><success>Always</success><operations>{xml_patches}</operations></Operation></Patch>')


processor_framework = '''
<comps>
      <li Class="ProcessorFramework.CompProperties_Processor">
        <capacity>120</capacity>
        <independentProcesses>false</independentProcesses>
        <parallelProcesses>false</parallelProcesses>
        <dropIngredients>false</dropIngredients>
        <showProductIcon>true</showProductIcon>
        <barOffset>(0.0, 0.25)</barOffset>
        <barScale>(1.0, 1.0)</barScale>
        <colorCoded>false</colorCoded>
        <processes>
			<li>Beer</li>
			<li>rum</li>
			<li>VGP_BerryWine</li>
			<li>VG_AmbrosiaWine</li>
			<li>saki</li>
			<li>CPD_Vodka</li>
			<li>tequila</li>
			<li>CPD_Whiskey</li>
			<li>Fruitshine</li>
			<li>FruitKvass</li>
			<li>VG_Kumis</li>					
		</processes>
		</li>
		<li Class="CompProperties_Forbiddable"/>
    </comps>
    <ProcessorFramework.ProcessDef>
    <defName>VGP_BerryWine</defName>
    <thingDef>VGP_BerryWine</thingDef>
    <ingredientFilter>
      <thingDefs>
        <li>CPD_WineMust</li>
      </thingDefs>
    </ingredientFilter>
    <processDays>7.5</processDays>
    <usesQuality>true</usesQuality>
    <qualityDays>(7.5, 7.5, 7.5, 9.75, 11.75, 13.75, 16.75)</qualityDays>				  
  </ProcessorFramework.ProcessDef>
'''

vef = '''
<comps>
			<li Class="ItemProcessor.CompProperties_ItemProcessor">
				<numberOfInputs>1</numberOfInputs>
				<acceptsNoneAsInput>false</acceptsNoneAsInput>
				<ignoresIngredientLists>true</ignoresIngredientLists>
				<showProgressBar>false</showProgressBar>
				<isSemiAutomaticMachine>true</isSemiAutomaticMachine>
				<isLightDependingMachine>true</isLightDependingMachine>
				<minLight>-1</minLight>
				<maxLight>0.5</maxLight>
				<isTemperatureDependingMachine>true</isTemperatureDependingMachine>
				<minTemp>7</minTemp>
				<maxTemp>25</maxTemp>
				<destroyIngredientsAtStartOfProcess>true</destroyIngredientsAtStartOfProcess>
				<noPowerDestroysProgress>true</noPowerDestroysProgress>
				<noPowerDestroysInitialWarning>Outland.NoFuelInKilnWarning</noPowerDestroysInitialWarning>
				<rareTicksToDestroy>10</rareTicksToDestroy>
			</li>
			</comps>

<ItemProcessor.ItemAcceptedDef>
		<defName>Outland_KilnInput</defName>
		<building>Outland_Kiln</building>
		<slot>1</slot>
		<items>			
			<li>Outland_Clay</li>						
		</items>							
	</ItemProcessor.ItemAcceptedDef>

	<ItemProcessor.CombinationDef>
		<defName>Outland_KilnCombination</defName>
		<building>Outland_Kiln</building>
		<items>
			<li>Outland_Clay</li>
		</items>
		<amount>
			<li>40</li>
		</amount>
		<result>Outland_ClayBrick</result>	
		<yield>40</yield>
		<singleTimeIfNotQualityIncreasing>1</singleTimeIfNotQualityIncreasing>
	</ItemProcessor.CombinationDef>
'''

fermenter = '''
<Fermenter.FermentDef>
              <defName>Ferment_VBE_AmbrandyMush</defName>
              <Product>VBE_AmbrandyMust</Product>
              <Resource>VBE_AmbrandyMush</Resource>
              <SpoilProduct>SpoilMash</SpoilProduct>
              <FermentHours>72.0</FermentHours>
              <ResearchProject>VBE_MixologyResearch</ResearchProject>
            </Fermenter.FermentDef>
'''