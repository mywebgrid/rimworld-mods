from ArchitectCategories.common import list2xpath, list2texts

remove = [
    # bulks
    'Make_PemmicanBulk', 'CookMealSimpleBulk', 'CookMealFineBulk', 'CookMealLavishBulk',
    'CookMealSurvivalBulk', 'SplitDirt', 'SplitDirtBulk', 'DirtToClay',
    'DirtToClayBulk', 'MakeSand', 'MakeSandBulk', 'MakeClay',
    'MakeClayBulk', 'MakeDirt', 'MakeDirtBulk', 'MakeCompost5',
    'BulkRollCrudeSmokeleafJointCraftingSpot', 'VG_BulkCigarillos', 'BulkRollSmokeleafJointDrugLab',
    # refineries
    '', '', '', '',
    '', '', '', '',
]

add = {
    '10': {
        'recipes': [
            'VBE_MakeTequilaMust', 'VBE_MakeWhiskeyMash', 'VBE_MakeWhiskeyMashFromFlour', 'VBE_MakeVodkaMash',
            'VBE_MakeGinMush', 'VBE_MakeCigarettes', 'MakeCompost', 'Outland_MakeClay',
            'Outland_MakeAdobeBricks', 'RB_MeadMust', 'RPP_Production_Component', 'Make_SynthamideComposite',
            'VBE_MakeCigars', 'BMT_RefineGleamcapSalve', '', '',
        ],
        'things': [
            'MedicineIndustrial', 'Penoxycyline', 'Flake', 'Yayo',
            'WakeUp', 'PsychiteTea', 'TM_AdvancedMedicine', 'VBE_AmbrosiaMush',
            'UraniumPellets', 'turbineBlade', 'CrudeSmokeleafJoint', 'Cigarillos',
            '', '', '', '',
        ]
    },
    '5,10': {
        'recipes': [
            'Make_ComponentSpacer', 'Make_ComponentSpacerSynthylene', '', '',
            '', '', '', '',
            '', '', '', '',
            '', '', '', '',
        ],
        'things': [
            '@MakeableShellBase', 'Make_ChemfuelFromWood', 'Make_ChemfuelFromOrganics', 'Make_Patchleather',
            '', '', '', '',
            '', '', '', '',
            '', '', '', '',
        ]
    },
    '10,20': {
        'recipes': [
            'Make_Kibble', 'Make_Pemmican', 'Make_Wort', 'CookMealSurvival',
            'MakeGlass', 'MakeReinforcedGlass', 'UraniumPellets', 'turbineBlade',
            '', '', '', '',
            '', '', '', '',
        ],
        'things': [
            '', '', '', '',
            '', '', '', '',
            '', '', '', '',
            '', '', '', '',
        ]
    },
    '10,20,50': {
        'recipes': [
            'Make_ComponentIndustrial', 'Make_ComponentIndustrialPlastic', '', '',
            '', '', '', '',
            '', '', '', '',
            '', '', '', '',
        ],
        'things': [
            '', '', '', '',
            '', '', '', '',
            '', '', '', '',
            '', '', '', '',
        ]
    },
    '5,10,20,50': {
        'recipes': [
            'CookMealSimple', 'CookMealFine', 'CookMealLavish', '',
            '', '', '', '',
            '', '', '', '',
            '', '', '', '',
        ],
        'things': [
            '', '', '', '',
            '', '', '', '',
            '', '', '', '',
            '', '', '', '',
        ]
    }
}

xml_patches = ''

if len(remove):
    xml_patches += f'<li Class="PatchOperationRemove"><xpath>Defs/RecipeDef[{list2xpath(remove)}]</xpath></li>'
    xml_patches += f'<li Class="PatchOperationRemove"><xpath>Defs/ThingDef/recipes/li[{list2texts(remove)}]</xpath></li>'

for variants, b in add.items():
    things = list2xpath(b.get('things', []))
    recipes = list2xpath(b.get('recipes', []))

    if len(things):
        xml_patches += f'<li Class="PatchOperationAddModExtension"><xpath>Defs/ThingDef[{things}]</xpath><value><li Class="JAQoL.BulkRecipeModExtension"><variants>{variants}</variants></li></value></li>'
        xml_patches += f'<li Class="XmlExtensions.PatchOperationSafeRemove"><xpath>Defs/ThingDef[{things}]/recipeMaker/bulkRecipeCount</xpath></li>'

    if len(recipes):
        xml_patches += f'<li Class="PatchOperationAddModExtension"><xpath>Defs/RecipeDef[{recipes}]</xpath><value><li Class="JAQoL.BulkRecipeModExtension"><variants>{variants}</variants></li></value></li>'


with open('D:/Games/Rimworld/rimworld-mods/AlienPatches/1.5/Patches/Bulks.xml', 'w') as patches_file:
    patches_file.write(f'<?xml version="1.0" encoding="utf-8"?><Patch><Operation Class="PatchOperationSequence"><success>Always</success><operations>{xml_patches}</operations></Operation></Patch>')
    