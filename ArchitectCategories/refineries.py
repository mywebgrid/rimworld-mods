﻿from ArchitectCategories.common import list2li, compress_xml

refineries = [
    {'product': 'RimPlas', 'label': 'RimPlas', 'power': 1000, 'stackSize': 50, 'bufferSize': 20, 'consumeRate': 0.2},
    {'product': 'Synthbase', 'label': 'Synthbase', 'power': 1000, 'stackSize': 50, 'bufferSize': 20, 'consumeRate': 0.2},
]

products = []
buildings_xml = '''<ThingDef ParentName="RefineryBase" Name="CustomRefineryBase" Abstract="True">
    <graphicData>
        <texPath>Rimefeller/Things/Building/SynthyleneRefinery</texPath>
        <graphicClass>Graphic_Single</graphicClass>
        <drawSize>(4,4)</drawSize>
    </graphicData>
    <costList>
        <Steel>150</Steel>
        <ComponentIndustrial>3</ComponentIndustrial>
    </costList>
    <researchPrerequisites>
        <li>SynthyleneProduction</li>
    </researchPrerequisites>
</ThingDef>'''

for refinery in refineries:
    product = refinery['product']
    products.append(product)
    buildings_xml += f'''<ThingDef ParentName="CustomRefineryBase">
    <defName>{product}_Refinery</defName>
    <label>{refinery["label"]} Refinery</label>
    <description>Processes chemfuel into {refinery["label"]}.</description>
    <comps>
        <li Class="CompProperties_Power">
            <compClass>CompPowerTrader</compClass>
            <basePowerConsumption>{str(refinery["power"])}</basePowerConsumption>
        </li>
        <li Class="Rimefeller.CompProperties_Refinery">
            <compClass>Rimefeller.CompRefineryNapalm</compClass>
            <Product>{product}</Product>
            <StackSize>{str(refinery["stackSize"])}</StackSize>
            <BufferSize>{str(refinery["bufferSize"])}</BufferSize>
            <ConsumeRate>{str(refinery["consumeRate"])}</ConsumeRate>
        </li>
    </comps>
</ThingDef>'''

xml_patches = f'<li Class="PatchOperationAdd"><xpath>Defs/ThingDef[defName="RefineryLoadingBay"]/building/fixedStorageSettings/filter/thingDefs</xpath><value>{list2li(products)}</value></li>'

with open('D:/Games/Rimworld/rimworld-mods/AlienPatches/1.5/Defs/Refineries.xml', 'w') as defs_file:
    defs_file.write(compress_xml(f'<?xml version="1.0" encoding="utf-8"?><Defs>{buildings_xml}</Defs>'))

with open('D:/Games/Rimworld/rimworld-mods/AlienPatches/1.5/Patches/Refineries.xml', 'w') as patches_file:
    patches_file.write(f'<?xml version="1.0" encoding="utf-8"?><Patch><Operation Class="PatchOperationSequence"><success>Always</success><operations>{xml_patches}</operations></Operation></Patch>')