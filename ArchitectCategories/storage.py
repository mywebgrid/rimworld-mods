﻿from ArchitectCategories.common import list2li

storage_add = {
    'ReelStorageAnimalFeeder': [
        ('INSSlab_Biomass', 'Mlie.Insulation'),
        ('BMT_MycelialBlocks', 'BiomesTeam.BiomesCaverns'),
    ],
    'ReelStorageFridge': [
        'MedicineHerbal',
    ]
}

xml_patches = ''

for storage, adds in storage_add.items():
    additions = list2li(adds)
    if len(additions):
        xml_patches += f'<li Class="PatchOperationAdd"><xpath>Defs/ThingDef[defName="{storage}"]/building/fixedStorageSettings/filter/thingDefs</xpath><value>{additions}</value></li>'
        xml_patches += f'<li Class="PatchOperationAdd"><xpath>Defs/ThingDef[defName="{storage}"]/building/defaultStorageSettings/filter/thingDefs</xpath><value>{additions}</value></li>'

with open('D:/Games/Rimworld/rimworld-mods/AlienPatches/1.5/Patches/StorageAdd.xml', 'w') as patches_file:
    patches_file.write(f'<?xml version="1.0" encoding="utf-8"?><Patch><Operation Class="PatchOperationSequence"><success>Always</success><operations>{xml_patches}</operations></Operation></Patch>')
