from ArchitectCategories.common import list2li, list2texts
from common import list2xpath
from categories.structure import data as structure
from categories.production import data as production
from categories.furniture import data as furniture
from categories.lights import data as lights
from categories.farming import data as farming
from categories.power import data as power
from categories.vehicles import data as vehicles
from categories.automation import data as automation
from categories.security import data as security
from categories.storage import data as storage
from categories.recreation import data as recreation

# Ogre_NanoRepairTech_DesignationCategory
# OuterRim_ArchitectHypertech
# MoreArchotechGarbage

merged_designators = [
    {
        'label': 'Tech', 'defName': 'Category_MiscTech', 'order': 500,
        'designators': [
            # 'Ogre_NanoRepairTech_DesignationCategory',
            'MoreArchotechGarbage',
            # 'GlitterNet',
        ],
        'specialDesignatorClasses': ['Designator_Cancel', 'Designator_Deconstruct', 'TabulaRasa.Designator_SubCategory']
    }
]
removed_designators = [
    'Insulation_Desig', 'LEDLights', 'LTSFurniture', 'LTSStructure', 'MSI', 'SRTS_Expanded', 'Ship', 'Outland_Outland',
]

xml_subcategories = '<DesignationCategoryDef><defName>Category_Others</defName><label>others</label><order>0</order></DesignationCategoryDef>'
xml_patches = ''

for category, cdata in {**structure, **production, **furniture, **lights, **farming, **power, **vehicles, **automation, **security, **storage, **recreation}.items():
    # allSubcategoryDefName = f'Subcategory_{category}_all'
    xml_patches += f'<li Class="PatchOperationAdd"><xpath>Defs/DesignationCategoryDef[defName="{category}"]/specialDesignatorClasses</xpath><value><li>TabulaRasa.Designator_SubCategory</li></value></li>'
    # xml_subcategories += f'<TabulaRasa.DesignatorSubCategoryDef><defName>{allSubcategoryDefName}</defName><label>All</label><designationCategory>{category}</designationCategory></TabulaRasa.DesignatorSubCategoryDef>'
    for subcategory in cdata:
        if len(subcategory["name"]) == 0:
            continue
        # <!-- <iconPath>Toolbox/UI/CategoryStructural</iconPath> -->
        if subcategory["name"] == 'MAIN':
            things = list2xpath(subcategory['things'])
            if len(things):
                xml_patches += f'<li Class="XmlExtensions.PatchOperationAddOrReplace"><xpath>Defs/ThingDef[{things}]</xpath><value><designationCategory>{category}</designationCategory></value></li>'
            terrains = list2xpath(subcategory.get('terrains', []))
            if len(terrains):
                xml_patches += f'<li Class="XmlExtensions.PatchOperationAddOrReplace"><xpath>Defs/TerrainDef[{terrains}]</xpath><value><designationCategory>{category}</designationCategory></value></li>'
            remove_dcat = list2xpath(subcategory.get('remove_dcat', []))
            if len(remove_dcat):
                xml_patches += f'<li Class="XmlExtensions.PatchOperationSafeRemove"><xpath>Defs/ThingDef[{remove_dcat}]/designationCategory</xpath></li>'
        else:
            subcategoryDefName = f'Subcategory_{category}_{subcategory["name"]}'
            xml_subcategories += f'<TabulaRasa.DesignatorSubCategoryDef><defName>{subcategoryDefName}</defName><label>{subcategory["name"]}</label><designationCategory>{category}</designationCategory></TabulaRasa.DesignatorSubCategoryDef>'

            things = list2xpath(subcategory['things'])
            if len(things):
                xml_patches += f'<li Class="XmlExtensions.PatchOperationSafeRemove"><xpath>Defs/ThingDef[{things}]/modExtensions/li[@Class="TabulaRasa.DefModExt_SubCategoryBuilding"]</xpath></li>'
                xml_patches += f'<li Class="XmlExtensions.PatchOperationAddOrReplace"><xpath>Defs/ThingDef[{things}]</xpath><value><designationCategory>{category}</designationCategory></value></li>'
                xml_patches += f'<li Class="XmlExtensions.PatchOperationAddModExtension"><xpath>Defs/ThingDef[{things}]</xpath><value><li Class="TabulaRasa.DefModExt_SubCategoryBuilding"><subCategory>{subcategoryDefName}</subCategory><showOnlyInCategory>true</showOnlyInCategory></li></value></li>'

            terrains = list2xpath(subcategory.get('terrains', []))
            if len(terrains):
                xml_patches += f'<li Class="XmlExtensions.PatchOperationAddOrReplace"><xpath>Defs/TerrainDef[{terrains}]</xpath><value><designationCategory>{category}</designationCategory></value></li>'
                xml_patches += f'<li Class="XmlExtensions.PatchOperationAddModExtension"><xpath>Defs/TerrainDef[{terrains}]</xpath><value><li Class="TabulaRasa.DefModExt_SubCategoryBuilding"><subCategory>{subcategoryDefName}</subCategory><showOnlyInCategory>true</showOnlyInCategory></li></value></li>'

for des in merged_designators:
    xml_subcategories += f'<DesignationCategoryDef><defName>{des["defName"]}</defName><label>{des["label"]}</label><order>{des["order"]}</order><specialDesignatorClasses>{list2li(des["specialDesignatorClasses"])}</specialDesignatorClasses></DesignationCategoryDef>'
    xml_patches += f'<li Class="XmlExtensions.PatchOperationReplace"><xpath>Defs/ThingDef[{list2xpath(des["designators"], "designationCategory")}]/designationCategory</xpath><value><designationCategory>{des["defName"]}</designationCategory></value></li>'

    xml_patches += f'<li Class="XmlExtensions.PatchOperationSafeRemove"><xpath>Defs/DesignationCategoryDef[{list2xpath(des["designators"])}]</xpath></li>'    

if len(removed_designators):
    xml_patches += f'<li Class="XmlExtensions.PatchOperationReplace"><xpath>Defs/ThingDef[{list2xpath(removed_designators, "designationCategory")}]/designationCategory</xpath><value><designationCategory>Category_Others</designationCategory></value></li>'
    xml_patches += f'<li Class="XmlExtensions.PatchOperationSafeRemove"><xpath>Defs/DesignationCategoryDef[{list2xpath(removed_designators)}]</xpath></li>'

with open('D:/Games/Rimworld/rimworld-mods/AlienPatches/1.5/Defs/Subcategories.xml', 'w') as defs_file:
    defs_file.write(f'<?xml version="1.0" encoding="utf-8"?><Defs>{xml_subcategories}</Defs>')

with open('D:/Games/Rimworld/rimworld-mods/AlienPatches/1.5/Patches/Subcategories.xml', 'w') as patches_file:
    patches_file.write(f'<?xml version="1.0" encoding="utf-8"?><Patch><Operation Class="PatchOperationSequence"><success>Always</success><operations>{xml_patches}</operations></Operation></Patch>')
