﻿using Verse;
using System;
using RimWorld;
using UnityEngine;

namespace Feathers
{
    public class CompPropertiesFeatherDrop : CompProperties
    {
        public float progressRate = 0.022f;
        public CompPropertiesFeatherDrop()
        {
            this.compClass = typeof(FeatherComp);
        }
    }

    public class FeatherComp : ThingComp
    {
        public CompPropertiesFeatherDrop Props => (CompPropertiesFeatherDrop) this.props;
        private float featherProgress;
        private Pawn pawn;
        private float progressRate = 0.2f;
        private ThingDef resultDef;
        private bool spawned = false;

        public override void PostSpawnSetup(bool respawningAfterLoad)
        {
            base.PostSpawnSetup(respawningAfterLoad);
            this.spawned = true;
            pawn = this.parent as Pawn;
            resultDef = DefDatabase<ThingDef>.GetNamed("CH_feather");
            if (pawn != null)
            {
                this.progressRate = pawn.BodySize * Props.progressRate;
            }
        }

        public override void CompTickRare()
        {
            // 1 day = 60k ticks, tickRare = 240 per day
            if (this.spawned && this.pawn.training != null && this.pawn.training.HasLearned(TrainableDefOf.Tameness))
            {
                base.CompTickRare();
                this.featherProgress += (this.progressRate + UnityEngine.Random.Range(-this.progressRate, this.progressRate));
                if (this.featherProgress > 0.99f)
                {
                    var thing = ThingMaker.MakeThing(resultDef);
                    thing.stackCount = 1;
                    GenSpawn.Spawn(thing, pawn.Position, pawn.Map);
                    this.featherProgress = 0f;
                }
            }
        }

        public override string CompInspectStringExtra()
        {
            return "FeatherProgress".Translate() + ": " + this.featherProgress.ToStringPercent();
        }

        public override void PostExposeData()
        {
            base.PostExposeData();
            Scribe_Values.Look<float>(ref this.featherProgress, "featherProgress", 0f, false);
        }
    }

    public class CompPropertiesDung : CompProperties
    {
        public float progressRate = 0.001f;

        public CompPropertiesDung()
        {
            this.compClass = typeof(DungComp);
        }
    }

    public class DungComp : ThingComp
    {
        public CompPropertiesDung Props => (CompPropertiesDung) this.props;
        private float dungProgress;
        private int count;
        private Pawn pawn;
        private ThingDef resultDef;
        private bool spawned = false;
        private TrainableDef haulTd = null;

        public override void PostSpawnSetup(bool respawningAfterLoad)
        {
            base.PostSpawnSetup(respawningAfterLoad);
            this.spawned = true;
            pawn = this.parent as Pawn;
            resultDef = DefDatabase<ThingDef>.GetNamed("CH_dung");
            haulTd = DefDatabase<TrainableDef>.GetNamed("Haul");
            if (pawn != null)
            {
                this.count = (int) Math.Ceiling(this.pawn.BodySize * 4);
            }
            else
            {
                this.count = 1;
            }
        }

        public override void CompTickRare()
        {
            if (ModSettings.Instance.enableDung && this.spawned && this.pawn.training != null && this.pawn.training.HasLearned(TrainableDefOf.Tameness) && !this.pawn.training.HasLearned(haulTd))
            {
                base.CompTickRare();
                this.dungProgress += (Props.progressRate + UnityEngine.Random.Range(-Props.progressRate, Props.progressRate));
                if (this.dungProgress > 0.99f && pawn.Map != null)
                {
                    var thing = ThingMaker.MakeThing(resultDef);
                    thing.stackCount = this.count;
                    GenSpawn.Spawn(thing, pawn.Position, pawn.Map);
                    this.dungProgress = 0f;
                }
            }
        }

        public override string CompInspectStringExtra()
        {
            return ModSettings.Instance.enableDung ? (string)("DungProgress".Translate() + ": " + this.dungProgress.ToStringPercent()) : "";
        }

        public override void PostExposeData()
        {
            base.PostExposeData();
            Scribe_Values.Look<float>(ref this.dungProgress, "dungProgress", 0f, false);
        }
    }

    public class CompPropertiesSpecialProduct : CompProperties
    {
        public ThingDef productThingDef;
        public float productMultiplier = 1f;
    }

    public class SpecialProductComp : ThingComp
    {
        public CompPropertiesSpecialProduct Props => (CompPropertiesSpecialProduct) this.props;
    }
}