﻿using HugsLib;
using HugsLib.Settings;

namespace Feathers
{
    public class ModSettings : ModBase
    {
        public override string ModIdentifier => "Feathers";

        public SettingHandle<bool> enableDung;
        public SettingHandle<bool> enableFeather;
        public SettingHandle<bool> enableSinew;
        public SettingHandle<bool> enableHoof;
        public SettingHandle<bool> enableTeeth;
        public SettingHandle<bool> enableFat;

        private ModSettings()
        {
            Instance = this;
        }
        public override void DefsLoaded()
        {
            enableDung = Settings.GetHandle<bool>("enableDung", "Enable dung", "", true);
            enableFeather = Settings.GetHandle<bool>("enableFeather", "Enable feather", "", true);
            enableSinew = Settings.GetHandle<bool>("enableSinew", "Enable sinew", "", true);
            enableHoof = Settings.GetHandle<bool>("enableHoof", "Enable hoof", "", true);
            enableTeeth = Settings.GetHandle<bool>("enableTeeth", "Enable teeth", "", true);
            enableFat = Settings.GetHandle<bool>("enableFat", "Enable fat", "", true);
        }
        
        public static ModSettings Instance { get; private set; }
    }
}