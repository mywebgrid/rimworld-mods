using Verse;
using System;
using System.Linq;
using RimWorld;
using UnityEngine;

namespace Feathers
{
    public class IngestionOutcomeDoer_RemoveHediff : IngestionOutcomeDoer
    {
        public HediffDef hediffDef;

        protected override void DoIngestionOutcomeSpecial(Pawn pawn, Thing ingested)
        {
            if (pawn.health.hediffSet.HasHediff(hediffDef))
            {
                foreach (var hediff in pawn.health.hediffSet.hediffs.Where(hediff => hediff.def == hediffDef))
                {
                    pawn.health.RemoveHediff(hediff);
                    return;
                }
            }
        }
    }
}