using HarmonyLib;
using System.Reflection;
using System.Collections.Generic;
using Verse;
using RimWorld;
using System;

namespace Feathers
{
    [StaticConstructorOnStartup]
    public class HarmonyPatches
    {
        static HarmonyPatches()
        {
            var harmony = new Harmony("rimworld.bodlosh.feathers");
            MethodInfo targetMethod;
            HarmonyMethod prefixMethod;
            HarmonyMethod postfixMethod;
            //
            targetMethod = AccessTools.Method(typeof(Verse.Pawn), nameof(Verse.Pawn.ButcherProducts));
            postfixMethod = new HarmonyMethod(typeof(HarmonyPatches).GetMethod("ButcherProducts_PostFix"));
            harmony.Patch(targetMethod, null, postfixMethod);
            //
            // harmony.Patch(
            //     original: AccessTools.Method(type: typeof(Corpse), name: nameof(Corpse.SpecialDisplayStats)),
            //     prefix: null,
            //     postfix: new HarmonyMethod(typeof(HarmonyPatches), nameof(SpecialDisplayStats_PostFix)));
        }

        public static void ButcherProducts_PostFix(Verse.Pawn __instance, ref IEnumerable<Thing> __result, float efficiency)
        {
            if (__instance.kindDef.defaultFactionType == FactionDefOf.Mechanoid)
            {
                return;
            }

            var featherCount = 0;
            var teethCount = 0;
            var hoofCount = 0;
            var sinewCount = 0;
            var fatCount = 0;
            var dropsWool = (__instance.GetComp<CompShearable>() != null);

            if (ModSettings.Instance.enableFeather && __instance.GetComp<FeatherComp>() != null)
            {
                featherCount = GenMath.RoundRandom(__instance.BodySize * 100 * efficiency);
            }

            if (ModSettings.Instance.enableHoof || ModSettings.Instance.enableTeeth)
            {
                var tools = __instance.Tools;
                foreach (var t in tools)
                {
                    if (ModSettings.Instance.enableHoof && t.label != null && t.label.EndsWith("hoof") && hoofCount == 0)
                    {
                        hoofCount = GenMath.RoundRandom(__instance.BodySize * 4 * efficiency);
                    }

                    if (ModSettings.Instance.enableTeeth && t.linkedBodyPartsGroup == DefDatabase<BodyPartGroupDef>.GetNamed("Teeth") && teethCount == 0)
                    {
                        teethCount = GenMath.RoundRandom(__instance.BodySize * 25 * efficiency);
                    }
                }
            }

            List<Thing> NewList = new List<Thing>();
            foreach (Thing entry in __result)
            {
                NewList.Add(entry);
            }

            if (__instance.GetComps<SpecialProductComp>() != null)
            {
                var specialCount = 0;
                Thing special = null;
                foreach (var scp in __instance.GetComps<SpecialProductComp>())
                {
                    specialCount = GenMath.RoundRandom(__instance.BodySize * scp.Props.productMultiplier * efficiency);
                    if (specialCount > 0)
                    {
                        special = ThingMaker.MakeThing(scp.Props.productThingDef);
                        special.stackCount = specialCount;
                        NewList.Add(special);
                    }
                }
            }

            // var boneCount = GenMath.RoundRandom(__instance.GetStatValue(DefDatabase<StatDef>.GetNamed("BoneAmount", true), true) * efficiency);
            int meatCountCheck = GenMath.RoundRandom(__instance.GetStatValue(DefDatabase<StatDef>.GetNamed("MeatAmount", true), true));

            if (ModSettings.Instance.enableSinew)
            {
                sinewCount = GenMath.RoundRandom(__instance.BodySize * 4 * efficiency);
            }

            if (ModSettings.Instance.enableFat)
            {
                fatCount = GenMath.RoundRandom(__instance.BodySize * 4 * efficiency);
            }

            // if (meatCountCheck > 1 && boneCount > 0)
            // {
            //     Thing bones = ThingMaker.MakeThing(DefDatabase<ThingDef>.GetNamed("BoneItem"), null);
            //     bones.stackCount = boneCount;
            //     NewList.Add(bones);
            // }

            if (fatCount > 0)
            {
                Thing fat = ThingMaker.MakeThing(DefDatabase<ThingDef>.GetNamed("CH_fat"), null);
                fat.stackCount = fatCount;
                NewList.Add(fat);
            }

            if (featherCount > 0)
            {
                Thing feathers = ThingMaker.MakeThing(DefDatabase<ThingDef>.GetNamed("CH_feather"), null);
                feathers.stackCount = featherCount;
                NewList.Add(feathers);
            }

            if (teethCount > 0)
            {
                Thing teeth = ThingMaker.MakeThing(DefDatabase<ThingDef>.GetNamed("CH_tooth"), null);
                teeth.stackCount = teethCount;
                NewList.Add(teeth);
            }

            if (hoofCount > 0)
            {
                Thing hooves = ThingMaker.MakeThing(DefDatabase<ThingDef>.GetNamed("CH_hoof"), null);
                hooves.stackCount = hoofCount;
                NewList.Add(hooves);
            }

            if (sinewCount > 0)
            {
                Thing sinew = ThingMaker.MakeThing(DefDatabase<ThingDef>.GetNamed("CH_sinew"), null);
                sinew.stackCount = sinewCount;
                NewList.Add(sinew);
            }

            if (dropsWool)
            {
                var comp = __instance.GetComp<CompShearable>();
                var woolDef = comp.Props.woolDef;
                var fullness = 0.5f;
                if (__instance.training != null && __instance.training.HasLearned(TrainableDefOf.Tameness))
                {
                    fullness = comp.Fullness;
                }

                var woolAmount = GenMath.RoundRandom(comp.Props.woolAmount * fullness * efficiency) + 1;
                if (woolAmount > 0)
                {
                    Thing wool = ThingMaker.MakeThing(woolDef, null);
                    wool.stackCount = woolAmount;
                    NewList.Add(wool);
                }
            }


            IEnumerable<Thing> output = NewList;
            __result = output;
        }


        static void SpecialDisplayStats_PostFix(Corpse __instance, ref IEnumerable<StatDrawEntry> __result)
        {
            // Create a modifyable list
            List<StatDrawEntry> NewList = new List<StatDrawEntry>();

            // copy vanilla entries into the new list
            foreach (StatDrawEntry entry in __result)
            {
                // it's possible to discard entries here if needed
                NewList.Add(entry);
            }

            // custom code to modify list contents
            StatDef BoneAmount = DefDatabase<StatDef>.GetNamed("BoneAmount", true);
            float pawnBoneCount = __instance.InnerPawn.GetStatValue(BoneAmount, true);
            NewList.Add(new StatDrawEntry(BoneAmount.category, BoneAmount, pawnBoneCount, StatRequest.For(__instance.InnerPawn), ToStringNumberSense.Undefined));
            
            StatDef BoneBigAmount = DefDatabase<StatDef>.GetNamed("BoneBigAmount", true);
            float pawnBigBoneCount = __instance.InnerPawn.GetStatValue(BoneBigAmount, true);
            NewList.Add(new StatDrawEntry(BoneBigAmount.category, BoneBigAmount, pawnBigBoneCount, StatRequest.For(__instance.InnerPawn), ToStringNumberSense.Undefined));

            // convert list to IEnumerable to match the caller's expectations
            IEnumerable<StatDrawEntry> output = NewList;

            // make caller use the list
            __result = output;
        }
    }
}