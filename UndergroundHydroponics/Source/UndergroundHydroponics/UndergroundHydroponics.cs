﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RimWorld;
using RimWorld.Planet;
using UnityEngine;
using Verse;

namespace UndergroundHydroponics
{
    public class UndergroundHydroponics : Building, IPlantToGrowSettable
    {
        public CompPowerTrader compPowerTrader;
        public CompRefuelable compRefuel;
        private ThingDef plantDefToGrow;
        private Thing harvestedThing;
        private int baseHarvestCount;
        private float baseGrowTime;
        public IEnumerable<IntVec3> Cells { get; }
        private bool growingNow;
        private int size;
        private float progress;
        private float progressPerDay;
        private int machineryInside;
        private const int MAX_SIZE = 100;

        public UndergroundHydroponics()
        {
            this.size = 1;
            this.progress = 0;
            this.growingNow = false;
            this.plantDefToGrow = DefDatabase<ThingDef>.GetNamed("Plant_Potato");
            this.machineryInside = 0;
        }

        public override void SpawnSetup(Map map, bool respawningAfterLoad)
        {
            base.SpawnSetup(map, respawningAfterLoad);
            this.compPowerTrader = this.GetComp<CompPowerTrader>();
            this.compRefuel = this.GetComp<CompRefuelable>();
            if (respawningAfterLoad)
            {
                // this.compRefuel.Props.fuelCapacity = this.size;
            }
        }

        private bool IncreaseSize(int amount = 1)
        {
            if (this.size + amount > MAX_SIZE)
            {
                amount = MAX_SIZE - this.size;
            }

            if (this.compRefuel.Fuel < amount)
            {
                return false;
            }

            this.compRefuel.ConsumeFuel(amount);
            this.size += amount;
            return true;
        }

        public override IEnumerable<Gizmo> GetGizmos()
        {
            foreach (var g in base.GetGizmos())
            {
                yield return g;
            }

            if (!this.IsGrowing())
            {
                yield return new Command_SetPlantToGrow
                {
                    settable = this
                };

                yield return new Command_Action
                {
                    icon = ContentFinder<Texture2D>.Get("UI/Commands/SetPlantToGrow"),
                    defaultLabel = "Size++",
                    defaultDesc = "Size++",
                    activateSound = SoundDef.Named("Click"),
                    action = delegate { this.IncreaseSize(1); }
                };

                yield return new Command_Action
                {
                    icon = ContentFinder<Texture2D>.Get("UI/Commands/SetPlantToGrow"),
                    defaultLabel = "Size+10",
                    defaultDesc = "Size+10",
                    activateSound = SoundDef.Named("Click"),
                    action = delegate { this.IncreaseSize(10); }
                };
            }

            yield return new Command_Action
            {
                icon = ContentFinder<Texture2D>.Get("UI/Commands/SetPlantToGrow"),
                defaultLabel = "Start/stop",
                defaultDesc = "Start/stop",
                activateSound = SoundDef.Named("Click"),
                action = delegate
                {
                    this.progress = 0;
                    if (!this.growingNow)
                    {
                        // this.compPowerTrader.powerOutputInt = this.size * 999;
                        this.harvestedThing = ThingMaker.MakeThing(this.plantDefToGrow.plant.harvestedThingDef);
                        this.baseHarvestCount = (int)Math.Round(this.plantDefToGrow.plant.harvestYield) * 2;
                        this.harvestedThing.stackCount = this.baseHarvestCount * this.size;
                        this.baseGrowTime = this.plantDefToGrow.plant.growDays;
                        this.progressPerDay =
                            100 / (this.baseGrowTime *
                                   240); // 1 day = 60k ticks, rate is 1 in 250 ticks, 60k / 250 = 240
                        // this.compRefuel.ConsumeFuel(1000);
                    }

                    this.growingNow = !this.growingNow;
                }
            };
        }

        public override string GetInspectString()
        {
            var sb = new StringBuilder("\n");
            sb.AppendLine("Selected plant: " + this.plantDefToGrow.label);
            sb.AppendLine("Current size: " + this.size);
            if (this.growingNow)
            {
                sb.Append("Progress: " + Math.Round(this.progress, 2) + "%");
            }
            else
            {
                sb.Append("Progress: not running");
            }

            return base.GetInspectString() + sb;
        }

        public override void TickRare()
        {
            if (this.compPowerTrader.PowerOn && this.growingNow && this.compRefuel.IsFull)
            {
                if (this.progress < 100)
                {
                    this.progress += this.progressPerDay;
                }
                else
                {
                    GenPlace.TryPlaceThing(this.harvestedThing, this.Position, this.Map, ThingPlaceMode.Near);
                    this.progress = 0;
                }
            }
            else if (this.size < MAX_SIZE)
            {
                this.IncreaseSize();
            }
        }

        public ThingDef GetPlantDefToGrow()
        {
            return this.plantDefToGrow;
        }

        public void SetPlantDefToGrow(ThingDef plantDef)
        {
            this.plantDefToGrow = plantDef;
        }

        public bool IsGrowing()
        {
            return this.growingNow;
        }

        public bool CanAcceptSowNow()
        {
            return true;
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look(ref this.progress, "growthProgress");
            Scribe_Values.Look(ref this.growingNow, "isGrowing");
            Scribe_Values.Look(ref this.size, "areaSize");
            Scribe_Defs.Look(ref this.plantDefToGrow, "plantDefToGrow");
            Scribe_Deep.Look(ref this.harvestedThing, "harvestedThing");
            Scribe_Values.Look(ref this.baseGrowTime, "baseGrowTime");
            Scribe_Values.Look(ref this.baseHarvestCount, "baseHarvestCount");
            Scribe_Values.Look(ref this.progressPerDay, "progressPerDay");
        }
    }
}