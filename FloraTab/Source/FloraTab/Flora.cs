﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Verse;
using RimWorld;

namespace FloraTab
{
    public class MainTabWindow_Flora : MainTabWindow
    {
        private const int ROW_HEIGHT = 30;
        private const int STAT_WIDTH = 60;
        private const int LABEL_WIDTH = 150;

        public Vector2 scrollPosition = Vector2.zero;
        private float scrollViewHeight;

        private string curTab = "f";

        private bool showTrees = false;
        private bool showCrops = false;
        private bool showBushes = true;

        private FloatRange growthPercents = new FloatRange(0f, 1f);

        private bool isDirty = true;
        private int listUpdateNext = 0;
        private int homeAreaUpdateNext = 0;
        private IntVec3 homeAreaCenter = new IntVec3(0, 0, 0);

        private struct plantInfo
        {
            public string label;
            public float growth;
            public bool isCrop;
            public bool isTree;
            public bool harvestableNow;
            public Plant innerPlant;
            public float distance;
        }

        private int count = 0;
        List<plantInfo> plantList = new List<plantInfo>();

        private string sortProperty;
        private string sortOrder;

        public MainTabWindow_Flora()
        {
            this.doCloseX = true;
            this.doCloseButton = false;
            this.closeOnClickedOutside = true;
            this.sortProperty = "distance";
            this.sortOrder = "ASC";
        }

        private void DrawCommon(int num, float w)
        {
            int fnum = num;
            if (num == -1)
            {
                fnum = 0;
            }

            GUI.color = new Color(1f, 1f, 1f, 0.2f);
            Widgets.DrawLineHorizontal(0, ROW_HEIGHT * (fnum + 1), w);
            GUI.color = Color.white;
            Rect rowRect = new Rect(0, ROW_HEIGHT * num, w, ROW_HEIGHT);
            if (num > -1)
            {
                if (Mouse.IsOver(rowRect))
                {
                    GUI.DrawTexture(rowRect, TexUI.HighlightTex);
                }
            }
        }

        private int DrawCommonButtons(int rowNum, float rowWidth, Plant t)
        {
            if (Widgets.ButtonInvisible(new Rect(20, ROW_HEIGHT * rowNum, LABEL_WIDTH, ROW_HEIGHT)))
            {
                RimWorld.Planet.GlobalTargetInfo gti = new RimWorld.Planet.GlobalTargetInfo(t);
                CameraJumper.TryJumpAndSelect(gti);
            }

            return 0;
        }

        private void printCellSort(string sortProperty, string content, int x, int width = STAT_WIDTH)
        {
            Rect tmpRec = new Rect(x, 2, width, ROW_HEIGHT - 2);
            Widgets.Label(tmpRec, content);
            if (Mouse.IsOver(tmpRec))
            {
                GUI.DrawTexture(tmpRec, TexUI.HighlightTex);
            }

            if (Widgets.ButtonInvisible(tmpRec))
            {
                if (this.sortProperty == sortProperty)
                {
                    this.sortOrder = this.sortOrder == "ASC" ? "DESC" : "ASC";
                }
                else
                {
                    this.sortProperty = sortProperty;
                }

                this.isDirty = true;
            }

            if (this.sortProperty == sortProperty)
            {
                Texture2D texture2D = (this.sortOrder == "ASC")
                    ? ContentFinder<Texture2D>.Get("UI/Icons/Sorting", true)
                    : ContentFinder<Texture2D>.Get("UI/Icons/SortingDescending", true);
                Rect p = new Rect(tmpRec.xMax - (float) texture2D.width - 30, tmpRec.yMax - (float) texture2D.height - 1, (float) texture2D.width,
                    (float) texture2D.height);
                GUI.DrawTexture(p, texture2D);
            }
        }

        private void printCell(string content, int rowNum, int x, int width = STAT_WIDTH, string tooltip = "")
        {
            Rect tmpRec = new Rect(x, ROW_HEIGHT * rowNum + 3, width, ROW_HEIGHT - 3);
            Widgets.Label(tmpRec, content);
        }

        private void printCell(float content, int rowNum, int x, int width = STAT_WIDTH)
        {
            printCell(content.ToString(), rowNum, x, width);
        }

        private void printCell(double content, int rowNum, int x, int width = STAT_WIDTH, string unit = "")
        {
            printCell(content.ToString() + unit, rowNum, x, width);
        }

        public override Vector2 RequestedTabSize
        {
            get { return new Vector2(400, (float) UI.screenHeight); }
        }

        private void GetHomeAreaCenter()
        {
            Area_Home home = Find.CurrentMap.areaManager.Home;
            int minX = Find.CurrentMap.Size.x;
            int maxX = 0;
            int minZ = Find.CurrentMap.Size.z;
            int maxZ = 0;
            foreach (IntVec3 cell in home.ActiveCells)
            {
                if (cell.x < minX)
                {
                    minX = cell.x;
                }

                if (cell.x > maxX)
                {
                    maxX = cell.x;
                }

                if (cell.z < minZ)
                {
                    minZ = cell.z;
                }

                if (cell.z > maxZ)
                {
                    maxZ = cell.z;
                }
            }

            homeAreaCenter.x = (int) Math.Round((maxX + minX) / 2f);
            homeAreaCenter.z = (int) Math.Round((maxZ + minZ) / 2f);

            this.homeAreaUpdateNext = Find.TickManager.TicksGame + 3 * Verse.GenTicks.TickRareInterval;
        }

        private void UpdateFloraList()
        {
            if (Prefs.DevMode)
            {
                Log.Message("Update flora list");
            }

            this.count = 0;
            this.plantList = new List<plantInfo>();

            Plant plant;
            var pinf = new plantInfo();

            foreach (Thing th in Find.CurrentMap.listerThings.ThingsInGroup(ThingRequestGroup.HarvestablePlant))
            {
                plant = (Plant) th;
                if (!th.Position.Fogged(Find.CurrentMap))
                {
                    if ((plant.IsCrop && this.showCrops) || (plant.def.plant.IsTree && this.showTrees && plant.Growth > 0.4)
                                                         || (!plant.IsCrop && !plant.def.plant.IsTree && this.showBushes))
                    {
                        if (plant.Growth >= this.growthPercents.min && plant.Growth <= this.growthPercents.max)
                        {
                            pinf.isCrop = plant.IsCrop;
                            pinf.isTree = plant.def.plant.IsTree;
                            pinf.innerPlant = plant;
                            pinf.harvestableNow = plant.HarvestableNow;
                            pinf.label = plant.Label;
                            pinf.growth = plant.Growth;
                            pinf.distance = plant.Position.DistanceTo(this.homeAreaCenter);
                            this.plantList.Add(pinf);
                            this.count++;
                        }
                    }
                }
            }

            this.isDirty = false;
            this.listUpdateNext = Find.TickManager.TicksGame + Verse.GenTicks.TickRareInterval;
        }

        private void DoFloraTab(Rect r)
        {
            if (this.listUpdateNext < Find.TickManager.TicksGame)
            {
                this.isDirty = true;
            }

            if (this.homeAreaUpdateNext < Find.TickManager.TicksGame)
            {
                this.GetHomeAreaCenter();
            }

            if (this.isDirty)
            {
                this.UpdateFloraList();
            }

            Text.Font = GameFont.Small;
            Text.Anchor = TextAnchor.UpperLeft;
            GUI.color = Color.white;

            bool showTreesOld = this.showTrees, showCropsOld = this.showCrops, showBushesOld = this.showBushes;
            FloatRange growthPercentsOld = this.growthPercents;

            Widgets.CheckboxLabeled(new Rect(r.x, r.y, 65, 30), "Trees", ref this.showTrees, false);
            Widgets.CheckboxLabeled(new Rect(r.x + 80, r.y, 65, 30), "Crops", ref this.showCrops, false);
            Widgets.CheckboxLabeled(new Rect(r.x + 160, r.y, 75, 30), "Bushes", ref this.showBushes, false);

            Rect growthRangeRest = new Rect(r.x + 250, r.y, 110, 20);
            Widgets.FloatRange(growthRangeRest, 345, ref growthPercents, 0.0f, 1f, null, ToStringStyle.PercentZero);

            if (showTreesOld != this.showTrees || showCropsOld != this.showCrops || showBushesOld != this.showBushes ||
                growthPercentsOld != this.growthPercents)
            {
                this.isDirty = true;
            }

            r.y += 30;
            GUI.BeginGroup(r);
            scrollViewHeight = this.count * ROW_HEIGHT + 100;
            Rect inRect = new Rect(0, 0, r.width - 16, scrollViewHeight);

            int rowNum;
            this.DrawCommon(-1, inRect.width);
            this.printCellSort("label", "Name", 0, LABEL_WIDTH);
            this.printCellSort("growth", "Growth", LABEL_WIDTH, STAT_WIDTH);
            Texture2D cutIcon = ContentFinder<Texture2D>.Get("UI/Icons/CutPlants", true);
            Texture2D harvestIcon = ContentFinder<Texture2D>.Get("UI/Icons/Harvest", true);
            Rect icoRect = new Rect(LABEL_WIDTH + STAT_WIDTH, (ROW_HEIGHT - cutIcon.height) / 2, (float) cutIcon.width, (float) cutIcon.height);
            GUI.DrawTexture(icoRect, cutIcon);
            TooltipHandler.TipRegion(icoRect, "Cut plants");

            icoRect = new Rect(LABEL_WIDTH + STAT_WIDTH + 24, (ROW_HEIGHT - harvestIcon.height) / 2, (float) harvestIcon.width,
                (float) harvestIcon.height);
            GUI.DrawTexture(icoRect, harvestIcon);
            TooltipHandler.TipRegion(icoRect, "Harvest");

            this.printCellSort("distance", "Distance", LABEL_WIDTH + STAT_WIDTH + 24 + 48, STAT_WIDTH);

            Rect scrollRect = new Rect(r.x, r.y - 20, r.width, r.height);
            Widgets.BeginScrollView(scrollRect, ref scrollPosition, inRect);

            rowNum = 0;
            int ww = 0;
            bool valueCut, flagCut, valueHarv, flagHarv;
            DesignationDef ddefCut = DesignationDefOf.CutPlant, ddefHarv = DesignationDefOf.HarvestPlant;
            Designation desgCut, desgHarv;
            System.Reflection.FieldInfo pir = typeof(plantInfo).GetField(this.sortProperty);

            List<plantInfo> orderdedList;
            if (this.sortOrder == "DESC")
            {
                orderdedList = this.plantList.OrderByDescending(x => pir.GetValue(x)).ToList();
            }
            else
            {
                orderdedList = this.plantList.OrderBy(x => pir.GetValue(x)).ToList();
            }

            foreach (plantInfo p in orderdedList)
            {
                if (p.innerPlant != null)
                {
                    try
                    {
                        ww = 0;
                        this.DrawCommon(rowNum, inRect.width);
                        this.DrawCommonButtons(rowNum, inRect.width, p.innerPlant);
                        this.printCell(p.label, rowNum, ww, LABEL_WIDTH);
                        ww += LABEL_WIDTH;
                        this.printCell(Math.Round(p.growth * 100, 1), rowNum, ww, STAT_WIDTH, "%");
                        ww += STAT_WIDTH;

                        desgCut = p.innerPlant.MapHeld.designationManager.DesignationOn(p.innerPlant, ddefCut);
                        valueCut = desgCut != null;
                        flagCut = valueCut;
                        Widgets.Checkbox(new Vector2(ww, ROW_HEIGHT * rowNum), ref valueCut, 24, false, false);
                        if (valueCut != flagCut)
                        {
                            if (valueCut)
                            {
                                p.innerPlant.MapHeld.designationManager.AddDesignation(new Designation(p.innerPlant, ddefCut));
                            }
                            else
                            {
                                p.innerPlant.MapHeld.designationManager.RemoveDesignation(desgCut);
                            }
                        }

                        ww += 30;

                        desgHarv = p.innerPlant.MapHeld.designationManager.DesignationOn(p.innerPlant, ddefHarv);
                        valueHarv = desgHarv != null;
                        flagHarv = valueHarv;
                        Widgets.Checkbox(new Vector2(ww, ROW_HEIGHT * rowNum), ref valueHarv, 24, !p.harvestableNow || p.isTree, false);
                        if (valueHarv != flagHarv)
                        {
                            if (valueHarv)
                            {
                                p.innerPlant.MapHeld.designationManager.AddDesignation(new Designation(p.innerPlant, ddefHarv));
                            }
                            else
                            {
                                p.innerPlant.MapHeld.designationManager.RemoveDesignation(desgHarv);
                            }
                        }

                        ww += 40;
                        this.printCell(Math.Round(p.distance, 1), rowNum, ww, STAT_WIDTH);
                        rowNum++;
                    }
                    catch (System.NullReferenceException e)
                    {
                    }
                }
            }

            Widgets.EndScrollView();
            GUI.EndGroup();
        }

        private void DoInfoTab(Rect r)
        {
            GUI.BeginGroup(r);
            scrollViewHeight = this.count * ROW_HEIGHT + 100;
            Rect inRect = new Rect(0, 0, r.width - 16, scrollViewHeight);

            Rect scrollRect = new Rect(r.x, r.y - 20, r.width, r.height);
            Widgets.BeginScrollView(scrollRect, ref scrollPosition, inRect);

//            var settables = Find.Selector.SelectedObjects.OfType<IPlantToGrowSettable>().ToList<IPlantToGrowSettable>();
//            foreach (ThingDef plantTypesForGrower in PlantUtility.ValidPlantTypesForGrowers(settables))
//            {
//                if (this.IsPlantAvailable(plantTypesForGrower, map))
//                    Dialog_FancyDanPlantSetterBob.tmpAvailablePlants.Add(plantTypesForGrower);
//            }

            Widgets.EndScrollView();
            GUI.EndGroup();
        }

        public override void PreOpen()
        {
            base.PreOpen();
            this.GetHomeAreaCenter();
            this.isDirty = true;
        }

        public override void DoWindowContents(Rect r)
        {
            //base.DoWindowContents(r);
            r.yMin += 35;
            DoFloraTab(r);
        }
    }
}