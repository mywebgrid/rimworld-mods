﻿import os
import sys
from collections import OrderedDict

with open('env.txt', 'r') as env:
    currentEnv = env.readline().strip()
    if not currentEnv:
        sys.exit()
if currentEnv == 'linux':
    sourceDir = '/home/bodlosh/Repositories'
    destDir = '/home/bodlosh/.steam/steam/steamapps/common'
    rsync = '/usr/bin/rsync -rtvh'
elif currentEnv == 'windows':
    sourceDir = '/mnt/d/Games/Rimworld'
    destDir = '/mnt/d/SteamWin/steamapps/common'
    rsync = '/usr/bin/rsync -rtvh'
else:
    sys.exit()
mods = OrderedDict([
    ('1', 'AlienPatches'),
    ('2', 'AlienStuff'),
    ('3', 'FloraTab'),
    ('4', 'JAQoL'),
    ('5', 'Rimepedia'),
    ('6', 'UndergroundHydroponics'),
    ('7', 'WeaponsTab'),
    ('8', 'SaveStorageSettings'),
    ('9', 'FuelFilter'),
])
for key, value in mods.items():
    print('{}) {}'.format(key, value))
print('0) Exit')
doLanguages = False
doTextures = False
doAbout = False
while True:
    types = str(input())
    if 'a' in types:
        doAbout = True
    if 't' in types:
        doTextures = True
    if 'l' in types:
        doLanguages = True
    for key, mod in mods.items():
        if key in types:
            if key == '8':
                repository = 'rimworld-SaveStorageSettings'
            else:
                repository = f'rimworld-mods/{mod}'
            if 'd' in types:
                print(f'{rsync} {sourceDir}/{repository}/1.5/ {destDir}/RimWorld/Mods/{mod}/1.5/ --exclude \'Assemblies/0Harmony.dll\' --delete')
                if doAbout:
                    print(f'{rsync} {sourceDir}/{repository}/About/ {destDir}/RimWorld/Mods/{mod}/About/ --delete')
                if doTextures:
                    print(f'{rsync} {sourceDir}/{repository}/Textures/ {destDir}/RimWorld/Mods/{mod}/Textures/ --delete')
                if doLanguages:
                    print(f'{rsync} {sourceDir}/{repository}/Languages/ {destDir}/RimWorld/Mods/{mod}/Languages/ --delete')
            else:
                os.system(f'{rsync} {sourceDir}/{repository}/1.5/ {destDir}/RimWorld/Mods/{mod}/1.5/ --exclude \'Assemblies/0Harmony.dll\' --delete')
                if doAbout:
                    os.system(f'{rsync} {sourceDir}/{repository}/About/ {destDir}/RimWorld/Mods/{mod}/About/ --delete')
                if doTextures:
                    os.system(f'{rsync} {sourceDir}/{repository}/Textures/ {destDir}/RimWorld/Mods/{mod}/Textures/ --delete')
                if doLanguages:
                    os.system(f'{rsync} {sourceDir}/{repository}/Languages/ {destDir}/RimWorld/Mods/{mod}/Languages/ --delete')

    print('OK!')
    if '0' in types:
        sys.exit()
